package com.wds.json.v1.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSettingsRequest {
    private Long uid;
    private Boolean isIsPushUp;
    private Integer newslettersPeriod;
    private Boolean isIsGeolocationOn;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Boolean getIsIsPushUp() {
        return isIsPushUp;
    }

    public void setIsIsPushUp(Boolean isIsPushUp) {
        this.isIsPushUp = isIsPushUp;
    }

    public Integer getNewslettersPeriod() {
        return newslettersPeriod;
    }

    public void setNewslettersPeriod(Integer newslettersPeriod) {
        this.newslettersPeriod = newslettersPeriod;
    }

    public Boolean getIsIsGeolocationOn() {
        return isIsGeolocationOn;
    }

    public void setIsIsGeolocationOn(Boolean isIsGeolocationOn) {
        this.isIsGeolocationOn = isIsGeolocationOn;
    }
}
