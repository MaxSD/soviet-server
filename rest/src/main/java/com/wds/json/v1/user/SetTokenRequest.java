package com.wds.json.v1.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetTokenRequest {
    private Long uid;
    private boolean isIsAndroid;
    private String token;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public boolean isIsAndroid() {
        return isIsAndroid;
    }

    public void setIsAndroid(boolean isIsAndroid) {
        this.isIsAndroid = isIsAndroid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
