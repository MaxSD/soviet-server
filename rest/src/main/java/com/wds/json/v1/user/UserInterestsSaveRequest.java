package com.wds.json.v1.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@SuppressWarnings("UnusedDeclaration")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInterestsSaveRequest {
    private Long uid;
    private List<Long> myInterests;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public List<Long> getMyInterests() {
        return myInterests;
    }

    public void setMyInterests(List<Long> myInterests) {
        this.myInterests = myInterests;
    }
}
