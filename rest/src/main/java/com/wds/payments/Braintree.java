package com.wds.payments;

import com.braintreegateway.*;
import com.wds.dao.Dao;
import com.wds.entity.SvtOrder;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.OrderNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class Braintree {

    private static final String MERCHANT_ID = "jqfxq3s738x763vq";
    private static final String PUBLIC_KEY = "dghyvfmc827kmkn5";
    private static final String PRIVATE_KEY = "5d3118952f78d0cd2b8a7c8cb02c0b99";

    private Logger logger = LoggerFactory.getLogger(Braintree.class);

    private static com.braintreegateway.BraintreeGateway gateway = new com.braintreegateway.BraintreeGateway(
            Environment.SANDBOX,
            MERCHANT_ID,
            PUBLIC_KEY,
            PRIVATE_KEY
    );

    public String getClientoken() throws BaseException {
        try {
            return gateway.clientToken().generate();
        } catch (Exception ex) {
            logger.error("GET TOKEN ERROR: " + ex.getMessage());
            throw ex;
        }
    }

    public boolean pay(Double amount, String nonceFromTheClient) throws BaseException {
        try {
            TransactionRequest request = new TransactionRequest()
                    .amount(new BigDecimal(amount))
                    .paymentMethodNonce(nonceFromTheClient)
                    .options()
                    .submitForSettlement(true)
                    .done();

            Result<Transaction> result = gateway.transaction().sale(request);

            if (result.isSuccess()) {
                return true;
            } else {
                //result.getTransaction().getProcessorResponseText();
                return false;
            }
        } catch (Exception ex) {
            logger.error("PAY ERROR: " + ex.getMessage());
            throw ex;
        }
    }
}
