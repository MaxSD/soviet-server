package com.wds.method.v1.event;

import com.wds.dao.SvtEventsDao;
import com.wds.entity.SvtEvent;
import com.wds.exception.v1.BaseException;

import java.util.List;

public class EventsList {

    public static List<SvtEvent> performRequest(long cityId, int start, int count) throws BaseException {

        SvtEventsDao svtEventsDao = new SvtEventsDao();
        return svtEventsDao.findByCityId(cityId, start, count);
    }
}
