package com.wds.method.v1.event;

import com.wds.dao.Dao;
import com.wds.dao.SvtEventsDao;
import com.wds.entity.SvtEvent;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.EventNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventsGet {

    public static List<Map<String, Object>> performRequest(long eventId) throws BaseException {

        List<Map<String, Object>> result = new ArrayList<>(1);
        Map<String, Object> resultObject = new HashMap<>(1);

        Dao dao = new Dao();
        SvtEventsDao svtEventsDao = new SvtEventsDao();

        SvtEvent svtEvent = dao.find(SvtEvent.class, eventId);

        if (svtEvent == null)
            throw new EventNotFoundException();

        resultObject.put("id", svtEvent.getId());
        resultObject.put("fullDescription", svtEvent.getFullDescription());
        resultObject.put("participantsCount", svtEventsDao.getParticipantsCount(svtEvent.getId()));
        resultObject.put("commentsCount", svtEventsDao.getCommentsCount(svtEvent.getId()));

        result.add(resultObject);

        return result;
    }
}
