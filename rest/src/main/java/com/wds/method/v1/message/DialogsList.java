package com.wds.method.v1.message;

import com.wds.dao.SvtDialogsDao;
import com.wds.dataObjects.DialogsListElemData;
import com.wds.exception.v1.BaseException;

import java.util.List;

public class DialogsList {

    public static List<Object> performRequest(long uid, int start, int count) throws BaseException {

        SvtDialogsDao svtDialogsDao = new SvtDialogsDao();
        return svtDialogsDao.getList(uid, start, count, DialogsListElemData.class);
    }
}
