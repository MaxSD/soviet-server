package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.dao.SettingsDao;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.UserNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRewards {

    public static List<Map<String, Object>> performRequest(long uid) throws BaseException {

        Dao dao = new Dao();
        SettingsDao settingsDao = new SettingsDao();
        SvtUser svtUser = dao.find(SvtUser.class, uid);

        if (svtUser == null)
            throw new UserNotFoundException();

        List<Map<String, Object>> result = new ArrayList<>(2);

        Map<String, Object> resultObject = new HashMap<>(4);
        resultObject.put("events", svtUser.getEventsScore());
        resultObject.put("orders", svtUser.getOrdersScore());
        resultObject.put("partners", svtUser.getPartnersScore());
        resultObject.put("score", svtUser.getEventsScore() + svtUser.getOrdersScore() + svtUser.getPartnersScore());
        resultObject.put("scoreText", settingsDao.findByParamName("score_text"));
        result.add(resultObject);

        return result;
    }
}
