package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.user.UserSettingsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserSettingsSave {

    private static Logger logger = LoggerFactory.getLogger(UserSettingsSave.class);

    private static void validateRequest(UserSettingsRequest request) throws BaseException {
        if (request.getUid() == null)
            throw new NotSendingParameterException();
        if (request.getIsIsPushUp() == null)
            throw new NotSendingParameterException();
        if (request.getNewslettersPeriod() == null)
            throw new NotSendingParameterException();
        if (request.getIsIsGeolocationOn() == null)
            throw new NotSendingParameterException();
    }

    public static void performRequest(UserSettingsRequest request) throws BaseException {
        validateRequest(request);

        Dao dao = new Dao();
        SvtUser svtUser = dao.find(SvtUser.class, request.getUid());

        if (svtUser == null)
            throw new UserNotFoundException();

        svtUser.setIsIsPushUp(request.getIsIsPushUp());
        svtUser.setNewslettersPeriod(request.getNewslettersPeriod());
        svtUser.setIsIsGeolocationOn(request.getIsIsGeolocationOn());

        dao.save(svtUser);
    }
}
