package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.UserNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserSettings {

    public static List<Map<String, Object>> performRequest(long uid) throws BaseException {

        Dao dao = new Dao();
        SvtUser svtUser = dao.find(SvtUser.class, uid);

        if (svtUser == null)
            throw new UserNotFoundException();

        List<Map<String, Object>> result = new ArrayList<>(2);

        Map<String, Object> resultObject = new HashMap<>(1);
        resultObject.put("isPushUp", svtUser.isIsPushUp());
        resultObject.put("newslettersPeriod", svtUser.getNewslettersPeriod());
        resultObject.put("isGeolocationOn", svtUser.isIsGeolocationOn());
        result.add(resultObject);

        return result;
    }
}
