package com.wds.method.v1.partner;

import com.wds.dao.Dao;
import com.wds.entity.SvtPartner;
import com.wds.exception.v1.BaseException;

import java.util.List;

public class PartnersList {

    public static List<SvtPartner> performRequest(int start, int count) throws BaseException {

        Dao dao = new Dao();
        return dao.get(SvtPartner.class, start, count);
    }
}
