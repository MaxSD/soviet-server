package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.entity.SvtIndustry;
import com.wds.exception.v1.BaseException;

import java.util.List;

public class UserIndustryList {

    public static List<SvtIndustry> performRequest() throws BaseException {

        Dao dao = new Dao();
        return dao.get(SvtIndustry.class);
    }
}
