package com.wds.method.v1.message;

import com.wds.dao.SvtMessagesUnreadDao;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingUserIdException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageUnreadCount {

    private static void validateRequest(Long userId) throws BaseException {
        if (userId == null)
            throw new NotSendingUserIdException();
    }

    public static List<Map<String, Long>> performRequest(Long userId) throws BaseException {
        validateRequest(userId);

        List<Map<String, Long>> response = new ArrayList<>(0);
        Map<String, Long> returnObject = new HashMap<>();

        SvtMessagesUnreadDao messagesUnreadDao = new SvtMessagesUnreadDao();
        returnObject.put("count", messagesUnreadDao.getCountByReceiverId(userId));

        response.add(returnObject);
        return response;
    }
}
