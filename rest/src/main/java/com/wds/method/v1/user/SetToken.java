package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.dao.SvtUserTokenDao;
import com.wds.entity.SvtUserToken;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingTokenException;
import com.wds.exception.v1.NotSendingUserIdException;
import com.wds.json.v1.user.SetTokenRequest;
import org.apache.commons.lang3.StringUtils;

public class SetToken {

    private static void validateRequest(SetTokenRequest request) throws BaseException {
        if (request.getUid() == null)
            throw new NotSendingUserIdException();
        if (StringUtils.isBlank(request.getToken()))
            throw new NotSendingTokenException();
    }

    public static void performRequest(SetTokenRequest request) throws BaseException {
        validateRequest(request);

        Dao dao = new Dao();
        SvtUserTokenDao userTokenDao = new SvtUserTokenDao();

        SvtUserToken userToken = userTokenDao.findByToken(request.getToken());
        if (userToken != null) {
            if (userToken.getUserId() != request.getUid()) {
                // delete the same token for another user
                dao.delete(userToken);
            }
        }

        userToken = userTokenDao.findByUserId(request.getUid());
        if (userToken == null)
            userToken = new SvtUserToken();

        userToken.setUserId(request.getUid());
        userToken.setIsAndroid(request.isIsAndroid());
        userToken.setToken(request.getToken());

        dao.create(userToken);
    }
}
