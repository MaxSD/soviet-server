package com.wds.method.v1.message;

import com.wds.dao.Dao;
import com.wds.entity.SvtMessage;
import com.wds.exception.v1.BaseException;
import com.wds.json.v1.message.SendMessageRequest;
import com.wds.utils.ApplicationUtility;

public class SendMessage {

    public static void performRequest(SendMessageRequest request) throws BaseException {

        SvtMessage message = new SvtMessage();
        message.setDialogId(request.getDialogId());
        message.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
        message.setSenderId(request.getUid());
        message.setMessage(request.getMessage());
        message.setType(request.getMessageType());
        message.setIsIsFromUser(true);
        message.setIsIsRead(false);

        Dao dao = new Dao();
        dao.create(message);
    }
}
