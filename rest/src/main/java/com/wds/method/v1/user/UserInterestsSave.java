package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.dao.SvtInterestsDao;
import com.wds.entity.SvtUserInterests;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingLoginException;
import com.wds.json.v1.user.UserInterestsSaveRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserInterestsSave {

    private static Logger logger = LoggerFactory.getLogger(UserInterestsSave.class);

    private static void validateRequest(UserInterestsSaveRequest request) throws BaseException {
        if (request.getUid() == null)
            throw new NotSendingLoginException();
    }

    public static void performRequest(UserInterestsSaveRequest request) throws BaseException {
        validateRequest(request);

        SvtInterestsDao svtInterestsDao = new SvtInterestsDao();

        // Delete old user's interests
        svtInterestsDao.deleteUserInterests(request.getUid());

        // Save new user's interests
        if (request.getMyInterests() != null) {
            Dao dao = new Dao();

            for (Long interestId : request.getMyInterests()) {
                SvtUserInterests svtUserInterests = new SvtUserInterests();
                svtUserInterests.setUserId(request.getUid());
                svtUserInterests.setTagId(interestId);

                dao.create(svtUserInterests);
            }
        }
    }
}
