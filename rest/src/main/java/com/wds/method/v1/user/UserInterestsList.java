package com.wds.method.v1.user;

import com.wds.dao.SvtInterestsDao;
import com.wds.exception.v1.BaseException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserInterestsList {

    public static List<Map<String, String>> performRequest(long uid) throws BaseException {

        SvtInterestsDao svtInterestsDao = new SvtInterestsDao();

        List<Map<String, String>> result = new ArrayList<>(2);

        Map<String, String> resultObject = new HashMap<>(1);
        resultObject.put("allInterests", StringUtils.join(svtInterestsDao.getAllInterests(), ","));
        result.add(resultObject);

        resultObject = new HashMap<>(1);
        resultObject.put("myInterests", StringUtils.join(svtInterestsDao.getUserInterests(uid), ","));
        result.add(resultObject);

        return result;
    }
}
