package com.wds.method.v1.event;

import com.wds.dao.SvtEventsDao;
import com.wds.dataObjects.EventCommentData;
import com.wds.exception.v1.BaseException;

import java.util.List;

public class EventCommentsList {

    public static List<Object> performRequest(long eventId, int start, int count) throws BaseException {

        SvtEventsDao svtEventsDao = new SvtEventsDao();
        return svtEventsDao.getCommentsList(eventId, start, count, EventCommentData.class);
    }
}
