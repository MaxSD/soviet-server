package com.wds.method.v1.event;

import com.wds.dao.Dao;
import com.wds.entity.SvtEventComment;
import com.wds.exception.v1.*;
import com.wds.json.v1.event.EventCommentAddRequest;
import com.wds.utils.ApplicationUtility;

public class EventCommentAdd {

    private static void validateRequest(EventCommentAddRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
        if (request.getEventId() == null || request.getEventId() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(EventCommentAddRequest request) throws BaseException {
        validateRequest(request);

        Dao dao = new Dao();
        SvtEventComment svtEventComment = new SvtEventComment();

        svtEventComment.setUserId(request.getUid());
        svtEventComment.setEventId(request.getEventId());
        svtEventComment.setMessage(request.getMessage());
        svtEventComment.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());

        dao.create(svtEventComment);
    }
}
