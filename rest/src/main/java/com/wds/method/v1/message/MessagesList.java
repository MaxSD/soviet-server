package com.wds.method.v1.message;

import com.wds.dao.SvtMessagesDao;
import com.wds.dataObjects.MessagesListElemData;
import com.wds.exception.v1.BaseException;

import java.util.List;

public class MessagesList {

    public static List<Object> performRequest(long dialogId, int start, int count) throws BaseException {

        SvtMessagesDao svtMessagesDao = new SvtMessagesDao();

        List<Object> result = svtMessagesDao.getList(dialogId, start, count, MessagesListElemData.class);
        svtMessagesDao.selectMessagesAsReaded(dialogId, false);
        return result;
    }
}
