package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.dao.SvtUsersDao;
import com.wds.database.define.Defines;
import com.wds.entity.Role;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.exception.v1.UserAlreadyExistsException;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

public class UserRegistration {

    private static void validateRequest(String firstName,
                                        String surname,
                                        Integer gender,
                                        Long birthDate,
                                        String phone,
                                        String profession,
                                        String email,
                                        String password) throws BaseException {

        if (StringUtils.isBlank(firstName) || StringUtils.isBlank(surname) ||
                gender == null || birthDate == null || StringUtils.isBlank(phone) ||
                StringUtils.isBlank(email) || StringUtils.isBlank(password))
            throw new NotSendingParameterException();
    }

    public static long performRequest(String firstName,
                                      String surname,
                                      Integer gender,
                                      Long birthDate,
                                      String phone,
                                      String profession,
                                      String email,
                                      String photoURL,
                                      String password,
                                      String friend) throws BaseException {

        validateRequest(firstName, surname, gender, birthDate, phone, profession, email, password);

        SvtUsersDao svtUsersDao = new SvtUsersDao();
        SvtUser svtUser = svtUsersDao.findByEmail(email);

        if (svtUser != null)
            throw new UserAlreadyExistsException();
        else {

            Dao dao = new Dao();

            svtUser = new SvtUser();
            svtUser.setDateRegistration(ApplicationUtility.getCurrentTimeStampGMT_0());
            svtUser.setCardNumber("");
            svtUser.setFirstName(firstName);
            svtUser.setSurname(surname);
            svtUser.setGender(gender);
            svtUser.setBirthDate(birthDate);
            svtUser.setPhone(phone);
            svtUser.setProfession(profession);
            svtUser.setEmail(email);
            svtUser.setPhoto(photoURL);
            svtUser.setPassword(ApplicationUtility.md5crypt(password));
            svtUser.setRole(dao.find(Role.class, Defines.USER_TYPE_USER));
            svtUser.setFriend(friend);

            dao.create(svtUser);
        }

        return svtUser.getId();
    }
}
