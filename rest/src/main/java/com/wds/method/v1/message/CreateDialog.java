package com.wds.method.v1.message;

import com.wds.dao.Dao;
import com.wds.entity.SvtDialog;
import com.wds.exception.v1.BaseException;
import com.wds.json.v1.message.CreateDialogRequest;
import com.wds.utils.ApplicationUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateDialog {

    public static List<Map> performRequest(CreateDialogRequest request) throws BaseException {

        SvtDialog dialog = new SvtDialog();
        dialog.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
        dialog.setUserId(request.getUid());
        dialog.setTitle(request.getTitle());

        Dao dao = new Dao();
        dao.create(dialog);

        Map<String, Long> returnObject = new HashMap<>();
        returnObject.put("id", dialog.getId());

        List<Map> result = new ArrayList<>();
        result.add(returnObject);

        return result;
    }
}
