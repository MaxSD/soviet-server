package com.wds.method.v1.message;

import com.wds.dao.Dao;
import com.wds.entity.SvtDialog;
import com.wds.exception.v1.BaseException;
import com.wds.json.v1.message.DeleteDialogRequest;

public class DeleteDialog {

    public static void performRequest(DeleteDialogRequest request) throws BaseException {

        Dao dao = new Dao();
        SvtDialog dialog = dao.find(SvtDialog.class, request.getDialogId());

        if (dialog != null) {
            dao.delete(dialog);
        }
    }
}
