package com.wds.method.v1.user;

import com.wds.dao.SvtUsersDao;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingPasswordException;
import com.wds.exception.v1.NotSendingLoginException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.user.UserLoginRequest;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserLogin {

    private static void validateRequest(UserLoginRequest request) throws BaseException {
        if (StringUtils.isBlank(request.getLogin()))
            throw new NotSendingLoginException();
        if (StringUtils.isBlank(request.getPassword()))
            throw new NotSendingPasswordException();
    }

    public static List<Map<String, Object>> performRequest(UserLoginRequest request) throws BaseException {
        validateRequest(request);

        SvtUsersDao svtUsersDao  = new SvtUsersDao();
        SvtUser user = svtUsersDao.findByCardNumber(request.getLogin());

        if (user == null || !user.getPassword().equals(ApplicationUtility.md5crypt(request.getPassword())))
            throw new UserNotFoundException();

        Map<String, Object> responseObj = new HashMap<>(1);
        responseObj.put("uid", user.getId());
        responseObj.put("date_registration", user.getDateRegistration());
        responseObj.put("card_number", user.getCardNumber());
        responseObj.put("first_name", user.getFirstName());
        responseObj.put("surname", user.getSurname());

        List<Map<String, Object>> response = new ArrayList<>(1);
        response.add(responseObj);

        return response;
    }
}
