package com.wds.method.v1.event;

import com.wds.dao.Dao;
import com.wds.entity.SvtUserEvents;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingIdException;
import com.wds.json.v1.event.EventAddToListRequest;

public class EventAddToList {

    private static void validateRequest(EventAddToListRequest request) throws BaseException {
        if (request.getUid() == null || request.getUid() == 0)
            throw new NotSendingIdException();
        if (request.getEventId() == null || request.getEventId() == 0)
            throw new NotSendingIdException();
    }

    public static void performRequest(EventAddToListRequest request) throws BaseException {
        validateRequest(request);

        Dao dao = new Dao();
        SvtUserEvents userEvents = new SvtUserEvents();

        userEvents.setUserId(request.getUid());
        userEvents.setEventId(request.getEventId());

        dao.create(userEvents);
    }
}
