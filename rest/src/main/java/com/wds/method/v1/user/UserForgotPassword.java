package com.wds.method.v1.user;

import com.wds.dao.SvtUsersDao;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingLoginException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.json.v1.user.UserForgotPasswordRequest;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.SenderEmail;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class UserForgotPassword {

    private static Logger logger = LoggerFactory.getLogger(UserForgotPassword.class);

    private static void validateRequest(UserForgotPasswordRequest request) throws BaseException {
        if (StringUtils.isBlank(request.getEmail()))
            throw new NotSendingLoginException();
    }

    public static void performRequest(UserForgotPasswordRequest request) throws BaseException {
        validateRequest(request);

        SvtUsersDao svtUsersDao  = new SvtUsersDao();
        SvtUser user = svtUsersDao.findByEmail(request.getEmail());

        if (user == null)
            throw new UserNotFoundException();

        try {
            SenderEmail senderEmail = new SenderEmail();
            senderEmail.sendEmail(request.getEmail(), "Password recovery", ApplicationUtility.generateRandomString(6), false);
        } catch (Exception ex) {
            logger.error(Arrays.toString(ex.getStackTrace()));
        }
    }
}
