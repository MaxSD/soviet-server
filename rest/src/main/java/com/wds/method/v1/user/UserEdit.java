package com.wds.method.v1.user;

import com.wds.dao.Dao;
import com.wds.dao.SvtUsersDao;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.exception.v1.UserAlreadyExistsException;
import com.wds.exception.v1.UserNotFoundException;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

public class UserEdit {

    private static void validateRequest(Long uid,
                                        String firstName,
                                        String surname,
                                        Integer gender,
                                        Long birthDate,
                                        String phone,
                                        String profession,
                                        String email,
                                        String password) throws BaseException {

        if (uid == null || StringUtils.isBlank(firstName) || StringUtils.isBlank(surname) ||
                gender == null || birthDate == null || StringUtils.isBlank(phone) ||
                StringUtils.isBlank(email) || StringUtils.isBlank(password))
            throw new NotSendingParameterException();
    }

    public static void performRequest(Long uid,
                                      String firstName,
                                      String surname,
                                      Integer gender,
                                      Long birthDate,
                                      String phone,
                                      String profession,
                                      String email,
                                      String photoURL,
                                      String password) throws BaseException {

        validateRequest(uid, firstName, surname, gender, birthDate, phone, profession, email, password);

        SvtUsersDao svtUsersDao = new SvtUsersDao();
        Dao dao = new Dao();

        SvtUser svtUser = svtUsersDao.findByEmail(email);
        if (svtUser != null && svtUser.getId() != uid)
            throw new UserAlreadyExistsException();

        svtUser = dao.find(SvtUser.class, uid);
        if (svtUser == null)
            throw new UserNotFoundException();

        svtUser.setCardNumber("");
        svtUser.setFirstName(firstName);
        svtUser.setSurname(surname);
        svtUser.setGender(gender);
        svtUser.setBirthDate(birthDate);
        svtUser.setPhone(phone);
        svtUser.setProfession(profession);
        svtUser.setEmail(email);
        svtUser.setPhoto(photoURL);
        svtUser.setPassword(ApplicationUtility.md5crypt(password));

        dao.save(svtUser);
    }
}
