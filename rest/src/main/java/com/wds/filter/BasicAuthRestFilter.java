package com.wds.filter;

import com.wds.dao.SettingsDao;
import com.wds.define.Defines;
import com.wds.entity.Settings;
import com.wds.exception.v1.SelectDatabaseException;
import org.apache.commons.codec.binary.Base64;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

@WebFilter(value = "/rest/*")
public class BasicAuthRestFilter implements Filter {

    public void init(FilterConfig fConfig) throws ServletException {}

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String authHeader = request.getHeader("Authorization");

        if (authHeader != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(authHeader);
            if (stringTokenizer.hasMoreTokens()) {
                String basic = stringTokenizer.nextToken();

                if (basic.equalsIgnoreCase("Basic"))
                    try {
                        String credentials = new String(Base64.decodeBase64(stringTokenizer.nextToken().getBytes()), "UTF-8");
                        int p = credentials.indexOf(":");
                        if (p != -1) {
                            String _username = credentials.substring(0, p).trim();
                            String _password = credentials.substring(p + 1).trim();

                            String username;
                            String password;

                            SettingsDao settingsDao = new SettingsDao();
                            Settings settings = settingsDao.findByParamName(Defines.REST_AUTH_USERNAME);

                            /*

                            if (settings != null) {
                                username = StringUtils.trimToEmpty(settings.getParamValue());

                                settings = settingsDao.findByParamName(Defines.REST_AUTH_PASSWORD);
                                if (settings != null) {
                                    password = StringUtils.trimToEmpty(settings.getParamValue());

                                    if (!username.equals(_username) || !BCrypt.checkpw(_password, password)) {
                                        unauthorized(response, "Bad credentials");
                                    }
                                } else {
                                    unauthorized(response, "Couldn't get a password.");
                                }
                            } else {
                                unauthorized(response, "Couldn't get an user name.");
                            }

                            */

                            filterChain.doFilter(servletRequest, servletResponse);
                        } else {
                            unauthorized(response, "Invalid authentication token");
                        }
                    } catch (UnsupportedEncodingException e) {
                        throw new Error("Couldn't retrieve authentication", e);
                    } catch (SelectDatabaseException ce) {
                        throw new Error(ce.getMessage());
                    }

            }
        } else {
            unauthorized(response);
        }
    }

    public void destroy() {}

    private void unauthorized(HttpServletResponse response, String message) throws IOException {
        response.setHeader("WWW-Authenticate", "Basic realm=\"Protected\"");
        response.sendError(401, message);
    }

    private void unauthorized(HttpServletResponse response) throws IOException {
        unauthorized(response, "Unauthorized");
    }
}
