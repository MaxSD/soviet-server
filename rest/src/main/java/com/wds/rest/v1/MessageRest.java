package com.wds.rest.v1;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;
import com.wds.dao.Dao;
import com.wds.database.define.Defines;
import com.wds.entity.SvtMessage;
import com.wds.exception.v1.BaseException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.message.CreateDialogRequest;
import com.wds.json.v1.message.DeleteDialogRequest;
import com.wds.method.v1.message.*;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.InputStream;

@Path("v1/message")
@Produces(MediaType.APPLICATION_JSON)
public class MessageRest {

    @GET
    @Path("/unreadCount/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getUnreadMessagesCount(@PathParam("uid") Long userId) {
        String methodName = "/message/unreadCount/{uid}";
        try {
            return Response.ok(MessageUnreadCount.performRequest(userId)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/dialog/list/{uid}/{start}/{count}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getDialogsList(@PathParam("uid") Long uid,
                                   @PathParam("start") Integer start,
                                   @PathParam("count") Integer count) {
        String methodName = "/message/dialog/list/{uid}/{start}/{count}";
        try {
            return Response.ok(DialogsList.performRequest(uid, start, count)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/list/{dialogId}/{start}/{count}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMessagesList(@PathParam("dialogId") Long dialogId,
                                    @PathParam("start") Integer start,
                                    @PathParam("count") Integer count) {
        String methodName = "/message/list/{dialogId}/{start}/{count}";
        try {
            return Response.ok(MessagesList.performRequest(dialogId, start, count)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/createDialog")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCreateDialog(CreateDialogRequest request) {
        String methodName = "/message/createDialog";
        try {
            return Response.ok(CreateDialog.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/send")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response sendMessage(
            FormDataMultiPart multiPart,
            @FormDataParam("dialogId") Long dialogId,
            @FormDataParam("uid") Long uid,
            @FormDataParam("messageType") Integer messageType,
            @FormDataParam("message") String message,
            @FormDataParam("file") InputStream file,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {

        String methodName = "/event/create";
        try {
            String fileName = "";

            /**
             * Upload image 1
             */
            if (file != null) {
                fileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(fileDetail.getFileName(), ".");

                ApplicationUtility.writeToFile(file,
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                fileName);
            }

            SvtMessage svtMessage = new SvtMessage();
            svtMessage.setDialogId(dialogId);
            svtMessage.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
            svtMessage.setSenderId(uid);
            svtMessage.setType(messageType);
            svtMessage.setIsIsFromUser(true);
            svtMessage.setIsIsRead(false);
            svtMessage.setServiceMessage("");

            if (messageType == Defines.MESSAGE_TYPE_LINK) {
                svtMessage.setMessage(com.wds.utils.define.Defines.getHostImages() + fileName);
            } else {
                svtMessage.setMessage(message);
            }

            Dao dao = new Dao();
            dao.create(svtMessage);

            return Response.ok("{}").build();

        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/deleteDialog")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteDialog(DeleteDialogRequest request) {
        String methodName = "/message/deleteDialog";
        try {
            DeleteDialog.performRequest(request);
            return Response.ok("{}").build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
