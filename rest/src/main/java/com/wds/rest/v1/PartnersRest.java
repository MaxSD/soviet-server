package com.wds.rest.v1;

import com.wds.exception.v1.BaseException;
import com.wds.json.v1.ErrorReturn;
import com.wds.method.v1.partner.PartnersList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("v1/partner")
@Produces(MediaType.APPLICATION_JSON)
public class PartnersRest {

    @GET
    @Path("/list/{start}/{count}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getList(@PathParam("start") Integer start,
                            @PathParam("count") Integer count) {
        String methodName = "/list/list/{start}/{count}";
        try {
            return Response.ok(PartnersList.performRequest(start, count)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
