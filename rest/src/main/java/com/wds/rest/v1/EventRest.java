package com.wds.rest.v1;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;
import com.wds.dao.SvtEventsDao;
import com.wds.entity.SvtEvent;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.event.EventAddToListRequest;
import com.wds.json.v1.event.EventCommentAddRequest;
import com.wds.method.v1.event.*;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Path("v1/event")
@Produces(MediaType.APPLICATION_JSON)
public class EventRest {

    @GET
    @Path("/list/{cityId}/{start}/{count}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getList(@PathParam("cityId") Long cityId,
                            @PathParam("start") Integer start,
                            @PathParam("count") Integer count) {
        String methodName = "/event/{cityId}/list/{start}/{count}";
        try {
            return Response.ok(EventsList.performRequest(cityId, start, count)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/{eventId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getEvent(@PathParam("eventId") Long eventId) {
        String methodName = "/event/{eventId}";
        try {
            return Response.ok(EventsGet.performRequest(eventId)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/comment/list/{eventId}/{start}/{count}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCommentsList(@PathParam("eventId") Long eventId,
                                    @PathParam("start") Integer start,
                                    @PathParam("count") Integer count) {
        String methodName = "/event/{eventId}";
        try {
            return Response.ok(EventCommentsList.performRequest(eventId, start, count)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/comment/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(EventCommentAddRequest request) {
        String methodName = "/event/comment/add";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            EventCommentAdd.performRequest(request);
            return Response.ok("{}").build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/addToList")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(EventAddToListRequest request) {
        String methodName = "/event/addToList";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            EventAddToList.performRequest(request);
            return Response.ok("{}").build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createEvent(
            FormDataMultiPart multiPart,
            @FormDataParam("cityId") Long cityId,
            @FormDataParam("title") String title,
            @FormDataParam("description") String description,
            @FormDataParam("type") String type,
            @FormDataParam("address") String address,
            @FormDataParam("image1") InputStream image1,
            @FormDataParam("image1") FormDataContentDisposition image1Detail,
            @FormDataParam("image2") InputStream image2,
            @FormDataParam("image2") FormDataContentDisposition image2Detail,
            @FormDataParam("image3") InputStream image3,
            @FormDataParam("image3") FormDataContentDisposition image3Detail,
            @FormDataParam("date") Long date,
            @FormDataParam("price") Double price,
            @FormDataParam("tags") List<FormDataBodyPart> tags) {

        String methodName = "/event/create";
        try {
            String image1FileName = "";
            String image2FileName = "";
            String image3FileName = "";

            /**
             * Upload image 1
             */
            if (image1 != null || !StringUtils.isBlank(image1Detail.getFileName())) {
                image1FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image1Detail.getFileName(), ".");

                ApplicationUtility.writeToFile(image1,
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image1FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image1FileName);
            }

            /**
             * Upload image 2
             */
            if (image2 != null || !StringUtils.isBlank(image2Detail.getFileName())) {
                image2FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image2Detail.getFileName(), ".");

                ApplicationUtility.writeToFile(image2,
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image2FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image2FileName);
            }

            /**
             * Upload image 3
             */
            if (image3 != null || !StringUtils.isBlank(image3Detail.getFileName())) {
                image3FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image3Detail.getFileName(), ".");

                ApplicationUtility.writeToFile(image3,
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image3FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image3FileName);
            }

            SvtEvent event = new SvtEvent();
            event.setCityId(cityId);
            event.setDate(date);
            event.setCurrMonthMs(ApplicationUtility.formattedStringToDate(
                    ApplicationUtility.millisecondsToFormattedString(date * 1000, "MM.yyyy"),
                    ApplicationUtility.GMT_0,
                    "MM.yyyy").getTime());
            event.setTitle(title);
            event.setDescription(description);
            event.setFullDescription("");
            event.setType(type);
            event.setAddress(address);
            event.setImage1(com.wds.utils.define.Defines.getHostImages() + image1FileName);
            event.setImage2(com.wds.utils.define.Defines.getHostImages() + image2FileName);
            event.setImage3(com.wds.utils.define.Defines.getHostImages() + image3FileName);
            event.setPrice(price);
            event.setStatus(com.wds.database.define.Defines.EVENT_STATUS_NEW);

            SvtEventsDao eventsDao = new SvtEventsDao();

            List<Long> tagsList = new ArrayList<>();
            if (tags != null) {
                for (FormDataBodyPart tag : tags) {
                    tagsList.add(Long.parseLong(tag.getValue()));
                }
            }
            eventsDao.create(event, tagsList);

            return Response.ok("{}").build();

        } catch (IOException ie) {
            return Response.status(500).
                    entity(new ErrorReturn(500, ie.getMessage(), methodName)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
