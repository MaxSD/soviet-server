package com.wds.rest.v1;

import com.wds.dao.Dao;
import com.wds.dao.SvtUserEventsDao;
import com.wds.entity.SvtEvent;
import com.wds.entity.SvtOrder;
import com.wds.entity.SvtUserEvents;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.OrderNotFoundException;
import com.wds.json.braibtreePayments.PayEventRequest;
import com.wds.json.braibtreePayments.PayOrderRequest;
import com.wds.json.v1.ErrorReturn;
import com.wds.payments.Braintree;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("v1/braintree_payments")
@Produces(MediaType.APPLICATION_JSON)
public class BraintreePaymentsRest {

    @POST
    @Path("/get_token")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getClientToken() {
        String methodName = "/braintree_payments/get_token";
        try {
            Braintree braintree = new Braintree();

            Map<String, String> returnObject = new HashMap<>();
            returnObject.put("client_token", braintree.getClientoken());

            List<Map> result = new ArrayList<>();
            result.add(returnObject);
            return Response.ok(result).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/pay_order")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response pay(PayOrderRequest request) {
        String methodName = "/braintree_payments/pay_order";
        try {
            Dao dao = new Dao();
            SvtOrder order = dao.find(SvtOrder.class, request.getOrderId());
            if (order == null)
                throw new OrderNotFoundException();

            Braintree braintree = new Braintree();
            boolean payResult = braintree.pay(order.getPrice(), request.getNonce());

            if (payResult) {
                order.setIsIsPaid(true);
                dao.save(order);
            }

            Map<String, Boolean> returnObject = new HashMap<>();
            returnObject.put("isSuccess", payResult);

            List<Map> result = new ArrayList<>();
            result.add(returnObject);

            return Response.ok(result).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/pay_event")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response pay(PayEventRequest request) {
        String methodName = "/braintree_payments/pay_event";
        try {
            Dao dao = new Dao();

            // Check if event exists
            SvtEvent event = dao.find(SvtEvent.class, request.getEventId());
            if (event == null)
                throw new OrderNotFoundException();

            // Pay event
            Braintree braintree = new Braintree();
            boolean payResult = braintree.pay(event.getPrice(), request.getNonce());

            if (payResult) {
                SvtUserEventsDao userEventsDao = new SvtUserEventsDao();
                SvtUserEvents userEvents = userEventsDao.findByUserIdAndEventId(request.getUserId(), request.getEventId());

                if (userEvents == null) {
                    // If event is no assigned to user - assign the event to the user and set isPaid = TRUE
                    userEvents = new SvtUserEvents();
                    userEvents.setUserId(request.getUserId());
                    userEvents.setEventId(request.getEventId());
                    userEvents.setIsIsPaid(true);
                    dao.create(userEvents);
                } else {
                    // Just set isPaid = TRUE
                    userEvents.setIsIsPaid(true);
                    dao.save(userEvents);
                }
            }

            // Create result object
            Map<String, Boolean> returnObject = new HashMap<>();
            returnObject.put("isSuccess", payResult);

            List<Map> result = new ArrayList<>();
            result.add(returnObject);

            return Response.ok(result).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
