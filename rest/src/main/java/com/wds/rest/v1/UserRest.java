package com.wds.rest.v1;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.v1.user.*;
import com.wds.method.v1.user.*;
import com.wds.utils.ApplicationUtility;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("v1/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserRest {

    @POST
    @Path("/registration")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response userRegistration (
            FormDataMultiPart multiPart,
            @FormDataParam("firstName") String firstName,
            @FormDataParam("surname") String surname,
            @FormDataParam("gender") Integer gender,
            @FormDataParam("birthDate") Long birthDate,
            @FormDataParam("phone") String phone,
            @FormDataParam("profession") String profession,
            @FormDataParam("about") String about,
            @FormDataParam("email") String email,
            @FormDataParam("photo") InputStream photo,
            @FormDataParam("photo") FormDataContentDisposition photoDetail,
            @FormDataParam("password") String password,
            @FormDataParam("friend") String friend) {

        String methodName = "/user/registration";
        try {

            String photoURL = "";

            if (photo != null) {
                ApplicationUtility.writeToFile(photo, com.wds.utils.define.Defines.getImagesPathStorage() + photoDetail.getFileName());
                photoURL = com.wds.utils.define.Defines.getHostImages() + photoDetail.getFileName();
            }

            Long userId = UserRegistration.performRequest(firstName, surname, gender, birthDate, phone, profession,
                    email, photoURL, password, friend);

            Map<String, Long> returnObject = new HashMap<>();
            returnObject.put("id", userId);

            List<Map> result = new ArrayList<>();
            result.add(returnObject);

            return Response.ok(result).build();

        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/edit")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response userEdit (
            FormDataMultiPart multiPart,
            @FormDataParam("uid") Long uid,
            @FormDataParam("firstName") String firstName,
            @FormDataParam("surname") String surname,
            @FormDataParam("gender") Integer gender,
            @FormDataParam("birthDate") Long birthDate,
            @FormDataParam("phone") String phone,
            @FormDataParam("profession") String profession,
            @FormDataParam("about") String about,
            @FormDataParam("email") String email,
            @FormDataParam("photo") InputStream photo,
            @FormDataParam("photo") FormDataContentDisposition photoDetail,
            @FormDataParam("password") String password) {

        String methodName = "/user/settings";
        try {

            String photoURL = "";

            if (photo != null) {
                ApplicationUtility.writeToFile(photo, com.wds.utils.define.Defines.getImagesPathStorage() + photoDetail.getFileName());
                photoURL = com.wds.utils.define.Defines.getHostImages() + photoDetail.getFileName();
            }

            UserEdit.performRequest(uid, firstName, surname, gender, birthDate, phone, profession,
                    email, photoURL, password);

            List<Map> result = new ArrayList<>();
            return Response.ok(result).build();

        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/industry/list")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getIndustryList() {
        String methodName = "/user/industry/list";
        try {
            return Response.ok(UserIndustryList.performRequest()).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(UserLoginRequest request) {
        String methodName = "/user/login";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            return Response.ok(UserLogin.performRequest(request)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/forgotPassword")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(UserForgotPasswordRequest request) {
        String methodName = "/user/forgotPassword";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            UserForgotPassword.performRequest(request);
            return Response.ok("{}").build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/interests/list/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getInterestsList(@PathParam("uid") Long uid) {
        String methodName = "/user/interests/list/{uid}";
        try {
            return Response.ok(UserInterestsList.performRequest(uid)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/interests/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(UserInterestsSaveRequest request) {
        String methodName = "/user/interests/save";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            UserInterestsSave.performRequest(request);
            return Response.ok("{}").build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/settings/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSettings(@PathParam("uid") Long uid) {
        String methodName = "/user/settings/{uid}";
        try {
            return Response.ok(UserSettings.performRequest(uid)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/settings")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(UserSettingsRequest request) {
        String methodName = "/user/settings";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            UserSettingsSave.performRequest(request);
            return Response.ok("{}").build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @GET
    @Path("/rewards/{uid}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRewards(@PathParam("uid") Long uid) {
        String methodName = "/user/rewards/{uid}";
        try {
            return Response.ok(UserRewards.performRequest(uid)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }

    @POST
    @Path("/set_token")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(SetTokenRequest request) {
        String methodName = "/push/set_token";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            SetToken.performRequest(request);
            return Response.ok(new ArrayList<>(0)).build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
