package com.wds.rest.v1;

import com.wds.utils.define.Defines;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;

@Path("v1/images")
@Produces(MediaType.APPLICATION_JSON)
public class ImagesRest {

    @GET
    @Path("/{imageName}")
    @Produces("image/*")
    public Response getImage(@PathParam("imageName") String imageName) {
        File f = new File(Defines.getImagesPathStorage() + imageName);

        if (!f.exists()) {
            throw new WebApplicationException(404);
        }

        String mt = new MimetypesFileTypeMap().getContentType(f);
        return Response.ok(f, mt).build();
    }
}
