package com.wds.rest;

import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.NotSendingParameterException;
import com.wds.json.v1.ErrorReturn;
import com.wds.json.test.post.PostRequest;
import com.wds.method.test.Post;
import com.wds.utils.ApplicationUtility;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/test")
@Produces(MediaType.APPLICATION_JSON)
public class TestRest {

    @POST
    @Path("/post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response consumeJSON(PostRequest request) {
        String methodName = "/test/post";
        try {
            if (request == null)
                throw new NotSendingParameterException();

            Post.performRequest(request);
            return Response.ok("{}").build();
        } catch (BaseException e) {
            return Response.status(e.getCode()).
                    entity(new ErrorReturn(e.getLocalCode(), e.getMessage(), methodName)).build();
        }
    }
}
