package com.wds.define;

public class Defines {
    public static boolean       DEBUG_MODE = true;

    public final static String  REST_AUTH_USERNAME =        "rest_auth_username";
    public final static String  REST_AUTH_PASSWORD =        "rest_auth_password";

    public static String getSiteHostName() {
        return DEBUG_MODE ? DebugDefines.SITE_HOST_NAME : ReleaseDefines.SITE_HOST_NAME;
    }
}
