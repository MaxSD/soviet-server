package com.wds.exception.v1;

public class WrongValueFlatNumberException extends BaseException {

    public WrongValueFlatNumberException() {
        super();
        this.code = 400;
        this.localCode = 24;
        this.message = "Failed. Wrong value of parameter <flatNumber>!";
    }

}
