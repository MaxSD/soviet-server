package com.wds.exception.v1;

public class WrongValueUidException extends BaseException {

    public WrongValueUidException() {
        super();
        this.code = 400;
        this.localCode = 22;
        this.message = "Failed. Wrong value of parameter <uid>!";
    }

}
