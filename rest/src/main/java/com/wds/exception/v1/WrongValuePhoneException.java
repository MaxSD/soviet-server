package com.wds.exception.v1;

public class WrongValuePhoneException extends BaseException {

    public WrongValuePhoneException() {
        super();
        this.code = 400;
        this.localCode = 18;
        this.message = "Failed. Wrong value of parameter <phone>!";
    }

}
