package com.wds.exception.v1;

public class NotSendingLoginException extends BaseException {

    public NotSendingLoginException() {
        super();
        this.code = 400;
        this.localCode = 17;
        this.message = "Failed. Parameter <login> not sending!";
    }
}
