package com.wds.exception.v1;

public class NotSendingUidException extends BaseException {

    public NotSendingUidException() {
        super();
        this.code = 400;
        this.localCode = 21;
        this.message = "Failed. Parameter <uid> not sending!";
    }
}
