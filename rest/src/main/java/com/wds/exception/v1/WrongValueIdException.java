package com.wds.exception.v1;

public class WrongValueIdException extends BaseException {

    public WrongValueIdException() {
        super();
        this.code = 400;
        this.localCode = 5;
        this.message = "Failed. Wrong value of parameter <id>!";
    }

}
