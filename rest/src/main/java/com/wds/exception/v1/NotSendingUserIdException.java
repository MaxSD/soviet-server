package com.wds.exception.v1;

public class NotSendingUserIdException extends BaseException {

    public NotSendingUserIdException() {
        super();
        this.code = 400;
        this.localCode = 14;
        this.message = "Failed. Parameter <userId> not sending!";
    }
}
