package com.wds.exception.v1;

public class NotSendingGuestNameException extends BaseException {

    public NotSendingGuestNameException() {
        super();
        this.code = 400;
        this.localCode = 32;
        this.message = "Failed. Parameter <guestName> not sending!";
    }
}
