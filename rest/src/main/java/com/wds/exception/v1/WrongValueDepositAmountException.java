package com.wds.exception.v1;

public class WrongValueDepositAmountException extends BaseException {

    public WrongValueDepositAmountException() {
        super();
        this.code = 400;
        this.localCode = 26;
        this.message = "Failed. Wrong value of parameter <depositAmount>!";
    }

}
