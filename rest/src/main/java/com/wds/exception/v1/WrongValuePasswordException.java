package com.wds.exception.v1;

public class WrongValuePasswordException extends BaseException {

    public WrongValuePasswordException() {
        super();
        this.code = 400;
        this.localCode = 20;
        this.message = "Failed. Wrong value of parameter <password>!";
    }

}
