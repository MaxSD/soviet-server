package com.wds.exception.v1;

public class NotSendingServiceIdException extends BaseException {

    public NotSendingServiceIdException() {
        super();
        this.code = 400;
        this.localCode = 29;
        this.message = "Failed. Parameter <serviceId> not sending!";
    }
}
