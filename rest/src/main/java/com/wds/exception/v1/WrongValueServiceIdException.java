package com.wds.exception.v1;

public class WrongValueServiceIdException extends BaseException {

    public WrongValueServiceIdException() {
        super();
        this.code = 400;
        this.localCode = 30;
        this.message = "Failed. Wrong value of parameter <serviceId>!";
    }

}
