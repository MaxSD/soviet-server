package com.wds.exception.v1;

public class NotSendingDepositAmountException extends BaseException {

    public NotSendingDepositAmountException() {
        super();
        this.code = 400;
        this.localCode = 25;
        this.message = "Failed. Parameter <depositAmount> not sending!";
    }
}
