package com.wds.exception.v1;

public class NotSendingFlatNumberException extends BaseException {

    public NotSendingFlatNumberException() {
        super();
        this.code = 400;
        this.localCode = 23;
        this.message = "Failed. Parameter <flatNumber> not sending!";
    }
}
