package com.wds.exception.v1;

public class NotSendingTokenException extends BaseException {

    public NotSendingTokenException() {
        super();
        this.code = 400;
        this.localCode = 29;
        this.message = "Failed. Parameter <token> not sending!";
    }
}
