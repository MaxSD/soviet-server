package com.wds.exception.v1;

public class NotSendingDateException extends BaseException {

    public NotSendingDateException() {
        super();
        this.code = 400;
        this.localCode = 27;
        this.message = "Failed. Parameter <date> not sending!";
    }
}
