package com.wds.exception.v1;

public class WrongValueBillIdException extends BaseException {

    public WrongValueBillIdException() {
        super();
        this.code = 400;
        this.localCode = 34;
        this.message = "Failed. Wrong value of parameter <billId>!";
    }

}
