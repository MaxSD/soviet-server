package com.wds.exception.v1;

public class NotSendingCarNumberException extends BaseException {

    public NotSendingCarNumberException() {
        super();
        this.code = 400;
        this.localCode = 31;
        this.message = "Failed. Parameter <carNumber> not sending!";
    }
}
