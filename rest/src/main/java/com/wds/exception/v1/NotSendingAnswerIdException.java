package com.wds.exception.v1;

public class NotSendingAnswerIdException extends BaseException {

    public NotSendingAnswerIdException() {
        super();
        this.code = 400;
        this.localCode = 36;
        this.message = "Failed. Parameter <answerId> not sending!";
    }
}
