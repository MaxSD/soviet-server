package com.wds.exception.v1;

public class WrongValueDateException extends BaseException {

    public WrongValueDateException() {
        super();
        this.code = 400;
        this.localCode = 28;
        this.message = "Failed. Wrong value of parameter <date>!";
    }

}
