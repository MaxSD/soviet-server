package com.wds.exception.v1;

public class OrderNotFoundException extends BaseException {

    public OrderNotFoundException() {
        super();
        this.code = 404;
        this.localCode = 100;
        this.message = "Failed. Order is not found!";
    }
}
