package com.wds.exception.v1;

public class NotSendingQuestionIdException extends BaseException {

    public NotSendingQuestionIdException() {
        super();
        this.code = 400;
        this.localCode = 35;
        this.message = "Failed. Parameter <questionId> not sending!";
    }
}
