package com.wds.exception.v1;

public class NotSendingBillIdException extends BaseException {

    public NotSendingBillIdException() {
        super();
        this.code = 400;
        this.localCode = 33;
        this.message = "Failed. Parameter <billId> not sending!";
    }
}
