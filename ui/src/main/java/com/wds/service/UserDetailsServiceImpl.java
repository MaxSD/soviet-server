package com.wds.service;

import com.wds.dao.SvtUsersDao;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.SelectDatabaseException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private SvtUsersDao usersDao;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        SvtUser user;

        if (StringUtils.isBlank(login))
            throw new UsernameNotFoundException("User name is empty");

        try {
            user = usersDao.findAdminByEmail(login);
        } catch (SelectDatabaseException ce) {
            throw new UsernameNotFoundException(ce.getMessage());
        }

        // Check is admin or concierge
        if (user.getRole().getId() > 2) {
            throw new UsernameNotFoundException("User name is empty");
        }

        logger.warn("User " + login + " is logged in.");

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                getGrantedAuthorities(user.getRole().getPermissionsNames()));
    }

    public static List<GrantedAuthority> getGrantedAuthorities(String[] roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles)
            authorities.add(new SimpleGrantedAuthority(role));
        return authorities;
    }
}
