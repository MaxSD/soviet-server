package com.wds.service;

import com.wds.dao.SvtUsersDao;
import com.wds.dao.UsersDao;
import com.wds.define.SessionAttr;
import com.wds.entity.SvtUser;
import com.wds.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service("GlobalService")
public class GlobalService {
    private static final SvtUsersDao usersDao = new SvtUsersDao();

    public static SvtUser getLoggedAdmin(HttpServletRequest request)
            throws Exception {
        String email = "anonymousUser";
        if (SecurityContextHolder.getContext().getAuthentication() != null)
            email = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!"anonymousUser".equals(email)) {
            if (request.getSession().getAttribute(SessionAttr.getCurrentAdmin()) == null)
                request.getSession().setAttribute(SessionAttr.getCurrentAdmin(),
                        usersDao.findByEmail(email));
        }
        return (request.getSession().getAttribute(SessionAttr.getCurrentAdmin()) instanceof SvtUser)
                ? (SvtUser) request.getSession().getAttribute(SessionAttr.getCurrentAdmin())
                : null;
    }
}
