package com.wds.jstl;

import com.wds.entity.Role;

import java.io.IOException;
import java.util.Map;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

@SuppressWarnings({"UnusedDeclaration", "StringConcatenationInsideStringBufferAppend"})
public class RolePermissionsListWithCheckedTag extends SimpleTagSupport {
    private Map<Long, String> items;
    private Role role;
    private boolean isReadOnly;
    private String path = "_";
    private String dataTextOn = "On";
    private String dataTextOff = "Off";

    public void doTag() throws JspException {
        try {
            JspWriter out = getJspContext().getOut();
            int i=1;
            for (Map.Entry el : items.entrySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append("<span>");
                sb.append("<input id=\"");
                sb.append(i);
                sb.append("\"");
                sb.append(" name=\"permissionsList\" type=\"checkbox\" value=\"");
                sb.append(el.getKey());
                sb.append("\"");
                role.getPermissionsList().stream().filter(permission -> el.getKey() == permission.getId()).forEach(permission -> {
                    sb.append(" checked ");
                });
                if (role.getId() == 1) {
                    sb.append(" readonly ");
                }

                if (isReadOnly) sb.append(" disabled ");

                sb.append(" data-on-text=\""+dataTextOn+"\" ");
                sb.append(" data-off-text=\""+dataTextOff+"\" ");
                sb.append(" data-size=\"mini\"/>");
                sb.append("<label for=\"permissionsList");
                sb.append(i);
                sb.append("\">");
                sb.append(el.getValue());
                sb.append("</label>");
                sb.append("</span><br/>");

                out.print(sb.toString());
                i++;
            }
            out.println("<input type=\"hidden\" name=\""+path+"\" value=\"on\"/>");
        } catch(IOException ioe) {
            throw new JspException("Error: " + ioe.getMessage());
        }
    }

    public void setItems(Map<Long, String> items) {
        this.items = items;
    }
    public void setRole(Role role) { this.role = role; } public Role getRole() { return role; }
    public void setIsReadOnly(boolean isReadOnly) {
        this.isReadOnly = isReadOnly;
    }
    public void setPath(String path) {
        this.path += path;
    }
    public void setDataTextOn(String dataTextOn) {
        this.dataTextOn = dataTextOn;
    }
    public void setDataTextOff(String dataTextOff) {
        this.dataTextOff = dataTextOff;
    }
}
