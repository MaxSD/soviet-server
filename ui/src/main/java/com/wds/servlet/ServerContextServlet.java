package com.wds.servlet;

import com.wds.database.HibernateUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import com.wds.utils.push.apple.ApplePushModeEnum;

public class ServerContextServlet implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        HibernateUtil.getSession().close();
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        com.wds.define.Defines.DEBUG_MODE = true;
        com.wds.utils.define.Defines.DEBUG_MODE = false;
        com.wds.utils.define.Defines.PUSH_MODE = ApplePushModeEnum.PRODUCTION;
    }
}