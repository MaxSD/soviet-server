package com.wds.define;

public class Defines {
    public static boolean       DEBUG_MODE = true;
    // MESSAGE
    public final static String  ERROR_MESSAGE =             "errorMessage";
    public final static String  SUCCESS_MESSAGE =           "successMessage";

    public static boolean getDEBUG_MODE() { return DEBUG_MODE; }

    public static void setDEBUG_MODE(boolean debugMode) {
        DEBUG_MODE = debugMode;
    }
}
