package com.wds.define;

public class SessionAttr {
    // Global
    private final static String CURRENT_ADMIN =  "current_admin";

    public static String getCurrentAdmin() {
        return CURRENT_ADMIN;
    }
}
