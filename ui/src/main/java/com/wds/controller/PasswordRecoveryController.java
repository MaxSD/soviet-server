package com.wds.controller;

import com.wds.dao.UsersDao;
import com.wds.dao.PasswordRecoveryDao;
import com.wds.define.Defines;
import com.wds.entity.User;
import com.wds.entity.PasswordRecovery;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.SenderEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;

@Controller
@RequestMapping(value = "/passwordRecovery")
public class PasswordRecoveryController {

    @Autowired
    private UsersDao usersDao;
    @Autowired
    private PasswordRecoveryDao passwordRecoveryDao;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private SenderEmail senderEmail;

    @RequestMapping(value = "/email_request", method = RequestMethod.GET)
    public String emailRequest() {
        return "passwordRecovery/emailRequest";
    }

    @RequestMapping(value = "/email_request", method = RequestMethod.POST)
    public String emailRequest(HttpServletRequest request,
                               ServletRequest servletRequest,
                               @RequestParam("userEmail") String userEmail,
                               Locale locale,
                               RedirectAttributes redirectAttributes) {

        try {
            User user = usersDao.findByEmail(userEmail);
            if (user == null)
                throw new Exception("Incorrect Email.");

            PasswordRecovery passwordRecovery = new PasswordRecovery();
            passwordRecovery.setDateTime(new Timestamp(new Date().getTime()));
            passwordRecovery.setSessionId(request.getSession().getId());
            passwordRecovery.setUserEmail(userEmail);
            passwordRecoveryDao.delete(request.getSession().getId());
            passwordRecoveryDao.create(passwordRecovery);

            StringBuilder message = new StringBuilder("Hello, follow the link below to change your password: \r\n");
            message.append("http://");
            message.append(servletRequest.getServerName());
            if (servletRequest.getServerPort() > 0) {
                message.append(":");
                message.append(servletRequest.getServerPort());
            }
            message.append(request.getContextPath());
            message.append("/passwordRecovery/");
            message.append(request.getSession().getId());
            message.append("/change_password");

            senderEmail.sendEmail(userEmail, "Password recovery", message.toString(), false);

            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.PasswordRecovery.EmailSent", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/passwordRecovery/email_request";
        }

        return "redirect:/passwordRecovery/email_request";
    }

    @RequestMapping(value = "{sessionId}/change_password", method = RequestMethod.GET)
    public String changePassword(@PathVariable String sessionId,
                                 Model model) {
        model.addAttribute("sessionId", sessionId);
        return "passwordRecovery/changePassword";
    }

    @RequestMapping(value = "/change_password", method = RequestMethod.POST)
    public String changePassword(@RequestParam("sessionId") String sessionId,
                                 @RequestParam("password") String password,
                                 @RequestParam("password") String confirmPassword,
                                 Model model,
                                 Locale locale,
                                 RedirectAttributes redirectAttributes) {

        try {
            if (!password.equals(confirmPassword))
                throw new Exception(messageSource.getMessage("UI.Error.PasswordsDoNotMatch", null, locale));

            PasswordRecovery passwordRecovery = passwordRecoveryDao.findBySessionId(sessionId);
            if (passwordRecovery == null)
                throw new Exception(messageSource.getMessage("UI.Error.IncorrectLink", null, locale));

            User user = usersDao.findByEmail(passwordRecovery.getUserEmail());
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));

            user.setPassword(ApplicationUtility.bcrypt(password));
            usersDao.update(user);

            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.PasswordRecovery.PasswordChanged", null, locale));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "passwordRecovery/changePassword";
        }
        return "redirect:/";
    }
}
