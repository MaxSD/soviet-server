package com.wds.controller;

import com.wds.dao.PermissionsDao;
import com.wds.dao.RolesDao;
import com.wds.dao.UsersDao;
import com.wds.define.Defines;
import com.wds.entity.Permission;
import com.wds.entity.Role;
import com.wds.entity.User;
import com.wds.service.RolePermissionConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping(value = "/administrator/roles")
public class RoleController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UsersDao usersDao;
    @Autowired
    private RolesDao rolesDao;
    @Autowired
    private PermissionsDao permissionsDao;
    @Autowired
    private RolePermissionConverter rolePermissionConverter;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Permission.class, this.rolePermissionConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "roles";
    }

    @ModelAttribute("currentUser")
    public User getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return usersDao.findByLogin(principal.getUsername());
        } catch(Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/list_roles", method = RequestMethod.GET)
    public String listRoles(Model model, Principal principal, Locale locale) {
        if (principal==null) return "login";
        try {
            model.addAttribute("rolesList", rolesDao.getFullDataList(locale.getLanguage()));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/roles/listRoles";
    }

    @RequestMapping(value = "/new_role", method = RequestMethod.GET)
    public String newRole(Model model, Principal principal, Locale locale) {
        if (principal==null) return "login";
        Map<Long, String> permissionsList = null;
        try {
            permissionsList = permissionsDao.getAll(locale.getLanguage());
            Role roleCreateForm = new Role();
            model.addAttribute("roleCreateForm", roleCreateForm);
            model.addAttribute("permissionsList", permissionsList);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("permissionsList", permissionsList);
        }
        return "administrator/roles/newRole";
    }

    @RequestMapping(value = "/new_role", method = RequestMethod.POST)
    public String newRole(@ModelAttribute Role roleCreateForm,
                          @RequestParam("textRU") String textRU,
                          @RequestParam("textEN") String textEN,
                          @RequestParam("descriptionRU") String descriptionRU,
                          @RequestParam("descriptionEN") String descriptionEN,
                          BindingResult bindingResult,
                          Model model,
                          Locale locale,
                          Principal principal,
                          RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        Map<Long, String> permissionsList = null;
        try {
            permissionsList = rolesDao.getAll(locale.getLanguage());

            if (bindingResult.hasErrors())
                throw new Exception(bindingResult.getFieldError().getField());

            Map<String, String> fieldText = new HashMap<>();
            fieldText.put("ru", textRU);
            fieldText.put("en", textEN);
            roleCreateForm.setText(fieldText);

            Map<String, String> fieldDescription = new HashMap<>();
            fieldDescription.put("ru", descriptionRU);
            fieldDescription.put("en", descriptionEN);
            roleCreateForm.setDescription(fieldDescription);

            rolesDao.create(roleCreateForm);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Role.CreatedSuccess", null, locale));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("roleCreateForm", roleCreateForm);
            model.addAttribute("permissionsList", permissionsList);
            return "administrator/roles/newRole";
        }
        return "redirect:/administrator/roles/list_roles";
    }

    @RequestMapping(value = "{roleId}/edit_role", method = RequestMethod.GET)
    public String editRole(@PathVariable long roleId,
                            Model model, Principal principal, Locale locale) {
        if (principal==null) return "login";
        Map<Long, String> permissionsList = null;
        try {
            permissionsList = permissionsDao.getAll(locale.getLanguage());
            Role role = rolesDao.findById(roleId);
            model.addAttribute("roleEditForm", role);
            model.addAttribute("permissionsList", permissionsList);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("permissionsList", permissionsList);
        }
        return "administrator/roles/editRole";
    }

    @RequestMapping(value = "{roleId}/edit_role", method = RequestMethod.POST)
    public String editRole(@ModelAttribute Role roleEditForm,
                           @PathVariable long roleId,
                           @RequestParam("textRU") String textRU,
                           @RequestParam("textEN") String textEN,
                           @RequestParam("descriptionRU") String descriptionRU,
                           @RequestParam("descriptionEN") String descriptionEN,
                           BindingResult bindingResult,
                           Locale locale,
                           Principal principal,
                           RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        try {
            if (bindingResult.hasErrors())
                throw new Exception(bindingResult.getFieldError().getField());

            Role role = rolesDao.findById(roleId);

            if (role == null)
                throw new Exception(messageSource.getMessage("UI.Error.RoleNotFound", null, locale));

            roleEditForm.setId(roleId);

            Map<String, String> fieldText = new HashMap<>();
            fieldText.put("ru", textRU);
            fieldText.put("en", textEN);
            roleEditForm.setText(fieldText);

            Map<String, String> fieldDescription = new HashMap<>();
            fieldDescription.put("ru", descriptionRU);
            fieldDescription.put("en", descriptionEN);
            roleEditForm.setDescription(fieldDescription);

            rolesDao.update(roleEditForm);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Role.UpdatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/roles/"+roleId+"/edit_role";
        }
        return "redirect:/administrator/roles/list_roles";
    }

    @RequestMapping(value = "{roleId}/view_role", method = RequestMethod.GET)
    public String viewRole(@PathVariable long roleId,
                           Model model, Principal principal, Locale locale,
                           RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        Map<Long, String> permissionsList;
        try {
            permissionsList = permissionsDao.getAll(locale.getLanguage());
            Role role = rolesDao.findById(roleId);
            if (role == null)
                throw new Exception(messageSource.getMessage("UI.Error.RoleNotFound", null, locale));

            model.addAttribute("roleViewForm", role);
            model.addAttribute("permissionsList", permissionsList);
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/roles/list_roles";
        }
        return "administrator/roles/viewRole";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{roleId}/delete_role", method = RequestMethod.POST)
    public String deleteRole(@PathVariable long roleId,
                             Model model,
                             Principal principal,
                             Locale locale,
                             RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        try {
            Role role = rolesDao.findById(roleId);
            if (role == null)
                throw new Exception(messageSource.getMessage("UI.Error.RoleNotFound", null, locale));
            if (role.getId() == 1)
                throw new Exception(messageSource.getMessage("UI.Warning.YouCannotDeleteRole", null, locale));
            rolesDao.delete(role);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Role.DeletedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/roles/list_roles";
        }
        return "redirect:/administrator/roles/list_roles";
    }
}
