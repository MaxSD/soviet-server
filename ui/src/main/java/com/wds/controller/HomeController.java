package com.wds.controller;

import com.wds.dao.*;
import com.wds.define.Defines;
import com.wds.entity.SvtDialog;
import com.wds.entity.SvtEvent;
import com.wds.entity.SvtOrder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@SuppressWarnings("SameReturnValue")
@Controller
@RequestMapping(value = "/*")
public class HomeController {

    @ModelAttribute("page")
    String getPage() {
        return "dashboard";
    }

    @RequestMapping(value = "/")
    public String home(Model model, Principal principal) {
        if (principal == null) return "login";

        try {
            SvtUsersDao usersDao = new SvtUsersDao();
            model.addAttribute("acceptedUsersCount", usersDao.getAcceptedUsersCount());
            model.addAttribute("activeUsersCount", usersDao.getActiveUsersCount());

            Dao dao = new Dao();
            SvtDialogsDao dialogsDao = new SvtDialogsDao();
            model.addAttribute("dialogsCount", dao.getCount(SvtDialog.class));
            model.addAttribute("activeDialogsCount", dialogsDao.getActiveDialogsCount());
            model.addAttribute("closedDialogsCount", dialogsDao.getClosedDialogsCount());

            SvtOrdersDao ordersDao = new SvtOrdersDao();
            model.addAttribute("ordersCount", dao.getCount(SvtOrder.class));
            model.addAttribute("paidOrdersAmountSum", ordersDao.getPaidOrdersTotalAmount());

            SvtEventsDao eventsDao = new SvtEventsDao();
            model.addAttribute("events", eventsDao.getEventsForChart());
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "dashboard";
        }
        return "dashboard";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/j_spring_security_exit_user", method = RequestMethod.GET)
    public String switchUserBack(Model model, Principal principal) {
        if (principal == null) return "login";
        return "dashboard";
    }

    @RequestMapping("/403")
    public String _403() {
        return "403";
    }

    @RequestMapping("/404")
    public String _404() {
        return "404";
    }
}
