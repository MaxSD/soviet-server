package com.wds.controller;

import com.wds.dao.Dao;
import com.wds.dao.SettingsDao;
import com.wds.dao.SvtUserTokenDao;
import com.wds.dao.SvtUsersDao;
import com.wds.define.Defines;
import com.wds.entity.*;
import com.wds.service.UserRoleConverter;
import com.wds.utils.SenderEmail;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.StringWriter;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/administrator/settings")
public class SettingsController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private Dao dao;
    @Autowired
    private SettingsDao settingsDao;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "settings";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal,
                             RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            model.addAttribute("scoreText", settingsDao.findByParamName("score_text").getParamValue());
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/settings/settings";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String sendPush(
            @RequestParam("scoreText") String scoreText,
            Model model,
            Principal principal,
            Locale locale,
            RedirectAttributes redirectAttributes) {

        try {
            if (principal == null) throw new Exception("Please, login first!");

            SettingsDao settingsDao = new SettingsDao();
            Settings settings = settingsDao.findByParamName("score_text");

            if (settings != null) {
                settings.setParamValue(scoreText);
                dao.save(settings);
            }

            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.SettingsUpdated", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "redirect:/administrator/settings/settings";
    }
}
