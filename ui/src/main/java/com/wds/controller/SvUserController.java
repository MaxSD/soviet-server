package com.wds.controller;

import com.wds.dao.*;
import com.wds.dataObjects.UsersOnEventFormData;
import com.wds.dataObjects.UsersOnPartnerFormData;
import com.wds.define.Defines;
import com.wds.entity.Role;
import com.wds.entity.SvtEvent;
import com.wds.entity.SvtUser;
import com.wds.service.UserRoleConverter;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping(value = "/administrator/svUser")
public class SvUserController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private Dao dao;
    @Autowired
    private RolesDao rolesDao;
    @Autowired
    private SvtUsersDao svtUsersDao;
    @Autowired
    private SvtUserEventsDao userEventsDao;
    @Autowired
    private SvtUserPartnersDao userPartnersDao;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "svUser";
    }

    @ModelAttribute("currentUser")
    public SvtUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return svtUsersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal) {
        if (principal == null) return "login";
        return "administrator/svUser/list";
    }

    @RequestMapping(value = "/get_all_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsers(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "roleId", defaultValue = "0", required = false) long roleId,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<SvtUser> list;
            if (roleId > 0)
                list = svtUsersDao.getUsersListByRoleId(roleId, start, length);
            else
                list = dao.get(SvtUser.class, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            if (roleId > 0) {
                jsonObject.put("recordsTotal", svtUsersDao.getCount(roleId));
                jsonObject.put("recordsFiltered", svtUsersDao.getCount(roleId));
            } else {
                jsonObject.put("recordsTotal", dao.getCount(SvtUser.class));
                jsonObject.put("recordsFiltered", dao.getCount(SvtUser.class));
            }

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (SvtUser row : list) {

                jsonDataArray = new JSONArray();

                jsonDataArray.put(row.getId());

                if (!StringUtils.isBlank(row.getPhoto()))
                    jsonDataArray.put("<img src=\"" + row.getPhoto() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");

                jsonDataArray.put(row.getFirstName() + " " + row.getSurname());
                jsonDataArray.put(ApplicationUtility.secondsToFormattedString(
                        row.getBirthDate(),
                        ApplicationUtility.GMT_0,
                        "dd.MM.yyyy"));
                jsonDataArray.put(row.getAbout());
                jsonDataArray.put(row.getProfession());
                jsonDataArray.put(row.getEmail());
                jsonDataArray.put(row.getPhone());
                jsonDataArray.put(row.getCardNumber());

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");

                if (getLoggedUser().getRole().getId() == com.wds.database.define.Defines.USER_TYPE_ADMIN) {
                    sbActions.append("<a class=\"btn btn-xs btn-default\" href=\"" + request.getContextPath() + "/administrator/svUser/" + row.getId() + "/view\">&nbsp;<i class=\"fa fa-info\"></i>&nbsp;</a>");
                    sbActions.append("<a class=\"btn btn-xs btn-default\" href=\"" + request.getContextPath() + "/administrator/svUser/" + row.getId() + "/edit\">&nbsp;<i class=\"fa fa-edit\"></i>&nbsp;</a>");
                    if (row.getId() != 1)
                        sbActions.append("<a href=\"#\" class=\"btn btn-xs btn-default\" onclick=\"deleteUser('" + row.getId() + "', '" + row.getFirstName() + " " + row.getSurname() + "');\">&nbsp;<i class=\"fa fa-remove\"></i>&nbsp;</a>");
                }

                jsonDataArray.put(sbActions.toString());
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_users_on_event_form_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersOnEventForm(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "roleId", defaultValue = "0", required = false) long roleId,
            @RequestParam(value = "eventId", defaultValue = "0", required = false) long eventId,
            Principal principal,
            Locale locale,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<UsersOnEventFormData> list;
            list = userEventsDao.getUsersListByEventId(eventId, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", userEventsDao.getCount(eventId));
            jsonObject.put("recordsFiltered", userEventsDao.getCount(eventId));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (UsersOnEventFormData row : list) {

                jsonDataArray = new JSONArray();

                if (!StringUtils.isBlank(row.getPhoto()))
                    jsonDataArray.put("<img src=\"" + row.getPhoto() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");

                jsonDataArray.put(row.getUserName());
                jsonDataArray.put(row.getEmail());
                jsonDataArray.put(row.getPhone());
                jsonDataArray.put(row.getCardNumber());

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");

                if (row.getUserEventId() == null) {
                    sbActions.append("</br><a href=\"#\" onclick=\"userHasVisitedEvent('" + row.getUserId() + "');\"><button class=\"btn btn-xs btn-success pull-right\" type=\"button\">" +
                            messageSource.getMessage("Btn.Visited", null, locale) +
                            "</button></span></a>");
                }

                jsonDataArray.put(sbActions.toString());
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_users_visited_partner_form_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsersVisitedPartnerForm(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "partnerId", defaultValue = "0", required = false) long partnerId,
            Principal principal,
            Locale locale,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<UsersOnPartnerFormData> list;
            list = userPartnersDao.getUsersListByPartnerId(partnerId, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", userPartnersDao.getCount(partnerId));
            jsonObject.put("recordsFiltered", userPartnersDao.getCount(partnerId));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (UsersOnPartnerFormData row : list) {

                jsonDataArray = new JSONArray();

                if (!StringUtils.isBlank(row.getPhoto()))
                    jsonDataArray.put("<img src=\"" + row.getPhoto() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");

                jsonDataArray.put(row.getUserName());
                jsonDataArray.put(row.getEmail());
                jsonDataArray.put(row.getPhone());
                jsonDataArray.put(row.getCardNumber());

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                jsonDataArray.put(sbActions.toString());

                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newObject(Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            SvtEvent createForm = new SvtEvent();
            model.addAttribute("createForm", createForm);

            Map<Long, String> rolesList = rolesDao.getAll(locale.getLanguage());
            model.addAttribute("rolesList", rolesList);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/svUser/new";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String newObject(@RequestParam String firstName,
                            @RequestParam String surname,
                            @RequestParam Integer gender,
                            @RequestParam String phone,
                            @RequestParam String birthDate,
                            @RequestParam String profession,
                            @RequestParam String about,
                            @RequestParam("photo") MultipartFile photo,
                            @RequestParam String cardNumber,
                            @RequestParam String email,
                            @RequestParam String password,
                            @RequestParam long role,
                            Model model,
                            Locale locale,
                            Principal principal,
                            RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtUser svtUser = svtUsersDao.findByCardNumber(cardNumber);
            if (svtUser != null)
                throw new Exception("User with card number '" + cardNumber + "' already exists.");

            svtUser = new SvtUser();
            String photoFileName = "";

            /**
             * Upload photo
             */
            if (photo != null && !StringUtils.isBlank(photo.getOriginalFilename())) {
                photoFileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(photo.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(photo.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                photoFileName);

                svtUser.setPhoto(com.wds.utils.define.Defines.getHostImages() + photoFileName);
            }

            svtUser.setDateRegistration(ApplicationUtility.getCurrentTimeStampGMT_0());
            svtUser.setFirstName(firstName);
            svtUser.setSurname(surname);
            svtUser.setGender(gender);
            svtUser.setPhone(phone);
            svtUser.setBirthDate(ApplicationUtility.formattedStringToDate(
                    birthDate, ApplicationUtility.GMT_0, "dd.MM.yyyy").getTime() / 1000);
            svtUser.setProfession(profession);
            svtUser.setAbout(about);
            svtUser.setCardNumber(cardNumber);
            svtUser.setEmail(email);
            svtUser.setPassword(ApplicationUtility.md5crypt(password));

            RolesDao rolesDao = new RolesDao();
            svtUser.setRole(rolesDao.findById(role));

            dao.create(svtUser);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.CreatedSuccess", null, locale));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "administrator/svUser/new";
        }
        return "redirect:/administrator/svUser/list";
    }

    @RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
    public String editObject(@PathVariable long id,
                             Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        Map<Long, String> rolesList = null;
        try {
            SvtUser svtUser = dao.find(SvtUser.class, id);

            rolesList = rolesDao.getAll(locale.getLanguage());
            model.addAttribute("rolesList", rolesList);
            model.addAttribute("roleName", svtUser.getRole().getText().get(locale.getLanguage()));

            model.addAttribute("editForm", svtUser);
            model.addAttribute("birthDate", ApplicationUtility.secondsToFormattedString(
                    svtUser.getBirthDate(),
                    ApplicationUtility.GMT_0,
                    "dd.MM.yyyy"));
            boolean isAdmin = false;
            if (getLoggedUser().getRole().getId() == com.wds.database.define.Defines.USER_TYPE_ADMIN) {
                isAdmin = true;
            }
            model.addAttribute("isAdmin", isAdmin);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/svUser/edit";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{id}/edit", method = RequestMethod.POST)
    public String editObject(@PathVariable long id,
                             @RequestParam String firstName,
                             @RequestParam String surname,
                             @RequestParam Integer gender,
                             @RequestParam String phone,
                             @RequestParam String birthDate,
                             @RequestParam String profession,
                             @RequestParam String about,
                             @RequestParam("photo") MultipartFile photo,
                             @RequestParam String cardNumber,
                             @RequestParam String email,
                             @RequestParam String password,
                             @RequestParam long role,
                             Model model,
                             Locale locale,
                             Principal principal,
                             RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtUser svtUser = svtUsersDao.findByCardNumber(cardNumber);
            if (svtUser != null && svtUser.getId() != id)
                throw new Exception("User with card number '" + cardNumber + "' already exists.");

            svtUser = dao.find(SvtUser.class, id);
            if (svtUser == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            svtUser.setId(id);

            String photoFileName = "";

            /**
             * Upload photo
             */
            if (photo != null && !StringUtils.isBlank(photo.getOriginalFilename())) {
                photoFileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(photo.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(photo.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                photoFileName);

                svtUser.setPhoto(com.wds.utils.define.Defines.getHostImages() + photoFileName);
            }

            svtUser.setFirstName(firstName);
            svtUser.setSurname(surname);
            svtUser.setGender(gender);
            svtUser.setPhone(phone);
            svtUser.setBirthDate(ApplicationUtility.formattedStringToDate(
                    birthDate, ApplicationUtility.GMT_0, "dd.MM.yyyy").getTime() / 1000);
            svtUser.setProfession(profession);
            svtUser.setAbout(about);
            svtUser.setCardNumber(cardNumber);
            svtUser.setEmail(email);

            if (!StringUtils.isBlank(password))
                svtUser.setPassword(ApplicationUtility.md5crypt(password));

            RolesDao rolesDao = new RolesDao();
            svtUser.setRole(rolesDao.findById(role));

            dao.create(svtUser);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.UpdatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/svUser/" + id + "/edit";
        }
        return "redirect:/administrator/svUser/list";
    }

    @RequestMapping(value = "{id}/view", method = RequestMethod.GET)
    public String viewObject(@PathVariable long id,
                             Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        Map<Long, String> rolesList = null;
        try {
            SvtUser svtUser = dao.find(SvtUser.class, id);

            rolesList = rolesDao.getAll(locale.getLanguage());
            model.addAttribute("rolesList", rolesList);
            model.addAttribute("roleName", svtUser.getRole().getText().get(locale.getLanguage()));

            model.addAttribute("viewForm", svtUser);
            model.addAttribute("birthDate", ApplicationUtility.secondsToFormattedString(
                    svtUser.getBirthDate(),
                    ApplicationUtility.GMT_0,
                    "dd.MM.yyyy"));
            boolean isAdmin = false;
            if (getLoggedUser().getRole().getId() == com.wds.database.define.Defines.USER_TYPE_ADMIN) {
                isAdmin = true;
            }
            model.addAttribute("isAdmin", isAdmin);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/svUser/view";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{id}/delete_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String deleteObjectAjax(@PathVariable long id,
                                   Model model,
                                   Principal principal,
                                   Locale locale,
                                   RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtUser svtUser = dao.find(SvtUser.class, id);
            if (svtUser == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            dao.delete(svtUser);
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.User.DeletedSuccess", null, locale)) + "</p>";
    }
}
