package com.wds.controller;

import com.wds.dao.RolesDao;
import com.wds.dao.UsersDao;
import com.wds.define.Defines;
import com.wds.entity.User;
import com.wds.entity.Role;
import com.wds.service.UserRoleConverter;
import com.wds.utils.ApplicationUtility;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping(value = "/administrator/users")
public class UserController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UsersDao usersDao;
    @Autowired
    private RolesDao rolesDao;
    @Autowired
    private UserRoleConverter userRoleConverter;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "users";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal==null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/list_users", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal) {
        if (principal==null) return "login";
        try {
            model.addAttribute("usersList", usersDao.getAll());
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/users/listUsers";
    }

    @RequestMapping(value = "/new_user", method = RequestMethod.GET)
    public String newUser(Model model, Principal principal, Locale locale) {
        if (principal==null) return "login";
        Map<Long, String> rolesList = null;
        try {
            rolesList = rolesDao.getAll(locale.getLanguage());
            User userCreateForm = new User();
            model.addAttribute("userCreateForm", userCreateForm);
            model.addAttribute("rolesList", rolesList);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("rolesList", rolesList);
        }
        return "administrator/users/newUser";
    }

    @RequestMapping(value = "/new_user", method = RequestMethod.POST)
    public String newUser(@ModelAttribute User userCreateForm,
                           @RequestParam("confirmPassword") String confirmPassword,
                           BindingResult bindingResult,
                           Model model,
                           Locale locale,
                           Principal principal,
                           RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        Map<Long, String> rolesList = null;
        try {
            rolesList = rolesDao.getAll(locale.getLanguage());

            if (bindingResult.hasErrors())
                throw new Exception(bindingResult.getFieldError().getField());
            if (StringUtils.isBlank(userCreateForm.getPassword()))
                throw new Exception(messageSource.getMessage("UI.Error.EmptyPassword", null, locale));
            if (!userCreateForm.getPassword().equals(confirmPassword))
                throw new Exception(messageSource.getMessage("UI.Error.PasswordsDoNotMatch", null, locale));

            userCreateForm.setPassword(ApplicationUtility.bcrypt(userCreateForm.getPassword()));
            usersDao.create(userCreateForm);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.CreatedSuccess", null, locale));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("userCreateForm", userCreateForm);
            model.addAttribute("userRolesList", rolesList);
            return "administrator/users/newUser";
        }
        return "redirect:/administrator/users/list_users";
    }

    @RequestMapping(value = "{userId}/edit_user", method = RequestMethod.GET)
    public String editUser(@PathVariable long userId,
                            Model model, Principal principal, Locale locale) {
        if (principal==null) return "login";
        Map<Long, String> rolesList = null;
        try {
            rolesList = rolesDao.getAll(locale.getLanguage());
            User user = usersDao.findById(userId);
            model.addAttribute("userEditForm", user);
            model.addAttribute("rolesList", rolesList);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            model.addAttribute("rolesList", rolesList);
        }
        return "administrator/users/editUser";
    }

    @RequestMapping(value = "{userId}/edit_user", method = RequestMethod.POST)
    public String editUser(@ModelAttribute User userEditForm,
                            @PathVariable long userId,
                            @RequestParam("confirmPassword") String confirmPassword,
                           BindingResult bindingResult,
                           Locale locale,
                           Principal principal,
                           RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        try {
            if (bindingResult.hasErrors())
                throw new Exception(bindingResult.getFieldError().getField());

            User user = usersDao.findById(userId);

            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));

            userEditForm.setId(userId);

            if (!StringUtils.isBlank(userEditForm.getPassword())) {
                if (!userEditForm.getPassword().equals(confirmPassword))
                    throw new Exception(messageSource.getMessage("UI.Error.PasswordsDoNotMatch", null, locale));
                userEditForm.setPassword(ApplicationUtility.bcrypt(userEditForm.getPassword()));
            } else userEditForm.setPassword(user.getPassword());

            usersDao.update(userEditForm);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.UpdatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/"+userId+"/edit_user";
        }
        return "redirect:/administrator/users/list_users";
    }

    @RequestMapping(value = "{userId}/view_user", method = RequestMethod.GET)
    public String viewUser(@PathVariable long userId,
                            Model model, Principal principal, Locale locale,
                            RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        try {
            User user = usersDao.findById(userId);
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));

            model.addAttribute("userViewForm", user);
            model.addAttribute("roleName", user.getRole().getText().get(locale.getLanguage()));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/users/list_users";
        }
        return "administrator/users/viewUser";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{userId}/delete_user", method = RequestMethod.POST)
    public String deleteUser(@PathVariable long userId,
                            Model model,
                            Principal principal,
                            Locale locale,
                            RedirectAttributes redirectAttributes) {
        if (principal==null) return "login";
        try {
            User user = usersDao.findById(userId);
            if (user == null)
                throw new Exception(messageSource.getMessage("UI.Error.UserNotFound", null, locale));
            if (user.getId() == 1)
                throw new Exception(messageSource.getMessage("UI.Warning.YouCannotDeleteUser", null, locale));
            usersDao.delete(user);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.User.DeletedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "redirect:/administrator/users/list_users";
    }
}
