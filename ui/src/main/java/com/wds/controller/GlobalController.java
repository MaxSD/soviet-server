package com.wds.controller;

import com.wds.entity.SvtUser;
import com.wds.entity.User;
import com.wds.service.GlobalService;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalController {

    @ModelAttribute("admin")
    SvtUser getCurrentAdmin(HttpServletRequest request)
            throws Exception {
        return GlobalService.getLoggedAdmin(request);
    }

//    @ModelAttribute("photoAdminUrl")
//    String getHostImageAdmin() {
//        return Define.getHostMedia()+Define.getFolderPhotoAdmin();
//    }

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;
        ModelAndView modelAndView = new ModelAndView("exception");
        modelAndView.addObject("exceptionObject", e);
        modelAndView.addObject("exceptionUrl", req.getRequestURL());
        return modelAndView;
    }
}