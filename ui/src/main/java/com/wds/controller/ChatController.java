package com.wds.controller;

import com.wds.dao.*;
import com.wds.dataObjects.DialogsListElemData;
import com.wds.dataObjects.MessagesListElemData;
import com.wds.database.define.Defines;
import com.wds.entity.*;
import com.wds.service.UserRoleConverter;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.push.apple.ApplePushNotificationNewMessageThread;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/administrator/dialog")
public class ChatController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private Dao dao;
    @Autowired
    private SvtUsersDao svtUsersDao;
    @Autowired
    private SvtDialogsDao dialogsDao;
    @Autowired
    private SvtMessagesDao messagesDao;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "dialog";
    }

    @ModelAttribute("currentUser")
    public SvtUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return svtUsersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal) {
        if (principal == null) return "login";
        return "administrator/dialog/list";
    }

    @RequestMapping(value = "/get_all_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getDialogsList(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            Principal principal,
            Locale locale,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            SvtUser loggedUser = getLoggedUser();

            List<Object> list;
            if (loggedUser.getRole().getId() == Defines.USER_TYPE_MODERATOR)
                list = dialogsDao.getList(start, length, false, DialogsListElemData.class);
            else
                list = dialogsDao.getList(start, length, null, DialogsListElemData.class);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", dao.getCount(SvtDialog.class));
            jsonObject.put("recordsFiltered", dao.getCount(SvtDialog.class));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (Object rowData : list) {
                DialogsListElemData row = (DialogsListElemData) rowData;

                jsonDataArray = new JSONArray();
                jsonDataArray.put((row.getUnreadCount() > 0 ? "<div class=\"badge bg-color-red pull-left\">" + row.getUnreadCount() + "</div>&nbsp;" : "") + row.getTitle());
                jsonDataArray.put(row.getUserName());
                jsonDataArray.put("<img src=\"" + row.getUserPhoto() + "\" width=\"64\" height=\"64\"/>");

                if (row.getDate() != null) {
                    jsonDataArray.put(ApplicationUtility.secondsToFormattedString(row.getDate(),
                            ApplicationUtility.GMT_0,
                            "dd.MM.yyyy HH:mm") + "&nbsp;<span style=\"color: #425070;\">МСК</span>");

                    if (row.getMessageType() == com.wds.database.define.Defines.MESSAGE_TYPE_TEXT)
                        jsonDataArray.put(row.getLastMessage());
                    else if (row.getMessageType() == com.wds.database.define.Defines.MESSAGE_TYPE_LINK)
                        jsonDataArray.put("<a target=\"_blank\" href=\"" + row.getLastMessage() + "\"><img src=\"" + row.getLastMessage() + "\" width=\"200\"/></a>");
                    else
                        jsonDataArray.put("");
                } else {
                    jsonDataArray.put("");
                    jsonDataArray.put("");
                }

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");

                if (row.getDialogIsClosed()) {
                    sbActions.append("</br><a href=\"#\" onclick=\"openMessagesForm('" + row.getDialogId() + "', '" + row.getTitle() + "');\"><button class=\"btn btn-xs btn-warning pull-right\" type=\"button\">" +
                            messageSource.getMessage("Btn.View", null, locale) +
                            "</button></span></a>");
                } else {
                    sbActions.append("</br><a href=\"#\" onclick=\"openMessagesForm('" + row.getDialogId() + "', '" + row.getTitle() + "');\"><button class=\"btn btn-xs btn-success pull-right\" type=\"button\">" +
                            messageSource.getMessage("Btn.View", null, locale) +
                            "</button></span></a>");
                }

                jsonDataArray.put(sbActions.toString());
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/open_dialog_messages_form", method = RequestMethod.GET)
    public String openDialogMessages(@RequestParam long dialogId,
                                     Model model, Principal principal, Locale locale) {
        try {
            if (principal == null) throw new Exception("Please, login first!");

            SvtDialog dialog = dao.find(SvtDialog.class, dialogId);
            model.addAttribute("dialog", dialog);
            model.addAttribute("senderId", getLoggedUser().getId());
            model.addAttribute("messagesData", getMessagesContent(messagesDao.getList(dialogId, 0, 10000, MessagesListElemData.class)));
        } catch (Exception ex) {
            model.addAttribute("errorMessage", ex.getMessage());
        }
        return "administrator/dialog/messagesForm";
    }

    @RequestMapping(value = "/get_dialog_messages", method = RequestMethod.GET)
    @ResponseBody
    public String getDialogMessagesList(
            @RequestParam("dialogId") long dialogId,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<Object> list = messagesDao.getList(dialogId, 0, 10000, MessagesListElemData.class);
            messagesDao.selectMessagesAsReaded(dialogId, true);

            result = getMessagesContent(list);
        } catch (Exception ex) {
            result = ex.getMessage();
        }
        //response.setCharacterEncoding("UTF-8");
        return ApplicationUtility.encodeStringForAjaxResponse(result);
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/changeDialogTitle", method = RequestMethod.POST)
    @ResponseBody
    public String changeDialogTitle(@RequestParam Long dialogId,
                                    @RequestParam String dialogTitle,
                                    Locale locale,
                                    Principal principal,
                                    RedirectAttributes redirectAttributes) {
        try {
            if (principal == null) throw new Exception("Please, login first!");

            SvtDialog dialog = dao.find(SvtDialog.class, dialogId);

            if (dialog == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            dialog.setTitle(dialogTitle);
            dao.create(dialog);

        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    @ResponseBody
    public String sendMessage(@RequestParam Long dialogId,
                              @RequestParam(value = "dialogTitle", required = false) String dialogTitle,
                              @RequestParam Long senderId,
                              @RequestParam Long receiverId,
                              @RequestParam String messageText,
                              @RequestParam(value = "file", required = false) MultipartFile file,
                              @RequestParam(value = "serviceDescription", required = false, defaultValue = "") String serviceDescription,
                              @RequestParam(value = "servicePrice", required = false, defaultValue = "0") Double servicePrice,
                              Locale locale,
                              Principal principal,
                              RedirectAttributes redirectAttributes) {
        try {
            if (principal == null) throw new Exception("Please, login first!");

            String fileName = "";
            String strMessage = "";

            /**
             * Upload image 1
             */
            if (file != null && !StringUtils.isBlank(file.getOriginalFilename())) {
                fileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(file.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(file.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                fileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        fileName);
            }

            /**
             * Send text message
             */
            if (!StringUtils.isBlank(messageText)) {
                SvtMessage message = new SvtMessage();
                message.setDialogId(dialogId);
                message.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
                message.setIsIsFromUser(false);
                message.setSenderId(senderId);
                message.setMessage(messageText);
                message.setType(Defines.MESSAGE_TYPE_TEXT);
                dao.create(message);
            }

            /**
             * Send link to the document or image
             */
            if (!StringUtils.isBlank(fileName)) {
                if (StringUtils.endsWithIgnoreCase(fileName, ".jpg")) {
                    strMessage = com.wds.utils.define.Defines.getHostImages() + fileName;
                } else if (StringUtils.endsWithIgnoreCase(fileName, ".pdf")) {
                    strMessage = com.wds.utils.define.Defines.getHostImages() + fileName;
                } else if (StringUtils.endsWithIgnoreCase(fileName, ".docx")) {
                    strMessage = com.wds.utils.define.Defines.getHostImages() + fileName;
                } else if (StringUtils.endsWithIgnoreCase(fileName, ".xlsx")) {
                    strMessage = com.wds.utils.define.Defines.getHostImages() + fileName;
                } else {
                    throw new Exception("Incorrect file format.");
                }

                SvtMessage message = new SvtMessage();
                message.setDialogId(dialogId);
                message.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
                message.setIsIsFromUser(false);
                message.setSenderId(senderId);
                message.setMessage(strMessage);
                message.setType(Defines.MESSAGE_TYPE_LINK);
                dao.create(message);
            }

            /**
             * Send button to pay an order
             */
            if (!StringUtils.isBlank(serviceDescription) && (servicePrice != null && servicePrice > 0)) {
                // Create an order
                SvtOrder order = new SvtOrder();
                order.setUid(receiverId);
                order.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
                order.setTitle(serviceDescription);
                order.setPrice(servicePrice);
                dao.create(order);

                SvtMessage message = new SvtMessage();
                message.setDialogId(dialogId);
                message.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
                message.setIsIsFromUser(false);
                message.setSenderId(senderId);
                message.setMessage(serviceDescription + " - $" + servicePrice);
                message.setServiceMessage(order.getId() + "|" + servicePrice);
                message.setType(Defines.MESSAGE_TYPE_BUTTON);
                dao.create(message);
            }

            // Send push notification (Apple)
            SvtUserTokenDao userTokenDao = new SvtUserTokenDao();
            SvtUserToken userToken = userTokenDao.findByUserId(receiverId);
            if (userToken != null) {
                List<String> tokenList = new ArrayList<>(1);
                tokenList.add(userToken.getToken());
                ApplePushNotificationNewMessageThread applePushNotificationNewMessageThread =
                        new ApplePushNotificationNewMessageThread(tokenList,
                                dialogId);
                applePushNotificationNewMessageThread.start();
            }
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return "";
    }

    private String getMessagesContent(List<Object> messagesList) {
        try {
            StringBuffer data = new StringBuffer("");
            for (Object message : messagesList) {
                MessagesListElemData row = (MessagesListElemData) message;

                String strMessage = row.getMessage();
                if (row.getMessageType() == Defines.MESSAGE_TYPE_LINK) {
                    strMessage = "<a target=\"_blank\" href=\"" + row.getMessage() + "\"><img src=\"" + row.getMessage() + "\" width=\"200\"/></a>";
                }

                if (row.isIsFromUser()) {
                    data.append("<div style=\"padding: 2px;\">" +
                            "            <img style=\"float: left;\" src=\"" + row.getSenderPhoto() + "\" width=\"64\" height=\"64\"/>" +
                            "            <div class=\"messageCell fromUser\" style=\"float: right;\">" +
                            "               <div class=\"messageCell_Header\">" +
                            ApplicationUtility.secondsToFormattedString(row.getDate(), ApplicationUtility.GMT_0, "dd.MM.yyyy HH:mm") +
                            " | " +
                            row.getSenderName() + "</div>" +
                            "               " + strMessage +
                            "            </div>" +
                            "        </div>");
                } else {
                    data.append("<div style=\"padding: 2px;\">" +
                            "            <img style=\"float: right;\" src=\"" + row.getSenderPhoto() + "\" width=\"64\" height=\"64\"/>" +
                            "            <div class=\"messageCell fromAdmin\" style=\"float: left;\">" +
                            "               <div class=\"messageCell_Header\">" +
                            ApplicationUtility.secondsToFormattedString(row.getDate(), ApplicationUtility.GMT_0, "dd.MM.yyyy HH:mm") +
                            " | " +
                            row.getSenderName() + "</div>" +
                            "               " + strMessage +
                            "            </div>" +
                            "        </div>");
                }
                data.append("<div style=\"clear: both\"></div>");
            }
            return data.toString();
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/closeDialog", method = RequestMethod.POST)
    @ResponseBody
    public String closeDialog(@RequestParam Long dialogId,
                              Locale locale,
                              Principal principal,
                              RedirectAttributes redirectAttributes) {
        try {
            if (principal == null) throw new Exception("Please, login first!");

            SvtDialog dialog = dao.find(SvtDialog.class, dialogId);

            if (dialog == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            dialog.setIsIsClosed(true);
            dao.save(dialog);

        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "";
    }
}
