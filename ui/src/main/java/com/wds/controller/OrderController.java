package com.wds.controller;

import com.wds.dao.*;
import com.wds.entity.*;
import com.wds.service.UserRoleConverter;
import com.wds.utils.ApplicationUtility;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/administrator/order")
public class OrderController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private Dao dao;
    @Autowired
    private SvtUserEventsDao userEventsDao;
    @Autowired
    private SvtOrdersDao ordersDao;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "order";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/get_orders_by_userId_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getEventsByUserId(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userId", required = false, defaultValue = "0") long userId,
            @RequestParam(value = "isPaid", required = false, defaultValue = "true") boolean isPaid,
            Locale locale,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<SvtOrder> list = ordersDao.getOrdersListByUserId(userId, isPaid, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", dao.getCount(SvtOrder.class));
            jsonObject.put("recordsFiltered", dao.getCount(SvtOrder.class));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (SvtOrder row : list) {

                jsonDataArray = new JSONArray();
                jsonDataArray.put(row.getId());
                jsonDataArray.put(ApplicationUtility.secondsToFormattedString(row.getDate(),
                        ApplicationUtility.GMT_0,
                        "dd.MM.yyyy HH:mm") + "&nbsp;<span style=\"color: #425070;\">МСК</span>");
                jsonDataArray.put(row.getTitle());
                jsonDataArray.put(row.getPrice());

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                jsonDataArray.put(sbActions.toString());

                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }
}
