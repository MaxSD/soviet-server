package com.wds.controller;

import com.wds.dao.Dao;
import com.wds.dao.SvtUserTokenDao;
import com.wds.dao.SvtUsersDao;
import com.wds.define.Defines;
import com.wds.entity.Role;
import com.wds.entity.SvtTag;
import com.wds.entity.SvtUser;
import com.wds.entity.SvtUserToken;
import com.wds.service.UserRoleConverter;
import com.wds.utils.SenderEmail;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.StringWriter;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/administrator/notifications")
public class NotificationsController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private Dao dao;
    @Autowired
    private SvtUserTokenDao svtUserTokenDao;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "notifications";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/send", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal) {
        if (principal == null) return "login";
        model.addAttribute("tagsList", dao.get(SvtTag.class));
        return "administrator/notifications/send";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/sendPush", method = RequestMethod.POST)
    public String sendPush(
            @RequestParam("strPushNotification") String strPushNotification,
            Model model,
            Principal principal,
            Locale locale) {

        try {
            if (principal == null) throw new Exception("Please, login first!");

            // send notifications for apple
            List<String> tokenList = svtUserTokenDao.getTokensListForApple();
            if (tokenList != null) {
                ApplePushNotificationThread applePushNotificationThread =
                        new ApplePushNotificationThread(tokenList, strPushNotification);
                applePushNotificationThread.start();
            }

            model.addAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Notification.Sent", null, locale));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "redirect:/administrator/notifications/send";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
    public String sendEmail(
            @RequestParam("emailContent") MultipartFile emailContent,
            @RequestParam("tags") List<Long> tags,
            Model model,
            Principal principal,
            Locale locale,
            RedirectAttributes redirectAttributes) {

        try {
            if (principal == null) throw new Exception("Please, login first!");

            //Get emails list
            SvtUsersDao usersDao = new SvtUsersDao();
            List<SvtUser> usersList = usersDao.getUsersByTagIds(tags);

            StringWriter writer = new StringWriter();
            IOUtils.copy(emailContent.getInputStream(), writer, "UTF8");
            String content = writer.toString();

            SenderEmail senderEmail = new SenderEmail();

            for (SvtUser user : usersList) {
                senderEmail.sendEmail(user.getEmail(), "Subject",
                        StringUtils.replace(content, "|FNAME|", user.getFirstName() + " " + user.getSurname()), true);
            }

            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Notification.Sent", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "redirect:/administrator/notifications/send";
    }
}
