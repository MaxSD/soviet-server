package com.wds.controller;

import com.wds.dao.*;
import com.wds.dataObjects.EventsOnUserFormData;
import com.wds.define.Defines;
import com.wds.entity.*;
import com.wds.service.UserRoleConverter;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/administrator/event")
public class EventController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private Dao dao;
    @Autowired
    private SvtEventTagsDao eventTagsDao;
    @Autowired
    private SvtUserEventsDao userEventsDao;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "event";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal) {
        if (principal == null) return "login";
        return "administrator/event/list";
    }

    @RequestMapping(value = "/get_all_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getAll(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            Locale locale,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<SvtEvent> list = dao.get(SvtEvent.class, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", dao.getCount(SvtEvent.class));
            jsonObject.put("recordsFiltered", dao.getCount(SvtEvent.class));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (SvtEvent row : list) {

                jsonDataArray = new JSONArray();
                jsonDataArray.put(row.getId());
                jsonDataArray.put(ApplicationUtility.secondsToFormattedString(row.getDate(),
                        ApplicationUtility.GMT_0,
                        "dd.MM.yyyy HH:mm") + "&nbsp;<span style=\"color: #425070;\">МСК</span>");
                jsonDataArray.put(row.getTitle());
                jsonDataArray.put(row.getDescription());

                if (!StringUtils.isBlank(row.getImage1()))
                    jsonDataArray.put("<img src=\"" + row.getImage1() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");

                if (!StringUtils.isBlank(row.getImage2()))
                    jsonDataArray.put("<img src=\"" + row.getImage2() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");

                if (!StringUtils.isBlank(row.getImage3()))
                    jsonDataArray.put("<img src=\"" + row.getImage3() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");

                jsonDataArray.put(row.getPrice());

                if (row.getStatus() == com.wds.database.define.Defines.EVENT_STATUS_NEW)
                    jsonDataArray.put("<span style='color: #8b0000; font-weight: bold;'>" + messageSource.getMessage("Event.Status.NEW", null, locale) + "</span>");
                else if (row.getStatus() == com.wds.database.define.Defines.EVENT_STATUS_ACCEPTED)
                    jsonDataArray.put("<span style='color: #006400; font-weight: bold;'>" + messageSource.getMessage("Event.Status.ACCEPTED", null, locale) + "</span>");

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                sbActions.append("<a class=\"btn btn-xs btn-default\" href=\"" + request.getContextPath() + "/administrator/event/" + row.getId() + "/view\">&nbsp;<i class=\"fa fa-info\"></i>&nbsp;</a>");
                sbActions.append("<a class=\"btn btn-xs btn-default\" href=\"" + request.getContextPath() + "/administrator/event/" + row.getId() + "/edit\">&nbsp;<i class=\"fa fa-edit\"></i>&nbsp;</a>");
                sbActions.append("<a href=\"#\" class=\"btn btn-xs btn-default\" onclick=\"deleteEvent('" + row.getId() + "', '" + row.getTitle() + "');\">&nbsp;<i class=\"fa fa-remove\"></i>&nbsp;</a>");

                jsonDataArray.put(sbActions.toString());
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/get_events_by_userId_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getEventsByUserId(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            @RequestParam(value = "userId", required = false, defaultValue = "0") long userId,
            @RequestParam(value = "isVisited", required = false, defaultValue = "true") boolean isVisited,
            Locale locale,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<SvtEvent> list = userEventsDao.getEventsListByUserId(userId, isVisited, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", userEventsDao.getCountByUserId(userId, isVisited));
            jsonObject.put("recordsFiltered", userEventsDao.getCountByUserId(userId, isVisited));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (SvtEvent row : list) {

                jsonDataArray = new JSONArray();
                jsonDataArray.put(row.getId());
                jsonDataArray.put(ApplicationUtility.secondsToFormattedString(row.getDate(),
                        ApplicationUtility.GMT_0,
                        "dd.MM.yyyy HH:mm") + "&nbsp;<span style=\"color: #425070;\">МСК</span>");
                jsonDataArray.put(row.getTitle());

                if (row.getCityId() == 1) {
                    jsonDataArray.put("London");
                } else if (row.getCityId() == 2) {
                    jsonDataArray.put("Moscow");
                } else {
                    jsonDataArray.put("");
                }

                /*
                if (!StringUtils.isBlank(row.getImage1()))
                    jsonDataArray.put("<img src=\"" + row.getImage1() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");
                    */

                jsonDataArray.put(row.getPrice());

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                jsonDataArray.put(sbActions.toString());

                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newObject(Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            SvtEvent createForm = new SvtEvent();
            model.addAttribute("createForm", createForm);
            model.addAttribute("tagsList", dao.get(SvtTag.class));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/event/new";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String newObject(@RequestParam String dateTime,
                            @RequestParam String title,
                            @RequestParam Long cityId,
                            @RequestParam String description,
                            @RequestParam String fullDescription,
                            @RequestParam String type,
                            @RequestParam String address,
                            @RequestParam("image1") MultipartFile image1,
                            @RequestParam("image2") MultipartFile image2,
                            @RequestParam("image3") MultipartFile image3,
                            @RequestParam Double price,
                            @RequestParam(value = "score", required = false, defaultValue = "0") int score,
                            @RequestParam("tags") List<Long> tags,
                            Locale locale,
                            Principal principal,
                            RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            String image1FileName = "";
            String image2FileName = "";
            String image3FileName = "";

            /**
             * Upload image 1
             */
            if (image1 != null && !image1.isEmpty()) {
                image1FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image1.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image1.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image1FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image1FileName);
            }

            /**
             * Upload image 2
             */
            if (image2 != null && !image2.isEmpty()) {
                image2FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image2.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image2.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image2FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image2FileName);
            }

            /**
             * Upload image 3
             */
            if (image3 != null && !image3.isEmpty()) {
                image3FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image3.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image3.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image3FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image3FileName);
            }

            long eventDate = ApplicationUtility.getDateInSeconds(
                    ApplicationUtility.formattedStringToDate(dateTime, ApplicationUtility.GMT_0, "dd.MM.yyyy HH:mm"));

            SvtEvent event = new SvtEvent();
            event.setDate(eventDate);

            event.setCurrMonthMs(ApplicationUtility.formattedStringToDate(
                    ApplicationUtility.millisecondsToFormattedString(eventDate * 1000, "MM.yyyy"),
                    ApplicationUtility.GMT_0,
                    "MM.yyyy").getTime());

            event.setCityId(cityId);
            event.setTitle(title);
            event.setDescription(description);
            event.setFullDescription(fullDescription);
            event.setType(type);
            event.setAddress(address);
            event.setImage1(com.wds.utils.define.Defines.getHostImages() + image1FileName);
            event.setImage2(com.wds.utils.define.Defines.getHostImages() + image2FileName);
            event.setImage3(com.wds.utils.define.Defines.getHostImages() + image3FileName);
            event.setPrice(price);
            event.setScore(score);
            event.setStatus(com.wds.database.define.Defines.EVENT_STATUS_ACCEPTED);

            SvtEventsDao eventsDao = new SvtEventsDao();
            eventsDao.create(event, tags);

            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Event.CreatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "administrator/event/new";
        }
        return "redirect:/administrator/event/list";
    }

    @RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
    public String editObject(@PathVariable long id,
                             Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            SvtEvent event = dao.find(SvtEvent.class, id);
            model.addAttribute("editForm", event);
            model.addAttribute("tagsList", dao.get(SvtTag.class));
            model.addAttribute("eventTagsIds", eventTagsDao.getTagIdsByEventId(id));
            model.addAttribute("dateTime", ApplicationUtility.secondsToFormattedString(event.getDate(),
                    ApplicationUtility.GMT_0,
                    "dd.MM.yyyy HH:mm"));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/event/edit";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{id}/edit", method = RequestMethod.POST)
    public String editObject(@PathVariable long id,
                             @RequestParam String dateTime,
                             @RequestParam Long cityId,
                             @RequestParam String title,
                             @RequestParam String description,
                             @RequestParam String fullDescription,
                             @RequestParam String type,
                             @RequestParam String address,
                             @RequestParam("image1") MultipartFile image1,
                             @RequestParam("image2") MultipartFile image2,
                             @RequestParam("image3") MultipartFile image3,
                             @RequestParam Double price,
                             @RequestParam(value = "score", required = false, defaultValue = "0") int score,
                             @RequestParam int status,
                             @RequestParam("tags") List<Long> tags,
                             Locale locale,
                             Principal principal,
                             RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtEvent event = dao.find(SvtEvent.class, id);
            if (event == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            event.setId(id);

            String image1FileName = "";
            String image2FileName = "";
            String image3FileName = "";
            /**
             * Upload image 1
             */
            if (image1 != null && !StringUtils.isBlank(image1.getOriginalFilename())) {
                image1FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image1.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image1.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image1FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image1FileName);

                event.setImage1(com.wds.utils.define.Defines.getHostImages() + image1FileName);
            }

            /**
             * Upload image 2
             */
            if (image2 != null && !StringUtils.isBlank(image2.getOriginalFilename())) {
                image2FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image2.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image2.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image2FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image2FileName);

                event.setImage2(com.wds.utils.define.Defines.getHostImages() + image2FileName);
            }

            /**
             * Upload image 3
             */
            if (image3 != null && !StringUtils.isBlank(image3.getOriginalFilename())) {
                image3FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image3.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image3.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image3FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image3FileName);

                event.setImage3(com.wds.utils.define.Defines.getHostImages() + image3FileName);
            }

            long eventDate = ApplicationUtility.getDateInSeconds(
                    ApplicationUtility.formattedStringToDate(dateTime, ApplicationUtility.GMT_0, "dd.MM.yyyy HH:mm"));

            event.setDate(eventDate);
            event.setCurrMonthMs(ApplicationUtility.formattedStringToDate(
                    ApplicationUtility.millisecondsToFormattedString(eventDate * 1000, "MM.yyyy"),
                    ApplicationUtility.GMT_0,
                    "MM.yyyy").getTime());

            event.setCityId(cityId);
            event.setTitle(title);
            event.setDescription(description);
            event.setFullDescription(fullDescription);
            event.setType(type);
            event.setAddress(address);
            event.setPrice(price);
            event.setScore(score);
            event.setStatus(status);

            SvtEventsDao eventsDao = new SvtEventsDao();
            eventsDao.update(event, tags);

            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Event.UpdatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/event/" + id + "/edit";
        }
        return "redirect:/administrator/event/list";
    }

    @RequestMapping(value = "{id}/view", method = RequestMethod.GET)
    public String viewObject(@PathVariable long id,
                             Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            SvtEvent event = dao.find(SvtEvent.class, id);
            model.addAttribute("viewForm", event);
            model.addAttribute("dateTime", ApplicationUtility.secondsToFormattedString(event.getDate(),
                    ApplicationUtility.GMT_3,
                    "dd.MM.yyyy HH:mm"));
            model.addAttribute("roleId", com.wds.database.define.Defines.USER_TYPE_USER);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/event/view";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/visit_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String visitEventAjax(@RequestParam long userEventId,
                                 Model model,
                                 Principal principal,
                                 Locale locale,
                                 RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtUserEvents userEvents = dao.find(SvtUserEvents.class, userEventId);

            if (userEvents != null) {
                userEvents.setIsIsVisited(true);
                dao.save(userEvents);

                SvtUser user = dao.find(SvtUser.class, userEvents.getUserId());
                if (user != null) {
                    SvtEvent event = dao.find(SvtEvent.class, userEvents.getEventId());
                    if (event != null) {
                        int currUsersScore = user.getEventsScore() + user.getOrdersScore() + user.getPartnersScore();
                        int newUsersScore = user.getEventsScore() + user.getOrdersScore() + user.getPartnersScore() + event.getScore();
                        user.setEventsScore(user.getEventsScore() + event.getScore());
                        dao.save(user);

                        if (currUsersScore < 1000 && newUsersScore >= 1000) {
                            // Send push notification if user earned 1000 points (Apple)
                            SvtUserTokenDao userTokenDao = new SvtUserTokenDao();
                            SvtUserToken userToken = userTokenDao.findByUserId(user.getId());
                            if (userToken != null) {
                                List<String> tokenList = new ArrayList<>(1);
                                ApplePushNotificationThread applePushNotificationThread =
                                        new ApplePushNotificationThread(tokenList,
                                                "CONGRATULATIONS you earned 1000 points!");
                                applePushNotificationThread.start();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{id}/delete_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String deleteObjectAjax(@PathVariable long id,
                                   Model model,
                                   Principal principal,
                                   Locale locale,
                                   RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtEvent event = dao.find(SvtEvent.class, id);
            if (event == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            dao.delete(event);
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.Event.DeletedSuccess", null, locale)) + "</p>";
    }
}
