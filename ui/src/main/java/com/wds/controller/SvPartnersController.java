package com.wds.controller;

import com.wds.dao.*;
import com.wds.define.Defines;
import com.wds.entity.*;
import com.wds.service.UserRoleConverter;
import com.wds.utils.ApplicationUtility;
import com.wds.utils.push.apple.ApplePushNotificationNewPartnerThread;
import com.wds.utils.push.apple.ApplePushNotificationThread;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "/administrator/partners")
public class SvPartnersController {

    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserRoleConverter userRoleConverter;
    @Autowired
    private Dao dao;
    @Autowired
    private RolesDao rolesDao;
    @Autowired
    private SvtUsersDao svtUsersDao;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, this.userRoleConverter);
    }

    @ModelAttribute("page")
    String getPage() {
        return "partners";
    }

    @ModelAttribute("currentUser")
    public SvtUser getLoggedUser() {
        try {
            final org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return svtUsersDao.findByEmail(principal.getUsername());
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Principal principal) {
        if (principal == null) return "login";
        return "redirect: /";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listAdmins(Model model, Principal principal) {
        if (principal == null) return "login";
        return "administrator/partners/list";
    }

    @RequestMapping(value = "/get_all_JSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String getUsers(
            @RequestParam("draw") int draw,
            @RequestParam("start") int start,
            @RequestParam("length") int length,
            Principal principal,
            HttpServletRequest request,
            HttpServletResponse response) {

        String result;

        try {
            if (principal == null) throw new Exception("Please, login first!");

            List<SvtPartner> list = dao.get(SvtPartner.class, start, length);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("draw", draw);
            jsonObject.put("recordsTotal", dao.getCount(SvtPartner.class));
            jsonObject.put("recordsFiltered", dao.getCount(SvtPartner.class));

            JSONArray jsonArray = new JSONArray();
            JSONArray jsonDataArray;
            for (SvtPartner row : list) {

                jsonDataArray = new JSONArray();

                jsonDataArray.put(row.getName());
                jsonDataArray.put(row.getDescription());
                jsonDataArray.put(row.getContacts());

                if (!StringUtils.isBlank(row.getImage1()))
                    jsonDataArray.put("<img src=\"" + row.getImage1() + "\" width=\"64\" height=\"64\"/>");
                else
                    jsonDataArray.put("");

                jsonDataArray.put(row.getComments());
                jsonDataArray.put(row.getDiscount());

                /**
                 * Actions
                 */
                StringBuilder sbActions = new StringBuilder("");
                sbActions.append("<a class=\"btn btn-xs btn-default\" href=\"" + request.getContextPath() + "/administrator/partners/" + row.getId() + "/view\">&nbsp;<i class=\"fa fa-info\"></i>&nbsp;</a>");
                sbActions.append("<a class=\"btn btn-xs btn-default\" href=\"" + request.getContextPath() + "/administrator/partners/" + row.getId() + "/edit\">&nbsp;<i class=\"fa fa-edit\"></i>&nbsp;</a>");
                sbActions.append("<a href=\"#\" class=\"btn btn-xs btn-default\" onclick=\"deletePriority('" + row.getId() + "', '" + row.getName() + "');\">&nbsp;<i class=\"fa fa-remove\"></i>&nbsp;</a>");

                jsonDataArray.put(sbActions.toString());
                jsonArray.put(jsonDataArray);
            }

            jsonObject.put("data", jsonArray);

            result = ApplicationUtility.encodeStringForAjaxResponse(jsonObject.toString());
        } catch (Exception ex) {
            response.setStatus(1001);
            return ex.getMessage();
        }

        return result;
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newObject(Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            SvtPartner createForm = new SvtPartner();
            model.addAttribute("createForm", createForm);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/partners/new";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String newObject(@RequestParam String name,
                            @RequestParam String description,
                            @RequestParam String fullDescription,
                            @RequestParam String contacts,
                            @RequestParam("image1") MultipartFile image1,
                            @RequestParam("image2") MultipartFile image2,
                            @RequestParam("image3") MultipartFile image3,
                            @RequestParam String comments,
                            @RequestParam Double discount,
                            Model model,
                            Locale locale,
                            Principal principal,
                            RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtPartner partner = new SvtPartner();
            String image1FileName = "";
            String image2FileName = "";
            String image3FileName = "";

            /**
             * Upload photo 1
             */
            if (image1 != null && !StringUtils.isBlank(image1.getOriginalFilename())) {
                if (image1.getSize() > 2000000)
                    throw new Exception("The image 1 is too large!");

                image1FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image1.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image1.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image1FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image1FileName);

                partner.setImage1(com.wds.utils.define.Defines.getHostImages() + image1FileName);
            } else {
                partner.setImage1("");
            }

            /**
             * Upload photo 2
             */
            if (image2 != null && !StringUtils.isBlank(image2.getOriginalFilename())) {
                if (image2.getSize() > 2000000)
                    throw new Exception("The image 2 is too large!");

                image2FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image2.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image2.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image2FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image2FileName);

                partner.setImage2(com.wds.utils.define.Defines.getHostImages() + image2FileName);
            } else {
                partner.setImage2("");
            }

            /**
             * Upload photo 3
             */
            if (image3 != null && !StringUtils.isBlank(image3.getOriginalFilename())) {
                if (image3.getSize() > 2000000)
                    throw new Exception("The image 3 is too large!");

                image3FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image3.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image3.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image3FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image3FileName);

                partner.setImage3(com.wds.utils.define.Defines.getHostImages() + image3FileName);
            } else {
                partner.setImage3("");
            }

            partner.setName(name);
            partner.setDescription(description);
            partner.setFulDescription(fullDescription);
            partner.setContacts(contacts);
            partner.setComments(comments);
            partner.setDiscount(discount);

            dao.create(partner);

            // Send push notification for all users (Apple)
            SvtUserTokenDao userTokenDao = new SvtUserTokenDao();
            List<String> tokenList = userTokenDao.getTokensListForApple();
            ApplePushNotificationNewPartnerThread applePushNotificationNewPartnerThread =
                    new ApplePushNotificationNewPartnerThread(tokenList,
                            partner.getId());
            applePushNotificationNewPartnerThread.start();

            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Partner.CreatedSuccess", null, locale));
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "administrator/partners/new";
        }
        return "redirect:/administrator/partners/list";
    }

    @RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
    public String editObject(@PathVariable long id,
                             Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            SvtPartner partner = dao.find(SvtPartner.class, id);

            model.addAttribute("editForm", partner);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/partners/edit";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{id}/edit", method = RequestMethod.POST)
    public String editObject(@PathVariable long id,
                             @RequestParam String name,
                             @RequestParam String description,
                             @RequestParam String fullDescription,
                             @RequestParam String contacts,
                             @RequestParam("image1") MultipartFile image1,
                             @RequestParam("image2") MultipartFile image2,
                             @RequestParam("image3") MultipartFile image3,
                             @RequestParam String comments,
                             @RequestParam Double discount,
                             Model model,
                             Locale locale,
                             Principal principal,
                             RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtPartner partner = dao.find(SvtPartner.class, id);
            if (partner == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            partner.setId(id);

            String image1FileName = "";
            String image2FileName = "";
            String image3FileName = "";

            /**
             * Upload photo 1
             */
            if (image1 != null && !StringUtils.isBlank(image1.getOriginalFilename())) {
                image1FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image1.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image1.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image1FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image1FileName);

                partner.setImage1(com.wds.utils.define.Defines.getHostImages() + image1FileName);
            }

            /**
             * Upload photo 2
             */
            if (image2 != null && !StringUtils.isBlank(image2.getOriginalFilename())) {
                image2FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image2.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image2.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image2FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image2FileName);

                partner.setImage2(com.wds.utils.define.Defines.getHostImages() + image2FileName);
            }

            /**
             * Upload photo 3
             */
            if (image3 != null && !StringUtils.isBlank(image3.getOriginalFilename())) {
                image3FileName = ApplicationUtility.getCurrentTimeStamp() +
                        ApplicationUtility.generateRandomString(6) + "." + StringUtils.substringAfterLast(image3.getOriginalFilename(), ".");

                ApplicationUtility.writeToFile(image3.getInputStream(),
                        com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                                image3FileName);

                ApplicationUtility.resizeImage(com.wds.utils.define.Defines.getImagesPathStorage() + File.separator +
                        image3FileName);

                partner.setImage3(com.wds.utils.define.Defines.getHostImages() + image3FileName);
            }

            partner.setName(name);
            partner.setDescription(description);
            partner.setFulDescription(fullDescription);
            partner.setContacts(contacts);
            partner.setComments(comments);
            partner.setDiscount(discount);

            dao.save(partner);
            redirectAttributes.addFlashAttribute(Defines.SUCCESS_MESSAGE, messageSource.getMessage("UI.Messages.Partner.UpdatedSuccess", null, locale));
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
            return "redirect:/administrator/partners/" + id + "/edit";
        }
        return "redirect:/administrator/partners/list";
    }

    @RequestMapping(value = "{id}/view", method = RequestMethod.GET)
    public String viewObject(@PathVariable long id,
                             Model model, Principal principal, Locale locale) {
        if (principal == null) return "login";
        try {
            SvtPartner partner = dao.find(SvtPartner.class, id);
            model.addAttribute("viewForm", partner);
        } catch (Exception ex) {
            model.addAttribute(Defines.ERROR_MESSAGE, ex.getMessage());
        }
        return "administrator/partners/view";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "/visit_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String visitEventAjax(@RequestParam(value = "firstName", defaultValue = "", required = false) String firstName,
                                 @RequestParam(value = "surname", defaultValue = "", required = false) String surname,
                                 @RequestParam(value = "cardNumber", defaultValue = "", required = false) String cardNumber,
                                 @RequestParam(value = "score", defaultValue = "0") Integer score,
                                 @RequestParam(value = "partnerId") Long partnerId,
                                 Model model,
                                 Principal principal,
                                 Locale locale,
                                 RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtUser user = null;
            if (!StringUtils.isBlank(cardNumber))
                user = svtUsersDao.findByCardNumber(cardNumber);

            if (!StringUtils.isBlank(firstName) && !StringUtils.isBlank(surname))
                user = svtUsersDao.findByFirstNameAndSurname(firstName, surname);

            if (user == null)
                throw new Exception("User not found.");

            int currUsersScore = user.getEventsScore() + user.getOrdersScore() + user.getPartnersScore();
            int newUsersScore = user.getEventsScore() + user.getOrdersScore() + user.getPartnersScore() + score;
            user.setPartnersScore(user.getPartnersScore() + score);
            dao.save(user);

            if (currUsersScore < 1000 && newUsersScore >= 1000) {
                // Send push notification if user earned 1000 points (Apple)
                SvtUserTokenDao userTokenDao = new SvtUserTokenDao();
                SvtUserToken userToken = userTokenDao.findByUserId(user.getId());
                if (userToken != null) {
                    List<String> tokenList = new ArrayList<>(1);
                    ApplePushNotificationThread applePushNotificationThread =
                            new ApplePushNotificationThread(tokenList,
                                    "CONGRATULATIONS you earned 1000 points!");
                    applePushNotificationThread.start();
                }
            }

            SvtUserPartners userPartners = new SvtUserPartners();
            userPartners.setUserId(user.getId());
            userPartners.setPartnerId(partnerId);
            userPartners.setDate(ApplicationUtility.getCurrentTimeStampGMT_0());
            dao.create(userPartners);
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "";
    }

    @SuppressWarnings("UnusedParameters")
    @RequestMapping(value = "{id}/delete_ajax", method = RequestMethod.POST)
    @ResponseBody
    public String deleteObjectAjax(@PathVariable long id,
                                   Model model,
                                   Principal principal,
                                   Locale locale,
                                   RedirectAttributes redirectAttributes) {
        if (principal == null) return "login";
        try {
            SvtPartner partner = dao.find(SvtPartner.class, id);
            if (partner == null)
                throw new Exception(messageSource.getMessage("UI.Error.DataNotFound", null, locale));

            dao.delete(partner);
        } catch (Exception ex) {
            return "<p class=\"alert alert-danger alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(ex.getMessage()) + "</p>";
        }
        return "<p class=\"alert alert-success alert-dismissable\">" + ApplicationUtility.encodeStringForAjaxResponse(messageSource.getMessage("UI.Messages.Partner.DeletedSuccess", null, locale)) + "</p>";
    }
}
