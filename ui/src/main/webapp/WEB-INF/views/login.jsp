<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<spring:message code="Login.Msg.Enter" var="msgEnterLogin"/>
<spring:message code="Password.Msg.Enter" var="msgEnterPassword"/>

<t:sign_in_wrapper>
    <jsp:attribute name="js">
        <script src="<c:url value="/resources/js/sign_in_validation.js"/>"></script>
        <script>
            $(document).ready(function () {
                pageSetUp();
                signInValidate('#sign-in-form', '${msgEnterLogin}', '${msgEnterPassword}');
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div class="well no-padding">
            <form action="<c:url value='/j_spring_security_check'/>" id="sign-in-form"
                  method='POST'
                  class="smart-form client-form">
                <header>
                    <spring:message code="SignIn.Title"/>
                </header>
                <fieldset>
                    <section>
                        <label class="label">
                            <spring:message code="Lbl.Email"/>
                        </label>
                        <label class="input"> <i class="icon-append fa fa-user"></i>
                            <input type="text" id="login" name="login">
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-user txt-color-teal"></i>
                                <spring:message code="Login.ToolTip"/>
                            </b>
                        </label>
                    </section>
                    <section>
                        <label class="label">
                            <spring:message code="Password.Lbl"/>
                        </label>
                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                            <input type="password" id="pass" name="pass">
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-lock txt-color-teal"></i>
                                <spring:message code="Password.ToolTip"/>
                            </b>
                        </label>
                        <div class="note">
                            <a href="<c:url value="/passwordRecovery/email_request"/>">
                                <spring:message code="ForgotPassword.Title"/>?
                            </a>
                        </div>
                    </section>
                </fieldset>
                <footer>
                    <button type="submit" class="btn btn-primary">
                        <spring:message code="SignIn.Submit"/>
                    </button>
                </footer>
            </form>
        </div>
    </jsp:body>
</t:sign_in_wrapper>