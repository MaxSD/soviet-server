﻿<%--suppress ELValidationInJSP --%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            /* chart colors default */
            var $chrt_border_color = "#efefef";
            var $chrt_grid_color = "#DDD"
            var $chrt_main = "#E24913";
            /* red       */
            var $chrt_second = "#6595b4";
            /* blue      */
            var $chrt_third = "#FF9F01";
            /* orange    */
            var $chrt_fourth = "#7e9d3a";
            /* green     */
            var $chrt_fifth = "#BD362F";
            /* dark red  */
            var $chrt_mono = "#000";

            $(document).ready(function () {
                /* bar-chart-h */
                if ($("#bar-chart-h-users").length) {
                    //Display horizontal graph
                    var d1_h = [];
                    d1_h.push([parseInt(${acceptedUsersCount}), 1]);

                    var d2_h = [];
                    d2_h.push([parseInt(${activeUsersCount}), 1]);

                    var ds_h = [];
                    ds_h.push({
                        data: d1_h,
                        bars: {
                            horizontal: true,
                            show: true,
                            barWidth: 0.5,
                            order: 2
                        }
                    });
                    ds_h.push({
                        data: d2_h,
                        bars: {
                            horizontal: true,
                            show: true,
                            barWidth: 0.5,
                            order: 1
                        }
                    });

                    // display graph
                    $.plot($("#bar-chart-h-users"), ds_h, {
                        colors: [$chrt_second, $chrt_fourth, "#666", "#BBB"],
                        grid: {
                            show: true,
                            hoverable: true,
                            clickable: true,
                            tickColor: $chrt_border_color,
                            borderWidth: 0,
                            borderColor: $chrt_border_color
                        },
                        legend: true,
                        tooltip: true,
                        tooltipOpts: {
                            content: "<b>%x</b>",
                            defaultTheme: false
                        }
                    });
                }

                /* dialogs chart */
                if ($("#bar-chart-dialogs").length) {

                    var dialogs_data1 = [];
                    dialogs_data1.push([1, parseInt(${dialogsCount})]);

                    var dialogs_data2 = [];
                    dialogs_data2.push([1, parseInt(${activeDialogsCount})]);

                    var dialogs_data3 = [];
                    dialogs_data3.push([1, parseInt(${closedDialogsCount})]);

                    var ds_dialogs = [];

                    ds_dialogs.push({
                        data: dialogs_data1,
                        bars: {
                            show: true,
                            barWidth: 0.2,
                            order: 1
                        }
                    });

                    ds_dialogs.push({
                        data: dialogs_data2,
                        bars: {
                            show: true,
                            barWidth: 0.2,
                            order: 2
                        }
                    });

                    ds_dialogs.push({
                        data: dialogs_data3,
                        bars: {
                            show: true,
                            barWidth: 0.2,
                            order: 3
                        }
                    });

                    //Display graph
                    $.plot($("#bar-chart-dialogs"), ds_dialogs, {
                        colors: [$chrt_second, $chrt_fourth, "#666", "#BBB"],
                        grid: {
                            show: true,
                            hoverable: true,
                            clickable: true,
                            tickColor: $chrt_border_color,
                            borderWidth: 0,
                            borderColor: $chrt_border_color,
                        },
                        legend: true,
                        tooltip: true,
                        tooltipOpts: {
                            content: "<b>%y</b>",
                            defaultTheme: false
                        }

                    });

                }
                /* end dialogs chart */

                /* orders chart */
                if ($("#bar-chart-orders").length) {

                    var data1 = [];
                    data1.push([1, parseInt(${ordersCount})]);

                    var data2 = [];
                    data2.push([1, parseInt(${paidOrdersAmountSum})]);

                    var ds = [];

                    ds.push({
                        data: data1,
                        bars: {
                            show: true,
                            barWidth: 0.2,
                            order: 1
                        }
                    });

                    ds.push({
                        data: data2,
                        bars: {
                            show: true,
                            barWidth: 0.2,
                            order: 2
                        }
                    });

                    //Display graph
                    $.plot($("#bar-chart-orders"), ds, {
                        colors: [$chrt_second, $chrt_fourth, "#666", "#BBB"],
                        grid: {
                            show: true,
                            hoverable: true,
                            clickable: true,
                            tickColor: $chrt_border_color,
                            borderWidth: 0,
                            borderColor: $chrt_border_color,
                        },
                        legend: true,
                        tooltip: true,
                        tooltipOpts: {
                            content: "<b>%y</b>",
                            defaultTheme: false
                        }
                    });
                }
                /* end orders chart */

                /* events count chart */
                if ($("#events").length) {
                    var d = [];

                    <c:if test="${not empty events}">
                    <c:forEach items="${events}" var="item">
                    d.push([${item.curr_month_ms}, ${item.count}]);
                    </c:forEach>
                    </c:if>

                    for (var i = 0; i < d.length; ++i)
                        d[i][0] += 60 * 60 * 1000;

                    var options = {
                        xaxis: {
                            mode: "time",
                            tickLength: 5
                        },
                        series: {
                            lines: {
                                show: true,
                                lineWidth: 1,
                                fill: true,
                                fillColor: {
                                    colors: [{
                                        opacity: 0.1
                                    }, {
                                        opacity: 0.15
                                    }]
                                }
                            },
                            //points: { show: true },
                            shadowSize: 0
                        },
                        selection: {
                            mode: "x"
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: $chrt_border_color,
                            borderWidth: 0,
                            borderColor: $chrt_border_color
                        },
                        tooltip: true,
                        tooltipOpts: {
                            content: "<b>%x</b> - <span>$%y</span>",
                            dateFormat: "%y-%0m",
                            defaultTheme: false
                        },
                        colors: [$chrt_second]
                    };

                    var plot = $.plot($("#events"), [d], options);
                }
                /* end events count chart */
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div style="margin: 0 5px;">
            <!-- widget grid -->
            <section id="widget-grid" class="">

                <!-- row -->
                <div style="margin: 0 5px;">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                                <h2>Accepted users / Active users</h2>
                            </header>
                            <!-- widget div-->
                            <div>
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div id="bar-chart-h-users" class="chart"></div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </article>
                </div>

                <!-- row -->
                <div style="margin: 0 5px;">
                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                                <h2>Orders (Count / Closed / Active)</h2>
                            </header>
                            <!-- widget div-->
                            <div>
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div id="bar-chart-dialogs" class="chart"></div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </article>

                    <article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                                <h2>Orders (Total amount / Count)</h2>
                            </header>
                            <!-- widget div-->
                            <div>
                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div id="bar-chart-orders" class="chart"></div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </article>
                </div>

                <!-- row -->
                <div style="margin: 0 5px;">
                    <!-- NEW WIDGET START -->
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                        <!-- Widget ID (each widget will need unique ID)-->
                        <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false">
                            <header>
                                <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>

                                <h2>Events count</h2>
                            </header>
                            <!-- widget div-->
                            <div>
                                <!-- widget edit box -->
                                <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->
                                </div>
                                <!-- end widget edit box -->

                                <!-- widget content -->
                                <div class="widget-body no-padding">
                                    <div id="events" class="chart"></div>
                                </div>
                                <!-- end widget content -->
                            </div>
                            <!-- end widget div -->
                        </div>
                        <!-- end widget -->
                    </article>
                    <!-- WIDGET END -->
                </div>
            </section>
        </div>
    </jsp:body>
</t:wrapper>