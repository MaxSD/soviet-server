<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body style="background-color: rgba(186, 204, 219, 0.99)">
<div class="container i-line">
    <div class="row">
        <div class="col-md-6 col-md-offset-3" style="margin-top: 100px">
            <%@include file="/WEB-INF/jspf/messages.jspf" %>
        </div>
        <div class="col-md-4 col-md-offset-4">
            <form name="login_form" action="<c:url value="/passwordRecovery/change_password"/>" method='POST' class="form-horizontal">
                <input type="hidden" id="sessionId" name="sessionId" value="${sessionId}"/>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><spring:message code="Lbl.PasswordRecoveryFormHeader"/></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="pass"><spring:message code="Lbl.NewPassword"/></label>
                            </div>
                            <div class="col-sm-12">
                                <input type="pass" class="form-control input-medium" style="width: 100%;"
                                       id="pass" name="pass" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="confirmPassword"><spring:message code="Lbl.ConfirmPassword"/></label>
                            </div>
                            <div class="col-sm-12">
                                <input type="pass" class="form-control input-medium" style="width: 100%;"
                                       id="confirmPassword" name="confirmPassword" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary pull-right" style="width: 100px;"><spring:message code="Btn.OK"/></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
<script>
    $(document).ready(function () {
        $("form input[name=login]:first").focus();
    });
</script>
</body>
</html>