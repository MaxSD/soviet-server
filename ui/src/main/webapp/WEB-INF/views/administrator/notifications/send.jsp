<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:body>
        <div class="col-md-8 col-md-offset-2">
            <form action="${pageContext.request.contextPath}/administrator/notifications/sendPush" method="post">
                <div>
                    <h4><spring:message code="Lbl.Notification.Push"/></h4>

                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td><spring:message code="Lbl.Message"/></td>
                            <td>
                                    <textarea id="strPushNotification" name="strPushNotification"
                                              class="form-control"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <button class="btn btn-success margin-top-10 pull-right" type="submit" name="submit">
                        <spring:message code="Btn.Send"/>
                    </button>
                </div>
            </form>

            <br/>
            <hr/>
            <br/>

            <form action="${pageContext.request.contextPath}/administrator/notifications/sendEmail"
                  method="post"
                  enctype="multipart/form-data" class="smart-form">
                <div>
                    <h4><spring:message code="Lbl.Notification.Email"/></h4>

                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td><spring:message code="Lbl.Message"/></td>
                            <td>
                                <div class="input input-file">
                                            <span class="button"><input id="emailContent" type="file" name="emailContent"
                                                                        onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input
                                        type="text" readonly="">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Tags"/></td>
                            <td style="text-align: center;">
                                <c:set var="collIndex" value="0"/>
                                <c:if test="${not empty tagsList}">
                                    <table border="0" style=" width: 100%; text-align: left;">
                                        <c:forEach items="${tagsList}" var="tag" varStatus="currElIndex">
                                            <c:if test="${collIndex == 4}">
                                                <c:set var="collIndex" value="0"/>
                                                </tr>
                                            </c:if>
                                            <c:if test="${collIndex == 0 || collIndex == 4}">
                                                <tr>
                                            </c:if>
                                            <td>
                                                <label>
                                                    <input name="tags" type="checkbox" value="${tag.id}"/>&nbsp;&nbsp;&nbsp;
                                                        ${tag.name}
                                                </label>
                                            </td>
                                            <c:if test="${currElIndex.index == (tagsList.size() - 1)}">
                                                </tr>
                                            </c:if>
                                            <c:set var="collIndex" value="${collIndex + 1}"/>
                                        </c:forEach>
                                    </table>
                                </c:if>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <button class="btn btn-success margin-top-10 pull-right" type="submit" name="submit">
                        <spring:message code="Btn.Send"/>
                    </button>
                </div>
            </form>
        </div>
    </jsp:body>
</t:wrapper>