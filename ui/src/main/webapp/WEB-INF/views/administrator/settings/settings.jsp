<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:body>
        <div class="col-md-8 col-md-offset-2">
            <form action="${pageContext.request.contextPath}/administrator/settings/save" method="post">
                <div>
                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td>Score message</td>
                            <td>
                                <input name="scoreText" value="${scoreText}" class="form-control"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <button class="btn btn-success margin-top-10 pull-right" type="submit" name="submit">
                        <spring:message code="Btn.Send"/>
                    </button>
                </div>
            </form>
        </div>
    </jsp:body>
</t:wrapper>