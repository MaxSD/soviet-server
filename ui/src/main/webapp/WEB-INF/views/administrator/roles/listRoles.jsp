<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
     <jsp:attribute name="js">
        <script>
            function deleteRole(_roleId, _roleName) {
                bootbox.confirm("<spring:message code="UI.Confirm.DeleteRole"/> (Role name -> " + _roleName + ")", function (result) {
                    if (result) {
                        $("#listRolesForm").attr("action",
                                "${pageContext.request.contextPath}/administrator/roles/" + _roleId + "/delete_role").submit();
                    }
                });
            }
        </script>
    </jsp:attribute>
    <jsp:body>
        <div class="jarviswidget jarviswidget-color-blueDark padding-10" id="wid-id-1"
             data-widget-editbutton="false">
            <form id="listRolesForm" name="listRolesForm" method="post">
                <table class="b-table b-table--search table table-striped">
                    <thead>
                    <tr>
                        <th data-hide="phone,tablet">&nbsp;</th>
                        <th data-hide="phone,tablet">&nbsp;</th>
                        <th data-hide="phone,tablet">&nbsp;</th>
                        <th data-hide="phone,tablet">&nbsp;</th>
                        <th data-hide="phone,tablet">
                            <i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:if test="${not empty rolesList}">
                        <c:forEach items="${rolesList}" var="roleData">
                            <tr>
                                <td>${roleData.get('id')}</td>
                                <td>${roleData.get('name')}</td>
                                <td>${roleData.get('text')}</td>
                                <td>${roleData.get('description')}</td>
                                <td class="pull-right">
                                    <sec:authorize access="hasAnyRole('PERMISSION_EDIT_ROLE')">
                                        <a href="<c:url value="/administrator/roles/${roleData.get('id')}/edit_role"/>"
                                           class="btn btn-xs btn-default">
                                            &nbsp;
                                            <i class="fa fa-edit"></i>
                                            &nbsp;
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('PERMISSION_EDIT_ROLE')">
                                        <a href="<c:url value="/administrator/roles/${roleData.get('id')}/view_role"/>"
                                           class="btn btn-xs btn-default">
                                            &nbsp;
                                            <i class="fa fa-folder-open"></i>
                                            &nbsp;
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('PERMISSION_DELETE_ROLE')">
                                        <c:if test="${user.id != 1}">
                                            <a href="#"
                                               onclick="deleteRole('${roleData.get('id')}', '${roleData.get('text')}');"
                                               class="btn btn-xs btn-default">
                                                &nbsp;
                                                <i class="fa fa-remove"></i>
                                                &nbsp;
                                            </a>
                                        </c:if>
                                    </sec:authorize>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </form>
        </div>
    </jsp:body>
</t:wrapper>