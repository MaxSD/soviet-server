﻿<%--suppress HtmlFormInputWithoutLabel --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<%@include file="/WEB-INF/jspf/checkBrowser.jspf" %>
<html>
<head>
    <%@include file="/WEB-INF/jspf/headSection.jspf" %>
</head>
<body>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container i-line">
    <div class="row">
        <%@include file="/WEB-INF/jspf/messages.jspf" %>

        <div class="col-md-8 col-md-offset-2">
            <form:form action="${pageContext.request.contextPath}/administrator/roles/new_role" method="post"
                       modelAttribute="roleCreateForm" autocomplete="true">

                <h4><spring:message code="Lbl.NewRole"/></h4>

                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Role.Name.RU"/></td>
                        <td>
                            <input type="text" id="textRU" name="textRU" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Role.Name.EN"/></td>
                        <td>
                            <input type="text" id="textEN" name="textEN" required="required" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Role.Description.RU"/></td>
                        <td>
                            <input type="text" id="descriptionRU" name="descriptionRU" required="required"
                                   class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Role.Description.EN"/></td>
                        <td>
                            <input type="text" id="descriptionEN" name="descriptionEN" required="required"
                                   class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Permissions"/></td>
                        <td>
                            <c:if test="${not empty permissionsList}">
                                <form:checkboxes path="permissionsList" items="${permissionsList}"
                                                 data-size="mini" delimiter="</br>"/>
                            </c:if>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <button class="btn btn-succes margin-top-10s pull-right" type="submit" name="submit">
                    <spring:message code="Btn.Save"/>
                </button>

                <a href="<c:url value="/administrator/roles/list_roles"/>">
                    <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                        <spring:message code="Btn.Cancel"/>
                    </button>
                </a>
            </form:form>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>
