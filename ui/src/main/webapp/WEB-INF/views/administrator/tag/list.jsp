<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            var usersTable;

            /**
             * Delete event
             * @param _id
             * @param _name
             */
            function deleteUser(_id, _name) {
                bootbox.confirm("<spring:message code="UI.Confirm.DeleteUser"/> (" + _name + ")", function (result) {
                    if (result) {
                        jQueryAjax.ajaxPOST_HTML("${pageContext.request.contextPath}/administrator/svUser/" + _id + "/delete_ajax", {}, deleteUserCallBack, true, "messages", null);
                    }
                });
            }
            function deleteUserCallBack() {
                usersTable.ajax.reload();
            }

            $(document).ready(function () {
                pageSetUp();
                var responsiveHelper_datatable = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };

                usersTable = $('#usersTable').DataTable({
                    iDisplayLength: 10,
                    "serverSide": true,
                    "bFilter": false,
                    "order": [],
                    "columnDefs": [
                        {"orderable": false, "targets": 0},
                        {"orderable": false, "targets": 1},
                        {"orderable": false, "targets": 2},
                        {"orderable": false, "targets": 3},
                        {"orderable": false, "targets": 4},
                        {"orderable": false, "targets": 5},
                        {"orderable": false, "targets": 6},
                        {"orderable": false, "targets": 7},
                        {"orderable": false, "targets": 8}
                    ],
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_datatable) {
                            responsiveHelper_datatable = new ResponsiveDatatablesHelper($('#usersTable'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_datatable.createExpandIcon(nRow);
                    },
                    "drawCallback": function () {
                        responsiveHelper_datatable.respond();
                    },
                    "ajax": {
                        "url": "${pageContext.request.contextPath}/administrator/svUser/get_all_JSON",
                        "type": "POST",
                        "data": function (d) {

                        }
                    },
                    "language": {
                        "url": "<c:url value="/resources/datatable/${pageContext.response.locale}.json"/>"
                    }
                });
                // Apply the filter
                $("#usersTable thead th input[type=text]").on('keyup change', function () {
                    dataTable
                            .column($(this).parent().index() + ':visible')
                            .search(this.value)
                            .draw();
                });
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div class="jarviswidget jarviswidget-color-blueDark padding-10" id="wid-id-1"
             data-widget-editbutton="false">
            <div>
                <div class="widget-body no-padding">
                    <table id="usersTable" class="table table-striped table-bordered" width="100%">
                        <thead>
                        <tr>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.ID"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.Photo"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.UserName"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.BirthDate"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.About"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.Profession"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.Email"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.Phone"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                <spring:message code="Lbl.CardNumber"/>
                            </th>
                            <th data-hide="phone,tablet">
                                <i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <sec:authorize access="hasRole('PERMISSION_EDIT_USERS')">
                <a href="<c:url value="/administrator/svUser/new"/>">
                    <button class="btn btn-success margin-top-10 pull-right" type="button" name="btnCreate">
                        <spring:message code="Btn.Create"/>
                    </button>
                </a>
            </sec:authorize>
        </div>
    </jsp:body>
</t:wrapper>