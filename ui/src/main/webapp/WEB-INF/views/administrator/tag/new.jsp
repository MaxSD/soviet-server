﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            $(document).ready(function () {
                $("#birthDate").mask("99.99.9999");
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div>
            <div class="col-md-8 col-md-offset-2">
                <form action="${pageContext.request.contextPath}/administrator/svUser/new" method="post"
                      enctype="multipart/form-data">

                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td><spring:message code="Lbl.FirstName"/></td>
                            <td>
                                <input type="text" id="firstName" name="firstName" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Surname"/></td>
                            <td>
                                <input value="" type="text" id="surname" name="surname" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Gender"/></td>
                            <td>
                                <select id="gender" name="gender" class="form-control">
                                    <option value="0">Ж</option>
                                    <option value="1">М</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Phone"/></td>
                            <td>
                                <input type="text" id="phone" name="phone" required="required" class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.BirthDate"/></td>
                            <td>
                                <input type="text" id="birthDate" name="birthDate" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Profession"/></td>
                            <td>
                                <input type="text" id="profession" name="profession" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.About"/></td>
                            <td>
                                <input type="text" id="about" name="about" class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Photo"/></td>
                            <td>
                                <input type="file" id="photo" name="photo"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.CardNumber"/></td>
                            <td>
                                <input type="text" id="cardNumber" name="cardNumber" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Email"/></td>
                            <td>
                                <input type="text" id="email" name="email" required="required" class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Password"/></td>
                            <td>
                                <input type="password" id="password" name="password" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Roles"/></td>
                            <td>
                                <c:if test="${not empty rolesList}">
                                    <select id="role" name="role" class="form-control">
                                        <c:forEach items="${rolesList}" var="role">
                                            <option value="${role.key}">${role.value}</option>
                                        </c:forEach>
                                    </select>
                                </c:if>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <sec:authorize access="hasRole('PERMISSION_CREATE_USERS')">
                        <button class="btn btn-success margin-top-10 pull-right" type="submit" name="submit">
                            <spring:message code="Btn.Save"/>
                        </button>
                    </sec:authorize>

                    <a href="<c:url value="/administrator/svUser/list"/>">
                        <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </form>
            </div>
        </div>
    </jsp:body>
</t:wrapper>