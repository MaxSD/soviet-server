﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            $(document).ready(function () {
                $("#birthDate").mask("99.99.9999");
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div>
            <div class="col-md-8 col-md-offset-2">
                <form action="${pageContext.request.contextPath}/administrator/partners/new" method="post"
                      enctype="multipart/form-data">

                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td><spring:message code="Lbl.Name"/></td>
                            <td>
                                <input type="text" id="name" name="name" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Description"/></td>
                            <td>
                                <input value="" type="text" id="description" name="description" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.FullDescription"/></td>
                            <td>
                                <textarea id="fullDescription" name="fullDescription" required="required"
                                          class="form-control"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Contacts"/></td>
                            <td>
                                <input type="text" id="contacts" name="contacts" required="required" class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Image"/>&nbsp;(max. size 2Mb)</td>
                            <td>
                                <input type="file" id="image1" name="image1"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <input type="file" id="image2" name="image2"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <input type="file" id="image3" name="image3"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Comments"/></td>
                            <td>
                                <input type="text" id="comments" name="comments" required="required"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Discount"/></td>
                            <td>
                                <input type="text" id="discount" name="discount" required="required"
                                       onkeypress="return fieldValidator.checkNumber(event);" class="form-control"
                                       style="width: 70px;"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <sec:authorize access="hasRole('PERMISSION_CREATE_USERS')">
                        <button class="btn btn-success margin-top-10 pull-right" type="submit" name="submit">
                            <spring:message code="Btn.Save"/>
                        </button>
                    </sec:authorize>

                    <a href="<c:url value="/administrator/partners/list"/>">
                        <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Return"/>
                        </button>
                    </a>
                </form>
            </div>
        </div>
    </jsp:body>
</t:wrapper>