﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            var usersTable;

            $(document).ready(function () {
                pageSetUp();

                var responsiveHelper_datatable = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };
                usersTable = $('#usersTable').DataTable({
                    iDisplayLength: 10,
                    "serverSide": true,
                    "bFilter": false,
                    "order": [],
                    "columnDefs": [
                        {"orderable": false, "targets": 0},
                        {"orderable": false, "targets": 1},
                        {"orderable": false, "targets": 2},
                        {"orderable": false, "targets": 3},
                        {"orderable": false, "targets": 4},
                        {"orderable": false, "targets": 5}
                    ],
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_datatable) {
                            responsiveHelper_datatable = new ResponsiveDatatablesHelper($('#usersTable'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_datatable.createExpandIcon(nRow);
                    },
                    "drawCallback": function () {
                        responsiveHelper_datatable.respond();
                    },
                    "ajax": {
                        "url": "${pageContext.request.contextPath}/administrator/svUser/get_users_visited_partner_form_JSON",
                        "type": "POST",
                        "data": function (d) {
                            d.partnerId = ${viewForm.id};
                        }
                    },
                    "language": {
                        "url": "<c:url value="/resources/datatable/${pageContext.response.locale}.json"/>"
                    }
                });
                // Apply the filter
                $("#usersTable thead th input[type=text]").on('keyup change', function () {
                    dataTable
                            .column($(this).parent().index() + ':visible')
                            .search(this.value)
                            .draw();
                });
            });

            function userHasVisitedPartner() {
                $("#visitPartnerAjaxMessage").html("");
                $("#successMessage").hide();
                $.ajax({
                    url: "<c:url value="/administrator/partners/visit_ajax"/>",
                    type: "POST",
                    data: {
                        firstName: $("#firstName").val(),
                        surname: $("#surname").val(),
                        cardNumber: $("#cardNumber").val(),
                        score: $("#score").val(),
                        partnerId: ${viewForm.id}
                    },
                    success: function (response) {
                        if (response.length)
                            $("#visitPartnerAjaxMessage").html(response);
                        else {
                            $("#firstName").val("");
                            $("#surname").val("");
                            $("#cardNumber").val("");
                            $("#score").val("0");
                            $("#successMessage").show();
                            usersTable.ajax.reload();
                        }
                    }
                });
            }
        </script>
    </jsp:attribute>
    <jsp:body>
        <div>
            <%@include file="/WEB-INF/jspf/messages.jspf" %>

            <div class="col-md-8 col-md-offset-2">
                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.Name"/></td>
                        <td>
                            <input type="text" id="name" name="name" required="required"
                                   class="form-control" value="${viewForm.name}" readonly="readonly"/>
                        </td>

                        <td><spring:message code="Lbl.Description"/></td>
                        <td>
                            <input type="text" id="description" name="description" required="required"
                                   class="form-control" value="${viewForm.description}" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Contacts"/></td>
                        <td>
                            <input type="text" id="contacts" name="contacts" required="required"
                                   class="form-control" value="${viewForm.contacts}" readonly="readonly"/>
                        </td>

                        <td><spring:message code="Lbl.Image"/></td>
                        <td>
                            <a target="_blank" href="${viewForm.image1}">
                                <img src="${viewForm.image1}" width="80"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Image"/></td>
                        <td>
                            <a target="_blank" href="${viewForm.image2}">
                                <img src="${viewForm.image2}" width="80"/>
                            </a>
                        </td>

                        <td><spring:message code="Lbl.Image"/></td>
                        <td>
                            <a target="_blank" href="${viewForm.image3}">
                                <img src="${viewForm.image3}" width="80"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Comments"/></td>
                        <td>
                            <input type="text" id="comments" name="comments" required="required"
                                   class="form-control" value="${viewForm.comments}" readonly="readonly"/>
                        </td>

                        <td><spring:message code="Lbl.Discount"/></td>
                        <td>
                                ${viewForm.discount}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <br/>

                <div style="text-align: center;"><h3><b>New visitor</b></h3></div>
                <div id="visitPartnerAjaxMessage" style="text-align: center;"></div>
                <div id="successMessage" style="text-align: center; display: none;">
                    <p class="alert alert-success alert-dismissable">The user been added.</p>
                </div>
                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.FirstName"/></td>
                        <td>
                            <input type="text" id="firstName" name="firstName"
                                   class="form-control" value=""/>
                        </td>

                        <td><spring:message code="Lbl.Surname"/></td>
                        <td>
                            <input type="text" id="surname" name="surname"
                                   class="form-control" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.CardNumber"/></td>
                        <td>
                            <input type="text" id="cardNumber" name="cardNumber"
                                   class="form-control" value=""/>
                        </td>

                        <td><spring:message code="Lbl.Score"/></td>
                        <td>
                            <input type="text" id="score" name="score" required="required"
                                   onkeypress="return fieldValidator.checkNumber(event);" class="form-control"
                                   value="0"
                                   style="width: 70px;"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <button class="btn btn-success margin-top-10 pull-right" type="button" name="btnVisitPartner"
                        onclick="userHasVisitedPartner();">
                    <spring:message code="Btn.Visited"/>
                </button>

                <br/>

                <div style="text-align: center;"><h3><b>Visitors</b></h3></div>
                <div>
                    <div class="widget-body no-padding">
                        <table id="usersTable" class="table table-striped table-bordered" width="100%">
                            <thead>
                            <tr>
                                <th data-hide="phone,tablet">
                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                    <spring:message code="Lbl.Photo"/>
                                </th>
                                <th data-hide="phone,tablet">
                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                    <spring:message code="Lbl.UserName"/>
                                </th>
                                <th data-hide="phone,tablet">
                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                    <spring:message code="Lbl.Email"/>
                                </th>
                                <th data-hide="phone,tablet">
                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                    <spring:message code="Lbl.Phone"/>
                                </th>
                                <th data-hide="phone,tablet">
                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                    <spring:message code="Lbl.CardNumber"/>
                                </th>
                                <th data-hide="phone,tablet">
                                    <i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i>
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <a href="<c:url value="/administrator/event/list"/>">
                    <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                        <spring:message code="Btn.Cancel"/>
                    </button>
                </a>
            </div>
        </div>
    </jsp:body>
</t:wrapper>