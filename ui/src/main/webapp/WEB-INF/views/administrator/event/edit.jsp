﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            $(document).ready(function () {
                $("#dateTime").mask("99.99.9999 99:99");
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div>
            <%@include file="/WEB-INF/jspf/messages.jspf" %>

            <div class="col-md-8 col-md-offset-2">
                <form action="${pageContext.request.contextPath}/administrator/event/${editForm.id}/edit"
                      method="post"
                      enctype="multipart/form-data">

                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td><spring:message code="Lbl.DateTime"/></td>
                            <td>
                                <input type="text" id="dateTime" name="dateTime" required="required"
                                       class="form-control" value="${dateTime}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.City"/></td>
                            <td>
                                <select id="cityId" name="cityId" required="required" class="form-control">
                                    <option value="1" <c:if test="${editForm.cityId == 1}"> selected="selected"</c:if>>London</option>
                                    <option value="2" <c:if test="${editForm.cityId == 2}"> selected="selected"</c:if>>Moscow</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Title"/></td>
                            <td>
                                <input type="text" id="title" name="title" required="required"
                                       class="form-control" value="${editForm.title}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Description"/></td>
                            <td>
                                <input type="text" id="description" name="description" required="required"
                                       class="form-control" value="${editForm.description}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.FullDescription"/></td>
                            <td>
                                <input type="text" id="fullDescription" name="fullDescription" required="required"
                                       class="form-control" value="${editForm.fullDescription}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Type"/></td>
                            <td>
                                <input type="text" id="type" name="type" required="required"
                                       class="form-control"  value="${editForm.type}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Address"/></td>
                            <td>
                                <input type="text" id="address" name="address" required="required"
                                       class="form-control" value="${editForm.address}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <input type="file" id="image1" name="image1"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <input type="file" id="image2" name="image2"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <input type="file" id="image3" name="image3"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Price"/></td>
                            <td>
                                <input type="text" id="price" name="price" required="required"
                                       class="form-control"
                                       style="width: 70px;" value="${editForm.price}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Score"/></td>
                            <td>
                                <input type="text" id="score" name="score" required="required"
                                       onkeypress="return fieldValidator.checkNumber(event);" class="form-control"
                                       style="width: 70px;" value="${editForm.score}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Status"/></td>
                            <td>
                                <select id="status" name="status" class="form-control">
                                    <option value="0" <c:if test="${editForm.status == 0}"> selected="selected"</c:if>><spring:message code="Event.Status.NEW"/></option>
                                    <option value="1" <c:if test="${editForm.status == 1}"> selected="selected"</c:if>><spring:message code="Event.Status.ACCEPTED"/></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Tags"/></td>
                            <td style="text-align: center;">
                                <c:set var="collIndex" value="0"/>
                                <c:if test="${not empty tagsList}">
                                    <table border="0" style=" width: 100%; text-align: left;">
                                        <c:forEach items="${tagsList}" var="tag" varStatus="currElIndex">
                                            <c:if test="${collIndex == 4}">
                                                <c:set var="collIndex" value="0"/>
                                                </tr>
                                            </c:if>
                                            <c:if test="${collIndex == 0 || collIndex == 4}">
                                                <tr>
                                            </c:if>
                                            <td>
                                                <label>
                                                    <input name="tags" type="checkbox" value="${tag.id}" <c:if test="${eventTagsIds.contains(tag.id)}"> checked="checked" </c:if> />&nbsp;&nbsp;&nbsp;
                                                        ${tag.name}
                                                </label>
                                            </td>
                                            <c:if test="${currElIndex.index == (tagsList.size() - 1)}">
                                                </tr>
                                            </c:if>
                                            <c:set var="collIndex" value="${collIndex + 1}"/>
                                        </c:forEach>
                                    </table>
                                </c:if>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <button class="btn btn-success margin-top-10 pull-right" type="submit" name="submit">
                        <spring:message code="Btn.Save"/>
                    </button>

                    <a href="<c:url value="/administrator/event/list"/>">
                        <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </form>
            </div>
        </div>
    </jsp:body>
</t:wrapper>