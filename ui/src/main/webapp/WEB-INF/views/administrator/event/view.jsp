﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            var usersTable;

            $(document).ready(function () {
                pageSetUp();

                var responsiveHelper_datatable = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };
                usersTable = $('#usersTable').DataTable({
                    iDisplayLength: 10,
                    "serverSide": true,
                    "bFilter": false,
                    "order": [],
                    "columnDefs": [
                        {"orderable": false, "targets": 0},
                        {"orderable": false, "targets": 1},
                        {"orderable": false, "targets": 2},
                        {"orderable": false, "targets": 3},
                        {"orderable": false, "targets": 4},
                        {"orderable": false, "targets": 5}
                    ],
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_datatable) {
                            responsiveHelper_datatable = new ResponsiveDatatablesHelper($('#usersTable'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_datatable.createExpandIcon(nRow);
                    },
                    "drawCallback": function () {
                        responsiveHelper_datatable.respond();
                    },
                    "ajax": {
                        "url": "${pageContext.request.contextPath}/administrator/svUser/get_users_on_event_form_JSON",
                        "type": "POST",
                        "data": function (d) {
                            d.roleId = ${roleId},
                                    d.eventId = ${viewForm.id};
                        }
                    },
                    "language": {
                        "url": "<c:url value="/resources/datatable/${pageContext.response.locale}.json"/>"
                    }
                });
                // Apply the filter
                $("#usersTable thead th input[type=text]").on('keyup change', function () {
                    dataTable
                            .column($(this).parent().index() + ':visible')
                            .search(this.value)
                            .draw();
                });
            });

            function userHasVisitedEvent(_userId) {
                $.ajax({
                    url: "<c:url value="/administrator/event/visit_ajax"/>",
                    type: "POST",
                    data: {
                        userId: _userId
                    },
                    success: function (response) {
                        if (response.length)
                            $("#ajaxMessage").html(response);
                        else
                            usersTable.ajax.reload();
                    }
                });
            }
        </script>
    </jsp:attribute>
    <jsp:body>
        <div>
            <%@include file="/WEB-INF/jspf/messages.jspf" %>

            <div class="col-md-8 col-md-offset-2">
                <form action="${pageContext.request.contextPath}/administrator/event/${viewForm.id}/edit"
                      method="post"
                      enctype="multipart/form-data">

                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td><spring:message code="Lbl.DateTime"/></td>
                            <td>
                                <input type="text" id="dateTime" name="dateTime" required="required"
                                       class="form-control" value="${dateTime}" readonly="readonly"/>
                            </td>

                            <td><spring:message code="Lbl.Address"/></td>
                            <td>
                                <input type="text" id="address" name="address" required="required"
                                       class="form-control" value="${viewForm.address}" readonly="readonly"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Title"/></td>
                            <td>
                                <input type="text" id="title" name="title" required="required"
                                       class="form-control" value="${viewForm.title}" readonly="readonly"/>
                            </td>

                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <a target="_blank" href="${viewForm.image1}">
                                    <img src="${viewForm.image1}" width="80"/>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Description"/></td>
                            <td>
                                <input type="text" id="description" name="description" required="required"
                                       class="form-control" value="${viewForm.description}" readonly="readonly"/>
                            </td>

                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <a target="_blank" href="${viewForm.image2}">
                                    <img src="${viewForm.image2}" width="80"/>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.FullDescription"/></td>
                            <td>
                                <input type="text" id="fullDescription" name="fullDescription" required="required"
                                       class="form-control" value="${viewForm.fullDescription}" readonly="readonly"/>
                            </td>

                            <td><spring:message code="Lbl.Image"/></td>
                            <td>
                                <a target="_blank" href="${viewForm.image3}">
                                    <img src="${viewForm.image3}" width="80"/>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Type"/></td>
                            <td>
                                <input type="text" id="type" name="type" required="required"
                                       class="form-control" value="${viewForm.type}" readonly="readonly"/>
                            </td>

                            <td><spring:message code="Lbl.Price"/></td>
                            <td>
                                <input type="text" id="price" name="price" required="required"
                                       class="form-control" value="${viewForm.price}" readonly="readonly"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.City"/></td>
                            <td>
                                <c:if test="${viewForm.cityId == 1}">London</c:if>
                                <c:if test="${viewForm.cityId == 2}">Moscow</c:if>
                            </td>

                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>

                    <div>
                        <div class="widget-body no-padding">
                            <table id="usersTable" class="table table-striped table-bordered" width="100%">
                                <thead>
                                <tr>
                                    <th data-hide="phone,tablet">
                                        <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                        <spring:message code="Lbl.Photo"/>
                                    </th>
                                    <th data-hide="phone,tablet">
                                        <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                        <spring:message code="Lbl.UserName"/>
                                    </th>
                                    <th data-hide="phone,tablet">
                                        <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                        <spring:message code="Lbl.Email"/>
                                    </th>
                                    <th data-hide="phone,tablet">
                                        <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                        <spring:message code="Lbl.Phone"/>
                                    </th>
                                    <th data-hide="phone,tablet">
                                        <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                        <spring:message code="Lbl.CardNumber"/>
                                    </th>
                                    <th data-hide="phone,tablet">
                                        <i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <a href="<c:url value="/administrator/event/list"/>">
                        <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </form>
            </div>
        </div>
    </jsp:body>
</t:wrapper>