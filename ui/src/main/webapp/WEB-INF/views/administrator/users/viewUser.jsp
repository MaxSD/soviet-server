﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:body>
        <div>
            <%@include file="/WEB-INF/jspf/messages.jspf" %>

            <div class="col-md-8 col-md-offset-2">
                <h4><spring:message code="Lbl.EditUser"/></h4>

                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.LastName"/></td>
                        <td>
                                ${userViewForm.lastName}
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.FirstName"/></td>
                        <td>
                                ${userViewForm.firstName}
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Patronymic"/></td>
                        <td>
                                ${userViewForm.patronymic}
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Login"/></td>
                        <td>
                                ${userViewForm.login}
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Email"/></td>
                        <td>
                                ${userViewForm.email}
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Roles"/></td>
                        <td>
                                ${roleName}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <a href="<c:url value="/administrator/users/list_users"/>">
                    <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                        <spring:message code="Btn.Cancel"/>
                    </button>
                </a>
            </div>
        </div>
    </jsp:body>
</t:wrapper>