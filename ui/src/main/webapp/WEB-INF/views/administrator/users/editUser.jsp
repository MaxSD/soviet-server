﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:body>
        <div>
            <%@include file="/WEB-INF/jspf/messages.jspf" %>

            <div class="col-md-8 col-md-offset-2">
                <form:form action="${pageContext.request.contextPath}/administrator/users/${userEditForm.id}/edit_user"
                           method="post"
                           modelAttribute="userEditForm" autocomplete="true">

                    <h4><spring:message code="Lbl.EditUser"/></h4>

                    <table class="b-table table table-striped">
                        <tbody>
                        <tr>
                            <td><spring:message code="Lbl.LastName"/></td>
                            <td>
                                <form:input type="text" path="lastName" required="required"
                                            class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.FirstName"/></td>
                            <td>
                                <form:input type="text" path="firstName" required="required"
                                            class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Patronymic"/></td>
                            <td>
                                <form:input type="text" path="patronymic" required="required"
                                            class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Login"/></td>
                            <td>
                                <form:input type="text" path="login" required="required"
                                            class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Email"/></td>
                            <td>
                                <form:input type="text" path="email" required="required"
                                            class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Password"/></td>
                            <td>
                                <form:password id="password" path="password"
                                               class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.ConfirmPassword"/></td>
                            <td>
                                <input type="password" id="confirmPassword" name="confirmPassword"
                                       class="form-control"/>
                            </td>
                        </tr>
                        <tr>
                            <td><spring:message code="Lbl.Role"/></td>
                            <td>
                                <c:if test="${not empty rolesList}">
                                    <form:select path="role.id" id="role" class="form-control">
                                        <form:options items="${rolesList}"/>
                                    </form:select>
                                </c:if>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <button class="btn btn-success margin-top-10 pull-right" type="submit" name="submit">
                        <spring:message code="Btn.Save"/>
                    </button>

                    <a href="<c:url value="/administrator/users/list_users"/>">
                        <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                            <spring:message code="Btn.Cancel"/>
                        </button>
                    </a>
                </form:form>
            </div>
        </div>
    </jsp:body>
</t:wrapper>