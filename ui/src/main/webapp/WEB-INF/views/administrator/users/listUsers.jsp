<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:body>
        <div class="jarviswidget jarviswidget-color-blueDark padding-10" id="wid-id-1"
             data-widget-editbutton="false">
            <form id="listUsersForm" name="listUsersForm" method="post">
                <table class="b-table b-table--search table table-striped">
                    <thead>
                    <tr>
                        <th data-hide="phone,tablet"><spring:message code="Lbl.ID"/></th>
                        <th data-hide="phone,tablet"><spring:message code="Lbl.LastName"/></th>
                        <th data-hide="phone,tablet"><spring:message code="Lbl.FirstName"/></th>
                        <th data-hide="phone,tablet"><spring:message code="Lbl.Patronymic"/></th>
                        <th data-hide="phone,tablet"><spring:message code="Lbl.Login"/></th>
                        <th data-hide="phone,tablet"><spring:message code="Lbl.Email"/></th>
                        <th data-hide="phone,tablet">
                            <i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i>
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:if test="${not empty usersList}">
                        <c:forEach items="${usersList}" var="user">
                            <tr>
                                <td>${user.id}</td>
                                <td>${user.lastName}</td>
                                <td>${user.firstName}</td>
                                <td>${user.patronymic}</td>
                                <td>${user.login}</td>
                                <td>${user.email}</td>
                                <td class="pull-right">
                                    <sec:authorize access="hasAnyRole('PERMISSION_EDIT_USERS')">
                                        <a href="<c:url value="/administrator/users/${user.id}/edit_user"/>"
                                           class="btn btn-xs btn-default">
                                            &nbsp;
                                            <i class="fa fa-edit"></i>
                                            &nbsp;
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('PERMISSION_VIEW_USERS')">
                                        <a href="<c:url value="/administrator/users/${user.id}/view_user"/>"
                                           class="btn btn-xs btn-default">
                                            &nbsp;
                                            <i class="fa fa-folder-open"></i>
                                            &nbsp;
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('PERMISSION_DELETE_USERS')">
                                        <c:if test="${user.id != 1}">
                                            <a href="#" onclick="deleteUser('${user.id}', '${user.login}');"
                                               class="btn btn-xs btn-default">
                                                &nbsp;
                                                <i class="fa fa-remove"></i>
                                                &nbsp;
                                            </a>
                                        </c:if>
                                    </sec:authorize>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </form>

                <sec:authorize access="hasAnyRole('PERMISSION_CREATE_USERS')">
                    <a href="<c:url value="/administrator/users/new_user"/>">
                        <button class="btn btn-success margin-top-10 pull-right" type="button" name="btnCreateNewUser">
                            <spring:message code="Btn.Create"/>
                        </button>
                    </a>
                </sec:authorize>
        </div>
    </jsp:body>
</t:wrapper>