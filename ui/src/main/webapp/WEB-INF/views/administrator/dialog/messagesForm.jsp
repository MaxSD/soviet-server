<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<style>
    .messageCell {
        min-height: 64px;
        width: 380px;
        color: #fff;
        border-color: #cccccc;
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
        text-align: left;
        margin: 0 auto;
        overflow: hidden;
        font-size: 13px;
        padding: 3px;
        position: relative;
    }

    .messageCell_Header {
        font-size: 12px;
        text-align: center;
        border-bottom: 2px #ffffff;
        font-weight: normal;
    }

    .fromAdmin {
        background-color: #434140;
    }

    .fromUser {
        background-color: #5c5a59;
    }

    #messagesList {
        height: 500px;
        overflow-y: scroll;
    }

    .modal-body {
        position: relative;
        padding: 0;
    }

    .smart-form fieldset, footer {
        background-color: #dddddd;
    }

    .buttons_section {
        display: block;
        padding: 7px 14px 15px;
        border-top: 1px solid rgba(0, 0, 0, .1);
        background-color: #dddddd;
    }
</style>

<!-- Modal - Messages of the dialog -->
<table border="0" style="width: 100%; background: #dddddd">
    <tr>
        <td style="width: 500px;">
            <div id="messagesList">
                ${messagesData}
            </div>
        </td>
        <td>
            <form id="messageForm" method="post"
                  action="${pageContext.request.contextPath}/administrator/dialog/sendMessage"
                  class="smart-form">

                <input type="hidden" id="dialogId" name="dialogId" value="${dialog.id}"/>
                <input type="hidden" id="senderId" name="senderId" value="${senderId}"/>
                <input type="hidden" id="receiverId" name="receiverId" value="${dialog.userId}"/>

                <c:if test="${!dialog.isClosed}">
                    <fieldset>
                        <section>
                            <div class="row">
                                <label class="label col col-2"><spring:message code="Lbl.Title"/></label>

                                <div class="col col-10">
                                    <label class="input">
                                        <input type="text" id="dialogTitle" name="dialogTitle">
                                    </label>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="button" class="btn btn-success"
                                onclick="changeDialogTitle(${dialog.id}, $('#dialogTitle').val());">
                            <spring:message code="Btn.Save"/>
                        </button>
                    </footer>

                    <fieldset>
                        <section>
                            <div class="row">
                                <label class="label col col-2"><spring:message code="Lbl.Message"/></label>

                                <div class="col col-10">
                                    <label class="input">
                                        <textarea id="messageText" name="messageText" style="width: 100%"></textarea>
                                    </label>
                                </div>
                            </div>
                        </section>

                        <section>
                            <div class="row">
                                <label class="label col col-2"><spring:message code="Lbl.File"/></label>

                                <div class="col col-10">
                                    <label class="input">
                                        <div class="input input-file">
                                            <span class="button"><input id="file" type="file" name="file"
                                                                        onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input
                                                type="text" readonly="">
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <fieldset>
                        <legend>Выставить счет</legend>
                        <section>
                            <div class="row">
                                <label class="label col col-2"><spring:message code="Lbl.ServiceDescription"/></label>

                                <div class="col col-10">
                                    <label class="input">
                                        <input type="text" name="serviceDescription">
                                    </label>
                                </div>
                            </div>
                        </section>
                        <section>
                            <div class="row">
                                <label class="label col col-2"><spring:message code="Lbl.ServicePrice"/></label>

                                <div class="col col-3">
                                    <label class="input">
                                        <input type="text" name="servicePrice">
                                    </label>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                </c:if>

                <c:choose>
                    <c:when test="${!dialog.isClosed}">
                        <footer>
                            <button id="sendMessage" name="sendMessage" type="submit" class="btn btn-primary">
                                <spring:message code="Btn.Send"/>
                            </button>
                            <button type="button" class="btn btn-warning" onclick="closeDialog(${dialog.id});"
                                    data-dismiss="modal">
                                <spring:message code="Btn.CloseDialog"/>
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <spring:message code="Btn.Cancel"/>
                            </button>
                        </footer>
                    </c:when>
                    <c:otherwise>
                        <p class="alert alert-warning alert-dismissable">
                            <spring:message code="Dialog.Closed"/>
                        </p>
                    </c:otherwise>
                </c:choose>
            </form>
        </td>
    </tr>
</table>

<script>
    $(document).ready(function () {
        // Reload messages list every 10 seconds
        refreshMessages = setInterval(function () {
            reloadMessages();
        }, 10000);

        scrollMessages();
    });

    function scrollMessages() {
        var wtf = $('#messagesList');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
    }

    /**
     * Send message
     */
    $('#messageForm').submit(function (event) {
        var formData = new FormData($('form')[0]);
        event.preventDefault();
        $.ajax({
            url: "<c:url value="/administrator/dialog/sendMessage"/>",
            type: "POST",
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function () {
                $("#sendMessage").prop('disabled', true);
            },
            success: function (response) {
                $('#messageText').val("")
                $('#file').val("")
                $('#serviceDescription').val("")
                $('#servicePrice').val("")
                reloadMessages();
                scrollMessages();
            },
            complete: function () {
                $("#sendMessage").prop('disabled', false);
            }
        });
        return false;
    });

    function changeDialogTitle(_dialogId, _dialogTitle) {
        $.ajax({
            url: "<c:url value="/administrator/dialog/changeDialogTitle"/>",
            type: "POST",
            data: {dialogId: _dialogId, dialogTitle: _dialogTitle},
            success: function (response) {
                $(".modal-title").html("Диалог - " + _dialogTitle);
                $('#dialogTitle').val("");
            }
        });
    }

    function closeDialog(_dialogId) {
        $.ajax({
            url: "<c:url value="/administrator/dialog/closeDialog"/>",
            type: "POST",
            data: {dialogId: _dialogId},
            success: function (response) {
                if (response.length)
                    $("#ajaxMessage").html(response);
                else
                    usersTable.ajax.reload();
            }
        });
    }

    function reloadMessages() {
        if ($('#messagesList').length) {
            $.ajax({
                url: "<c:url value="/administrator/dialog/get_dialog_messages"/>",
                type: "GET",
                cache: false,
                global: false,
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                dataType: "html",
                data: {dialogId: ${dialog.id}},
                success: function (response) {
                    $("#messagesList").html(response);
                    scrollMessages();
                }
            });
        } else {
            clearInterval(refreshMessages);
        }
    }
</script>
