﻿<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="/WEB-INF/customTags/customTag.tld" prefix="ct" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:wrapper admin="${admin}"
           page="${page}"
           photoAdminUrl="${photoAdminUrl}">
    <jsp:attribute name="js">
        <script>
            var eventsTable, ordersTable;

            $(document).ready(function () {
                pageSetUp();
                var responsiveHelper_datatable_events = undefined;
                var responsiveHelper_datatable_orders = undefined;
                var breakpointDefinition = {
                    tablet: 1024,
                    phone: 480
                };
                $("#birthDate").mask("99.99.9999");

                eventsTable = $('#eventsTable').DataTable({
                    iDisplayLength: 10,
                    "serverSide": true,
                    "bFilter": false,
                    "bSort": false,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_datatable_events) {
                            responsiveHelper_datatable_events = new ResponsiveDatatablesHelper($('#eventsTable'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_datatable_events.createExpandIcon(nRow);
                    },
                    "drawCallback": function () {
                        responsiveHelper_datatable_events.respond();
                    },
                    "ajax": {
                        "url": "${pageContext.request.contextPath}/administrator/event/get_events_by_userId_JSON",
                        "type": "POST",
                        "data": function (d) {
                            d.userId = "${viewForm.id}"
                        }
                    },
                    "language": {
                        "url": "<c:url value="/resources/datatable/${pageContext.response.locale}.json"/>"
                    }
                });

                // orders list
                ordersTable = $('#ordersTable').DataTable({
                    iDisplayLength: 10,
                    "serverSide": true,
                    "bFilter": false,
                    "bSort": false,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth": true,
                    "preDrawCallback": function () {
                        if (!responsiveHelper_datatable_orders) {
                            responsiveHelper_datatable_orders = new ResponsiveDatatablesHelper($('#ordersTable'), breakpointDefinition);
                        }
                    },
                    "rowCallback": function (nRow) {
                        responsiveHelper_datatable_orders.createExpandIcon(nRow);
                    },
                    "drawCallback": function () {
                        responsiveHelper_datatable_orders.respond();
                    },
                    "ajax": {
                        "url": "${pageContext.request.contextPath}/administrator/order/get_orders_by_userId_JSON",
                        "type": "POST",
                        "data": function (d) {
                            d.userId = "${viewForm.id}"
                        }
                    },
                    "language": {
                        "url": "<c:url value="/resources/datatable/${pageContext.response.locale}.json"/>"
                    }
                });
            });
        </script>
    </jsp:attribute>
    <jsp:body>
        <div>
            <div class="col-md-8 col-md-offset-2">
                <table class="b-table table table-striped">
                    <tbody>
                    <tr>
                        <td><spring:message code="Lbl.FirstName"/></td>
                        <td>
                            <input type="text" id="firstName" name="firstName" required="required"
                                   class="form-control" value="${viewForm.firstName}" readonly="readonly"/>
                        </td>

                        <td><spring:message code="Lbl.Surname"/></td>
                        <td>
                            <input type="text" id="surname" name="surname" required="required"
                                   class="form-control" value="${viewForm.surname}" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Gender"/></td>
                        <td>
                                <c:if test="${viewForm.gender == 0}">Ж</c:if>
                                <c:if test="${viewForm.gender == 1}">М</c:if>
                        </td>

                        <td><spring:message code="Lbl.Phone"/></td>
                        <td>
                            <input type="text" id="phone" name="phone" required="required"
                                   class="form-control" value="${viewForm.phone}" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.BirthDate"/></td>
                        <td>
                            <input type="text" id="birthDate" name="birthDate" required="required"
                                   class="form-control" value="${birthDate}" readonly="readonly"/>
                        </td>

                        <td><spring:message code="Lbl.Profession"/></td>
                        <td>
                            <input type="text" id="profession" name="profession" required="required"
                                   class="form-control" value="${viewForm.profession}" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.About"/></td>
                        <td>
                            <input type="text" id="about" name="about"
                                   class="form-control" value="${viewForm.about}" readonly="readonly"/>
                        </td>

                        <td><spring:message code="Lbl.Photo"/></td>
                        <td>
                            <img src="${viewForm.photo}" width="100"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.CardNumber"/></td>
                        <td>
                            <input type="text" id="cardNumber" name="cardNumber"
                                   class="form-control" value="${viewForm.cardNumber}" readonly="readonly"/>
                        </td>

                        <td><spring:message code="Lbl.Email"/></td>
                        <td>
                            <input type="text" id="email" name="email" required="required"
                                   class="form-control" value="${viewForm.email}" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <td><spring:message code="Lbl.Roles"/></td>
                        <td>
                                ${roleName}
                        </td>
                        <td><spring:message code="Lbl.FriendName"/></td>
                        <td>
                            <input type="text" id="friendName" name="friendName"
                                   class="form-control" value="${viewForm.friend}" readonly="readonly"/>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div>
                    <div class="jarviswidget" id="wid-id-11" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false">
                        <header>
                            <h2></h2>
                            <ul id="widget-tab-1" class="nav nav-tabs pull-right">
                                <li class="active">
                                    <a data-toggle="tab" href="#hr1"> <i class="fa fa-lg"></i> <span class="hidden-mobile hidden-tablet"><spring:message code="Lbl.Events"/></span></a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#hr2"> <i class="fa fa-lg"></i> <span class="hidden-mobile hidden-tablet"><spring:message code="Lbl.Orders"/></span></a>
                                </li>
                            </ul>
                        </header>
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <!-- widget body text-->
                                <div class="tab-content padding-10">
                                    <div class="tab-pane fade in active" id="hr1">
                                        <table id="eventsTable" class="table table-striped table-bordered" width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.ID"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.DateTime"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.Title"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.City"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.Price"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="hr2">
                                        <table id="ordersTable" class="table table-striped table-bordered" width="100%">
                                            <thead>
                                            <tr>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.ID"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.DateTime"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.Title"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-credit-card text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    <spring:message code="Lbl.Price"/>
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-fw fa-cog text-muted hidden-md hidden-sm hidden-xs"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- end widget body text-->
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </div>

                <a href="<c:url value="/administrator/svUser/list"/>">
                    <button class="btn btn-primary margin-top-10 pull-left" type="button" name="btnCancel">
                        <spring:message code="Btn.Return"/>
                    </button>
                </a>
            </div>
        </div>
    </jsp:body>
</t:wrapper>