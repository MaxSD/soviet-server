<%@tag description="Breadcrumb" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="page" required="true" %>

<ol class="breadcrumb">
    <li>
        <spring:message code="Dashboard.Title"/>
    </li>
    <c:if test="${page.contains('users')}">
        <li>
            <spring:message code="Navigation.Admin"/>
        </li>
    </c:if>
    <c:if test="${page.contains('roles')}">
        <li>
            <spring:message code="Btn.Roles"/>
        </li>
    </c:if>
    <c:if test="${page.contains('event')}">
        <li>
            <spring:message code="Navigation.Events"/>
        </li>
    </c:if>
</ol>
