<%--suppress JSUnresolvedLibraryURL--%>
<%@tag description="Meta information" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<meta charset="utf-8">
<title><spring:message code="Global.Title"/></title>
<meta name="description" content="">
<meta name="author" content="INSART">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="stylesheet" type="text/css" media="screen"
      href="<c:url value="/resources/smart_admin/css/bootstrap.min.css"/>"/>
<link rel="stylesheet" type="text/css" media="screen"
      href="<c:url value="/resources/smart_admin/css/font-awesome.min.css"/>"/>
<link rel="stylesheet" type="text/css" media="screen"
      href="<c:url value="/resources/smart_admin/css/smartadmin-production-plugins.min.css"/>">
<link rel="stylesheet" type="text/css" media="screen"
      href="<c:url value="/resources/smart_admin/css/smartadmin-production.min.css"/>">
<link rel="stylesheet" type="text/css" media="screen"
      href="<c:url value="/resources/smart_admin/css/smartadmin-skins.min.css"/>">
<link rel="stylesheet" type="text/css" media="screen"
      href="<c:url value="/resources/smart_admin/css/smartadmin-rtl.min.css"/>">

<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-switch.min.css"/>"/>
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-toggle.min.css"/>"/>
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/resources/css/server.css"/>"/>

<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
<link rel="apple-touch-icon" href="<c:url value="/resources/smart_admin/img/splash/sptouch-icon-iphone.png"/>"/>
<link rel="apple-touch-icon" sizes="76x76"
      href="<c:url value="/resources/smart_admin/img/splash/touch-icon-ipad.png"/>">
<link rel="apple-touch-icon" sizes="120x120"
      href="<c:url value="/resources/smart_admin/img/splash/touch-icon-iphone-retina.png"/>">
<link rel="apple-touch-icon" sizes="152x152"
      href="<c:url value="/resources/smart_admin/img/splash/touch-icon-ipad-retina.png"/>">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-startup-image" href="<c:url value="/resources/smart_admin/img/splash/ipad-landscape.png"/>"
      media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image" href="<c:url value="/resources/smart_admin/img/splash/ipad-portrait.png"/>"
      media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image" href="<c:url value="/resources/smart_admin/img/splash/iphone.png"/>"
      media="screen and (max-device-width: 320px)">