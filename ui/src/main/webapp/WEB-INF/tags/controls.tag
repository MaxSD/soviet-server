<%@tag description="Controls" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="hide-menu" class="btn-header pull-right">
    <span>
        <a href="#" data-action="toggleMenu"
           title="<spring:message code="Controls.Hint.Collapse"/>">
            <i class="fa fa-reorder"></i>
        </a>
    </span>
</div>
<div id="logout" class="btn-header transparent pull-right">
    <span>
        <a href="<c:url value="/logout"/>" title="<spring:message code="Controls.Hint.Logout"/>"
           data-action="userLogout"
           data-logout-msg="<spring:message code="UI.Confirm.Logout"/>">
            <i class="fa fa-sign-out"></i>
        </a>
    </span>
</div>
<div id="fullscreen" class="btn-header transparent pull-right">
    <span>
        <a href="#" data-action="launchFullscreen"
           title="<spring:message code="Controls.Hint.FullScreen"/>">
            <i class="fa fa-arrows-alt"></i>
        </a>
    </span>
</div>