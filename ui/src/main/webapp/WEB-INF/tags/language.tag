<%@tag description="Language" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<ul class="header-dropdown-list hidden-xs">
    <li>
        <c:if test="${pageContext.response.locale == 'en'}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<c:url value="/resources/smart_admin/img/blank.gif"/>" class="flag flag-us"
                    alt="United States"> <span> English (US) </span>
                <i class="fa fa-angle-down"></i>
            </a>
        </c:if>
        <c:if test="${pageContext.response.locale == 'ru'}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<c:url value="/resources/smart_admin/img/blank.gif"/>" class="flag flag-ru" alt="Russia">
                <span> Русский (RU) </span>
                <i class="fa fa-angle-down"></i>
            </a>
        </c:if>
        <ul class="dropdown-menu pull-right">
            <li
                    <c:if test="${pageContext.response.locale == 'en'}">
                        class="active"
                    </c:if>
                    >
                <a href="<c:url value="/?lang=en"/>">
                    <img src="<c:url value="/resources/smart_admin/img/blank.gif"/>"
                         class="flag flag-us" alt="United States">
                    English (US)
                </a>
            </li>
            <li
                    <c:if test="${pageContext.response.locale == 'ru'}">
                        class="active"
                    </c:if>
                    >
                <a href="<c:url value="/?lang=ru"/>">
                    <img src="<c:url value="/resources/smart_admin/img/blank.gif"/>"
                         class="flag flag-ru" alt="Russia">
                    Русский (RU)
                </a>
            </li>
        </ul>
    </li>
</ul>