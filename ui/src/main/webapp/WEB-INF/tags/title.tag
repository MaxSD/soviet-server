<%@tag description="Title" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="page" required="true" %>


<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
    <h1 class="page-title txt-color-blueDark">
        <c:if test="${page == 'dashboard'}">
            <i class="fa-fw fa fa-home"></i>
            <spring:message code="Dashboard.Title"/>
        </c:if>
        <c:if test="${page.contains('roles')}">
            <i class="fa-fw fa fa-key"></i>
            <spring:message code="Lbl.Roles"/>
        </c:if>
        <c:if test="${page.contains('users')}">
            <i class="fa-fw fa fa-users"></i>
            <spring:message code="Navigation.Admin"/>
        </c:if>
        <c:if test="${page.contains('svUser')}">
            <i class="fa-fw fa fa-list"></i>
            <spring:message code="Navigation.Users"/>
        </c:if>
        <c:if test="${page.contains('event')}">
            <i class="fa-fw fa fa-list"></i>
            <spring:message code="Navigation.Events"/>
        </c:if>
        <c:if test="${page.contains('notifications')}">
            <i class="fa-fw fa fa-list"></i>
            <spring:message code="Navigation.Notifications"/>
        </c:if>
        <c:if test="${page.contains('dialog')}">
            <i class="fa-fw fa fa-list"></i>
            <spring:message code="Navigation.Dialogs"/>
        </c:if>
        <c:if test="${page.contains('partners')}">
            <i class="fa-fw fa fa-list"></i>
            <spring:message code="Navigation.Partners"/>
        </c:if>
        <c:if test="${page.contains('settings')}">
            <i class="fa-fw fa fa-list"></i>
            <spring:message code="Navigation.Settings"/>
        </c:if>
    </h1>
</div>