<%@tag description="Message Success" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="messageSuccess" required="true" %>
<c:if test="${not empty messageSuccess}">
    <div class="col-md-6 col-md-offset-3" style="margin-top: 50px">
        <p class="alert alert-success alert-dismissable">
                ${messageSuccess}
        </p>
    </div>
</c:if>