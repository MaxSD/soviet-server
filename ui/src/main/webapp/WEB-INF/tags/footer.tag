<%--suppress JSUnresolvedLibraryURL --%>
<%@tag description="Footer" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="<c:url value="/resources/smart_admin/js/plugin/pace/pace.min.js"/>"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) { document.write('<script src="<c:url value="/resources/smart_admin/js/libs/jquery-2.1.1.min.js"/>"><\/script>');}
</script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) { document.write('<script src="<c:url value="/resources/smart_admin/js/libs/jquery-ui-1.10.3.min.js"/>"><\/script>');}
</script>
<script src="<c:url value="/resources/smart_admin/js/app.config.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/bootstrap/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/app.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/notification/SmartNotification.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/plugin/jquery-validate/jquery.validate.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/plugin/masked-input/jquery.maskedinput.min.js"/>"></script>
<script src="<c:url value="/resources/js/vendor/bootstrap-switch.min.js"/>"></script>
<script src="<c:url value="/resources/js/vendor/bootstrap-toggle.min.js"/>"></script>

<script src="<c:url value="/resources/smart_admin/js/smartwidgets/jarvis.widget.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/plugin/datatables/jquery.dataTables.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/plugin/datatables/dataTables.colVis.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/plugin/datatables/dataTables.tableTools.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/plugin/datatables/dataTables.bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/smart_admin/js/plugin/datatable-responsive/datatables.responsive.min.js"/>"></script>

<script src="<c:url value="/resources/js/vendor/bootbox.min.js"/>"></script>
<script src="<c:url value="/resources/js/jQueryAjax.js"/>"></script>
<script src="<c:url value="/resources/js/fieldsValidator.js"/>"></script>

<script src="<c:url value="/resources/js/plugin/flot/jquery.flot.cust.min.js"/>"></script>
<script src="<c:url value="/resources/js/plugin/flot/jquery.flot.fillbetween.min.js"/>"></script>
<script src="<c:url value="/resources/js/plugin/flot/jquery.flot.orderBar.min.js"/>"></script>
<script src="<c:url value="/resources/js/plugin/flot/jquery.flot.pie.min.js"/>"></script>
<script src="<c:url value="/resources/js/plugin/flot/jquery.flot.resize.min.js"/>"></script>
<script src="<c:url value="/resources/js/plugin/flot/jquery.flot.tooltip.min.js"/>"></script>

