<%@tag description="Left panel" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="page" required="true" %>

<ul>
    <li <c:if test="${page.contains('dashboard')}">class="active"</c:if>>
        <a href="<c:url value='/'/>" title="<spring:message code="Navigation.Dashboard"/>">
            <i class="fa fa-lg fa-fw fa-home"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Dashboard"/>
            </span>
        </a>
    </li>
    <li <c:if test="${page.contains('roles')}">class="active"</c:if>>
        <a href="<c:url value='/administrator/roles/list_roles'/>" title="<spring:message code="Navigation.Roles"/>">
            <i class="fa fa-lg fa-fw fa-key"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Roles"/>
            </span>
        </a>
    </li>
    <li <c:if test="${page.contains('svUser')}">class="active"</c:if>>
        <a href="<c:url value='/administrator/svUser/list'/>" title="<spring:message code="Navigation.Users"/>">
            <i class="fa fa-lg fa-fw fa-users"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Users"/>
            </span>
        </a>
    </li>
    <li <c:if test="${page.contains('event')}">class="active"</c:if>>
        <a href="<c:url value='/administrator/event/list'/>" title="<spring:message code="Navigation.Events"/>">
            <i class="fa fa-lg fa-fw fa-list"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Events"/>
            </span>
        </a>
    </li>
    <li <c:if test="${page.contains('notifications')}">class="active"</c:if>>
        <a href="<c:url value='/administrator/notifications/send'/>" title="<spring:message code="Navigation.Notifications"/>">
            <i class="fa fa-lg fa-fw fa-list"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Notifications"/>
            </span>
        </a>
    </li>
    <li <c:if test="${page.contains('dialog')}">class="active"</c:if>>
        <a href="<c:url value='/administrator/dialog/list'/>" title="<spring:message code="Navigation.Dialogs"/>">
            <i class="fa fa-lg fa-fw fa-list"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Dialogs"/>
            </span>
        </a>
    </li>
    <li <c:if test="${page.contains('priorities')}">class="active"</c:if>>
        <a href="<c:url value='/administrator/partners/list'/>" title="<spring:message code="Navigation.Partners"/>">
            <i class="fa fa-lg fa-fw fa-list"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Partners"/>
            </span>
        </a>
    </li>
    <li <c:if test="${page.contains('settings')}">class="active"</c:if>>
        <a href="<c:url value='/administrator/settings/settings'/>" title="<spring:message code="Navigation.Settings"/>">
            <i class="fa fa-lg fa-fw fa-list"></i>
            <span class="menu-item-parent">
                <spring:message code="Navigation.Settings"/>
            </span>
        </a>
    </li>
</ul>
