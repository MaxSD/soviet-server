<%@tag description="Wrapper" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="admin" required="true" type="com.wds.entity.SvtUser" %>
<%@attribute name="photoAdminUrl" required="true" %>
<%@attribute name="page" required="true" %>
<%@attribute name="successMessage" required="false" %>
<%@attribute name="errorMessage" required="false" %>
<%@attribute name="js" fragment="true" %>

<!DOCTYPE html>
<html>
<head>
    <t:meta/>
</head>
<body>
<header id="header">
    <t:logo/>
    <div class="pull-right">
        <t:controls/>
        <t:language/>
    </div>
</header>
<aside id="left-panel">
    <div class="login-info">
        <t:admin_info admin="${admin}"
                      photoAdminUrl="${photoAdminUrl}"/>
    </div>
    <nav>
        <t:navigation page="${page}"/>
    </nav>
    <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
    </span>
</aside>
<div id="main" role="main">
    <div id="ribbon">
        <t:breadcrumb page="${page}">
        </t:breadcrumb>
    </div>
    <div id="content">
        <div id="waitElement" style="display: none;"><img src="<c:url value="/resources/images/"/>loading_bot.gif">
        </div>
        <div>
            <t:title page="${page}">
            </t:title>
        </div>
        <c:if test="${not empty successMessage}">
            <t:message_success messageSuccess="${successMessage}"/>
        </c:if>
        <c:if test="${not empty errorMessage}">
            <t:message_warning warningMessage="${errorMessage}"/>
        </c:if>
        <div class="row">
            <jsp:doBody/>
        </div>
    </div>
</div>
<footer>
    <t:footer/>
</footer>
<jsp:invoke fragment="js"/>
</body>
</html>