<%@tag description="Error wrapper" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <t:meta/>
</head>
<body>
<div class="row text-align-center">
    <jsp:doBody/>
</div>
</body>
</html>