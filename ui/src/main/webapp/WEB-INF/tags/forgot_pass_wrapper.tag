<%@tag description="Forgot Password Wrapper" pageEncoding="UTF-8" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="js" fragment="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <t:meta/>
</head>
<body class="animated fadeInDown">
<header id="header">
    <t:logo/>
    <t:language/>
</header>
<div class="container i-line">
    <div class="row">
        <t:message_warning warningMessage="${warningMessage}" />
        <div class="col-md-4 col-md-offset-4 hidden-xs hidden-sm" style="margin-top: 50px">
            <jsp:doBody/>
        </div>
    </div>
</div>
<t:footer/>
<jsp:invoke fragment="js"/>
</body>
</html>
