<%@tag description="Admin info" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="warningMessage" required="true" %>
<c:if test="${not empty warningMessage}">
    <div class="col-md-6 col-md-offset-3" style="margin-top: 50px">
        <p class="alert alert-warning alert-dismissable">
                ${warningMessage}
        </p>
    </div>
</c:if>