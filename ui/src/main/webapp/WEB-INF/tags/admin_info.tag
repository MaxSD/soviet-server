<%@tag description="Admin info" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="admin" required="true" type="com.wds.entity.SvtUser" %>
<%@attribute name="photoAdminUrl" required="true" %>

<span>
    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
        <img src="${admin.photo}" alt="me"/>
        <span>
            ${admin.firstName}&nbsp;${admin.surname}
        </span>
    </a>
</span>