var jQueryAjax = {}, currAjaxRequest;
jQueryAjax.dialogWait = undefined;

jQueryAjax.showPleaseWait = function () {
    jQueryAjax.dialogWait = bootbox.dialog({
        title: "",
        keyboard: false,
        closeButton: false,
        className: "wait-dialog",
        message: $("#waitElement").html()
    })
};

jQueryAjax.hidePleaseWait = function () {
    if (jQueryAjax.dialogWait != undefined) {
        jQueryAjax.dialogWait.modal('hide');
    }
};

jQueryAjax.getServerName = function () {
    var host = window.location.protocol + '//' + window.location.hostname;
    var port = window.location.port;
    if (port != null)
        return host + ":" + port;
    else
        return host;
};

jQueryAjax.getURLPathName = function () {
    return window.location.pathname;
};

jQueryAjax.ajaxPOST_HTML = function (strURL, params, funcCallback, isASync, elementId, errorElemId) {
    jQueryAjax.ajaxRequest(strURL, params, 'POST', 'html', funcCallback, isASync, elementId, errorElemId);
};

jQueryAjax.ajaxPOST_HTML_Multipart = function (strURL, params, funcCallback, isASync, elementId, errorElemId) {
    jQueryAjax.ajaxRequestMultipart(strURL, params, 'POST', 'html', funcCallback, isASync, elementId, errorElemId);
};

jQueryAjax.ajaxGET_HTML = function (strURL, params, funcCallback, isASync, elementId, errorElemId) {
    jQueryAjax.ajaxRequest(strURL, params, 'GET', 'html', funcCallback, isASync, elementId, errorElemId);
};

jQueryAjax.ajaxPOST_TEXT = function (strURL, params, funcCallback, isASync, elementId, errorElemId) {
    jQueryAjax.ajaxRequest(strURL, params, 'POST', 'text', funcCallback, isASync, elementId, errorElemId);
};

jQueryAjax.ajaxPOST_XML = function (strURL, params, strMethod, funcCallback, isASync, elementId, errorElemId) {
    jQueryAjax.ajaxRequest(strURL, params, strMethod, 'xml', funcCallback, isASync, elementId, errorElemId);
};

jQueryAjax.loadPageToElement = function (strURL, params, strMethod, isASync, elementId, errorElemId) {
    return jQueryAjax.ajaxRequest(strURL, params, strMethod, 'html', null, isASync, elementId, errorElemId);
};

jQueryAjax.loadPageToDialog = function (title, data, dialogButtons) {
    bootbox.dialog({
        title: title,
        message: data,
        keyboard: false,
        buttons: dialogButtons
    })
};

jQueryAjax.showErrorDialog = function (title, message) {
    bootbox.dialog({
        title: title,
        message: "<span style='color: #8b0000;'>" + message + "</span>"
    })
};

jQueryAjax.ajaxRequest = function (strURL, aParams, strMethod, strDataType, funcCallback,
                                   isASync, elementId, errorElemId) {
    return $.ajax({
        url: strURL,
        async: isASync,
        type: strMethod,
        context: document.body,
        cache: false,
        global: false,
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        dataType: strDataType,
        data: aParams,
        beforeSend: function () {
            if (errorElemId != null && errorElemId != undefined) {
                $("#" + errorElemId).html('');
                $("#" + errorElemId).hide();
            }
            setTimeout(jQueryAjax.showPleaseWait, 1);
        },
        complete: function (jqXHR, textStatus) {
            setTimeout(jQueryAjax.hidePleaseWait, 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (errorElemId != null && errorElemId != undefined) {
                $("#" + errorElemId).html(jqXHR.responseText);
                $("#" + errorElemId).show();
            } else {
                if (errorThrown == 'CUSTOM')
                    jQueryAjax.showErrorDialog('Error!', textStatus + ': ' + errorThrown + "<br/>" + jqXHR.responseText);
                else
                    jQueryAjax.showErrorDialog('Error!', textStatus + ': ' + errorThrown);
            }
        },
        success: function (data, textStatus, jqXHR) {
            if (elementId != null) $('#' + elementId).html(data);
            if (funcCallback != null) funcCallback(data, textStatus, jqXHR);
        }
    });
};

jQueryAjax.ajaxRequestMultipart = function (strURL, aParams, strMethod, strDataType, funcCallback,
                                   isASync, elementId, errorElemId) {
    return $.ajax({
        url: strURL,
        async: isASync,
        type: strMethod,
        context: document.body,
        cache: false,
        global: false,
        processData: false,
        contentType: false,
        dataType: strDataType,
        data: aParams,
        beforeSend: function () {
            if (errorElemId != null && errorElemId != undefined) {
                $("#" + errorElemId).html('');
                $("#" + errorElemId).hide();
            }
            setTimeout(jQueryAjax.showPleaseWait, 1);
        },
        complete: function (jqXHR, textStatus) {
            setTimeout(jQueryAjax.hidePleaseWait, 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (errorElemId != null && errorElemId != undefined) {
                $("#" + errorElemId).html(jqXHR.responseText);
                $("#" + errorElemId).show();
            } else {
                if (errorThrown == 'CUSTOM')
                    jQueryAjax.showErrorDialog('Error!', textStatus + ': ' + errorThrown + "<br/>" + jqXHR.responseText);
                else
                    jQueryAjax.showErrorDialog('Error!', textStatus + ': ' + errorThrown);
            }
        },
        success: function (data, textStatus, jqXHR) {
            if (elementId != null) $('#' + elementId).html(data);
            if (funcCallback != null) funcCallback(data, textStatus, jqXHR);
        }
    });
};