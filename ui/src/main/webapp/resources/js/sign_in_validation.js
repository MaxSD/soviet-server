function signInValidate(idForm, msgEnterEmail, msgValidEmail,
                      msgEnterPassword) {
    $(idForm).validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        },
        messages: {
            email: {
                email: msgValidEmail,
                required: msgEnterEmail
            },
            password: {
                required: msgEnterPassword
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
}