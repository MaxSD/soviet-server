function forgotPassValidate(idForm, msgEnterEmail, msgValidEmail) {
    $(idForm).validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                email: msgValidEmail,
                required: msgEnterEmail
            }
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    });
}