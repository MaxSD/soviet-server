package com.wds.utils;

import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.CannotUploadFileException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

public class ApplicationUtility {

    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String GMT_0 = "GMT+0";
    public static final String GMT_1 = "GMT+1";
    public static final String GMT_2 = "GMT+2";
    public static final String GMT_3 = "GMT+3";

    public static String bcrypt(final String secret) {
        return BCrypt.hashpw(secret, BCrypt.gensalt());
    }

    public static boolean checkpw(String plaintext, String hashed) {
        return BCrypt.checkpw(plaintext, hashed);
    }

    public static String md5crypt(final String secret) {
        return MD5Util.md5Custom(secret);
    }

    public static long getCurrentTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }

    public static long getCurrentTimeStampGMT_0() {
        TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
        Calendar calendar = Calendar.getInstance(timeZone);

        return calendar.getTimeInMillis() / 1000;
    }

    public static long getDateInSeconds() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis() / 1000;
    }

    public static long getDateInSeconds(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis() / 1000;
    }

    public static long getDateTimeInSeconds(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.getTimeInMillis() / 1000;
    }

    public static Date getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Date getNextDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static long getNextDate(long dateInSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInSeconds * 1000);
        calendar.add(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis() / 1000;
    }

    public static String encodeStringForAjaxResponse(String strUTF8) {
        try {
            return new String(strUTF8.getBytes("UTF-8"), "ISO-8859-1");
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    public static void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) throws BaseException {
        try {
            int read;
            byte[] bytes = new byte[1024];

            OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            throw new CannotUploadFileException();
        }
    }

    public static int generateRandomInt(int min, int max) throws BaseException {
        return (1111 + (int) (Math.random() * ((9999 - 1111) + 1)));
    }

    public static String generateRandomString(int count) throws BaseException {
        return RandomStringUtils.random(count, true, true);
    }

    public static String secondsToFormattedString(long seconds, String timeZone, String format) throws BaseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        simpleDateFormat.setTimeZone(tz);
        return simpleDateFormat.format(seconds * 1000);
    }

    public static String millisecondsToFormattedString(long seconds, String format) throws BaseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(seconds);
    }

    public static String millisecondsToFormattedString(long seconds, String timeZone, String format) throws BaseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        simpleDateFormat.setTimeZone(tz);
        return simpleDateFormat.format(seconds);
    }

    public static Date formattedStringToDate(String dateTime, String timeZone, String format) {
        Date result = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            TimeZone tz = TimeZone.getTimeZone(timeZone);
            simpleDateFormat.setTimeZone(tz);
            result = simpleDateFormat.parse(dateTime);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static Date formattedStringToDate(String dateTime, String format) {
        Date result = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            result = simpleDateFormat.parse(dateTime);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return result;
    }

    public static boolean hasRole(String role) throws BaseException {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }

    public static void resizeImage(String originalImagePath) throws BaseException, IOException {
        try {
            BufferedImage originalImage = ImageIO.read(new File(originalImagePath));
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

            Number newWidth;
            Number newHeight;
            double dividerWidth = originalImage.getWidth() / 500;
            double dividerHeight = originalImage.getHeight() / 316;

            if (dividerWidth > 1 && dividerHeight > 1) {
                newWidth = originalImage.getWidth() / Math.min(dividerWidth, dividerHeight);
                newHeight = originalImage.getHeight() / Math.min(dividerWidth, dividerHeight);

                BufferedImage resizedImage = new BufferedImage(newWidth.intValue(), newHeight.intValue(), type);
                Graphics2D g = resizedImage.createGraphics();
                g.drawImage(originalImage, 0, 0, newWidth.intValue(), newHeight.intValue(), null);
                g.dispose();

                ImageIO.write(resizedImage, StringUtils.substringAfterLast(originalImagePath, "."), new File(originalImagePath));
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static String readFile(String path)
            throws IOException
    {
        byte[] content = Files.readAllBytes(Paths.get(path));
        return new String(content);
    }
}
