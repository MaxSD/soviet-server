package com.wds.utils.push.android;

import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AndroidPushNotification {

    private final static String GOOGLE_API_KEY = "key";
    private final static String GOOGLE_GCM_URL = "http://android.googleapis.com/gcm/send";

    public InputStream sendNotification(List<String> tokens, String message) throws IOException {

        HttpURLConnection connection = null;
        try {
            JSONObject jsonRootObject = new JSONObject();
            JSONObject jsonDataObject = new JSONObject();
            jsonRootObject.put("registration_ids", tokens.toArray());
            jsonDataObject.put("message", message);
            jsonRootObject.put("data", jsonDataObject);

            URL url = new URL(GOOGLE_GCM_URL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Connection", "Close");

            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Length", "" + jsonRootObject.toString().length());
            connection.setRequestProperty("Authorization", "key=" + GOOGLE_API_KEY);
            connection.setRequestProperty("Content-Type", "application/json");

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(jsonRootObject.toString());
            writer.flush();
            writer.close();
        } catch (Exception ex) {
            //System.out.println(e.getMessage());
            System.exit(1);
        }

        return connection.getInputStream();
    }

    public static void main(String[] args) {

        List<String> tokens = new ArrayList<>();
        tokens.add("APA91bGMV0lna0Rj2eedYOl3V-1WQJhedtFw5W2zawcTlR7V1LgrBtTi_J3aHvjfi9I85x1dgzEsXgLIEsb48NZ-1NviHimM_sFROvL9ODjGZpJohFV5wIBLHam9F-fLEq7kttqFTa-lMe2i3DprkEycESdn3DwhMnUn9mUY-A_4u4RnFGnVErw");

        try {
            AndroidPushNotification androidPushNotification = new AndroidPushNotification();
            androidPushNotification.sendNotification(tokens, "тест!");
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }
    }
}
