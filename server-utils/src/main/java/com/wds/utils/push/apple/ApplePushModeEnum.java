package com.wds.utils.push.apple;

public enum ApplePushModeEnum {
    ADHOCK,
    PRODUCTION
}
