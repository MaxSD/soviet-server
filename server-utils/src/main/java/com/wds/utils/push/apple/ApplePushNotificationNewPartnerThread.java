package com.wds.utils.push.apple;

import com.wds.utils.define.Defines;
import javapns.communication.ConnectionToAppleServer;
import javapns.devices.Devices;
import javapns.notification.*;
import org.apache.log4j.BasicConfigurator;

import java.util.List;

public class ApplePushNotificationNewPartnerThread extends Thread {

    private List<String> tokenList;
    private long id;

    public ApplePushNotificationNewPartnerThread(List<String> tokenList,
                                                 long id) {
        super("Push thread");
        this.tokenList = tokenList;
        this.id = id;
    }

    public void run() {
        try {
            BasicConfigurator.configure();
            PushNotificationManager pushManager = new PushNotificationManager();
            pushManager.initializeConnection(new AppleNotificationServerBasicImpl(Defines.getPushNotificationPath(), Defines.getPushNotificationPassword(), ConnectionToAppleServer.KEYSTORE_TYPE_JKS, Defines.getDebugServerFlag()));
            PushNotificationPayload payload = PushNotificationPayload.alert("We have a new partner! Read more...");
            payload.addCustomDictionary("isPartner", true);
            payload.addCustomDictionary("id", id);
            PushedNotifications notifications = pushManager.sendNotifications(payload, Devices.asDevices(tokenList));
            System.out.println("<Sent notifications> - " + notifications.getSuccessfulNotifications().size());
            for (PushedNotification notification : notifications.getSuccessfulNotifications()) {
                System.out.println("Device token - " + notification.getDevice().getToken());
            }
            System.out.println("</Sent notifications>");
            System.out.println("<Failed notifications> - " +  notifications.getFailedNotifications().size());
            for (PushedNotification notification : notifications.getFailedNotifications()) {
                System.out.println("Device token - " + notification.getDevice().getToken());
                System.out.println("Message - " + notification.getResponse().getMessage());
                System.out.println("Status - " + notification.getResponse().getStatus());
            }
            System.out.println("</Failed notifications>");
        } catch (Exception e) {
            System.out.println("Push notification exception");
        }
    }
}
