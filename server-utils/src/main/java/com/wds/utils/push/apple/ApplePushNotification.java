package com.wds.utils.push.apple;

import com.wds.utils.define.Defines;
import com.wds.exception.v1.BaseException;
import com.wds.exception.v1.PushNotificationException;
import javapns.communication.ConnectionToAppleServer;
import javapns.devices.Devices;
import javapns.notification.AppleNotificationServerBasicImpl;
import javapns.notification.PushNotificationManager;
import javapns.notification.PushNotificationPayload;
import org.apache.log4j.BasicConfigurator;

import java.util.List;

public class ApplePushNotification {
    public static void sendListNotification(String message, List<String> tokenList) throws BaseException {
        try {
            BasicConfigurator.configure();
            PushNotificationManager pushManager = new PushNotificationManager();
            pushManager.initializeConnection(new AppleNotificationServerBasicImpl(Defines.getPushNotificationPath(), Defines.getPushNotificationPassword(),
                    ConnectionToAppleServer.KEYSTORE_TYPE_JKS, Defines.getDebugServerFlag()));
            PushNotificationPayload payload = PushNotificationPayload.alert(message);
            payload.addSound("default");
            pushManager.sendNotifications(payload, Devices.asDevices(tokenList));
        } catch (Throwable e) {
            throw new PushNotificationException();
        }
    }
}
