package com.wds.utils.define;

public class DebugDefines {
    public static final String  XSL_PATH_STORAGE =                      "/var/www/wd/data/www/saferadmin.webdevelopers.su/xsl";

    public static final String  PUSH_ADHOCK_CERTIFICATE_PATH     =      "/var/www/wd/data/www/soviet/certificate/apns-adhoc.jks";
    public static final String  PUSH_ADHOCK_CERTIFICATE_PASSWORD =       "1234567890";

    public static final String  PUSH_PRODUCTION_CERTIFICATE_PATH =       "/var/www/wd/data/www/soviet/certificate/apns-prod.jks";
    public static final String  PUSH_PRODUCTION_CERTIFICATE_PASSWORD =   "1234567890";

    public static final String  IMAGES_PATH_STORAGE =              "/var/www/wd/data/www/flashmober.webdevelopers.su/images/";
    public final static String  HOST_TO_IMAGES = "http://5.45.121.172:8181/soviet-server/rest/v1/images/";

    public static final String  BOOKS_PATH_STORAGE =              "/var/www/wd/data/www/flashmober.webdevelopers.su/books/";
    public final static String  HOST_TO_BOOKS = "http://5.45.121.172/books/";
}
