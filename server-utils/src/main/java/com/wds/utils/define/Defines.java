package com.wds.utils.define;

import com.wds.utils.push.apple.ApplePushModeEnum;

public class Defines {

    // Notifications
    public final static String NOTIFICATION_ERROR_PUSH = "pushError";
    public final static String NOTIFICATION_ERROR_SMS = "smsError";
    public final static String NOTIFICATION_ERROR_EMAIL = "emailError";

    // SMTP
    public final static String SMTP_HOST = "smtp.gmail.com";
    public final static String SMTP_PORT = "587";
    public final static String SMTP_USER = "members@sovet.org.uk";
    public final static String SMTP_PASSWORD = "SovetLondon2014";
    public final static boolean SMTP_AUTH = true;

    public static boolean DEBUG_MODE = true;
    public static ApplePushModeEnum PUSH_MODE = ApplePushModeEnum.ADHOCK;

    public static void setServerMode(boolean serverMode) {
        DEBUG_MODE = true;
    }

    public static String getPushNotificationPath() {
        if (DEBUG_MODE) {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return DebugDefines.PUSH_ADHOCK_CERTIFICATE_PATH;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PATH;
        } else {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return ReleaseDefines.PUSH_ADHOCK_CERTIFICATE_PATH;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return ReleaseDefines.PUSH_PRODUCTION_CERTIFICATE_PATH;
        }
        return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PATH;
    }

    public static String getPushNotificationPassword() {
        if (DEBUG_MODE) {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return DebugDefines.PUSH_ADHOCK_CERTIFICATE_PASSWORD;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PASSWORD;
        } else {
            if (PUSH_MODE == ApplePushModeEnum.ADHOCK) return ReleaseDefines.PUSH_ADHOCK_CERTIFICATE_PASSWORD;
            if (PUSH_MODE == ApplePushModeEnum.PRODUCTION) return ReleaseDefines.PUSH_PRODUCTION_CERTIFICATE_PASSWORD;
        }
        return DebugDefines.PUSH_PRODUCTION_CERTIFICATE_PASSWORD;
    }

    public static boolean getDebugServerFlag() {
        return PUSH_MODE != ApplePushModeEnum.ADHOCK;
    }

    public static String getXslPathStorage() {
        return DEBUG_MODE ? DebugDefines.XSL_PATH_STORAGE : ReleaseDefines.XSL_PATH_STORAGE;
    }

    public static String getImagesPathStorage() {
        return DEBUG_MODE ? DebugDefines.IMAGES_PATH_STORAGE : ReleaseDefines.IMAGES_PATH_STORAGE;
    }

    public static String getHostImages() { return DEBUG_MODE ? DebugDefines.HOST_TO_IMAGES : ReleaseDefines.HOST_TO_IMAGES; }

    public static String getBooksPathStorage() {
        return DEBUG_MODE ? DebugDefines.BOOKS_PATH_STORAGE : ReleaseDefines.BOOKS_PATH_STORAGE;
    }

    public static String getHostBooks() { return DEBUG_MODE ? DebugDefines.HOST_TO_BOOKS : ReleaseDefines.HOST_TO_BOOKS; }
}
