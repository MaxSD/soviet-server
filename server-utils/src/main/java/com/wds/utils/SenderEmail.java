package com.wds.utils;

import com.wds.utils.define.Defines;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

@Service("SenderEmail")
public class SenderEmail {

    public void sendEmail(String recipientEmail, String subject, String message, boolean isHTML) throws Exception {
        Session session;

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", Defines.SMTP_HOST);
        props.put("mail.smtp.port", Defines.SMTP_PORT);
        props.put("mail.smtp.auth", Defines.SMTP_AUTH);

        if (Defines.SMTP_AUTH)
            session = Session.getInstance(props, getAuthenticator());
        else
            session = Session.getInstance(props, null);

        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(Defines.SMTP_USER);
            msg.setRecipients(Message.RecipientType.TO, recipientEmail);
            msg.setSubject(subject);
            msg.setSentDate(new Date());

            if (isHTML)
                msg.setContent(message, "text/html; charset=utf-8");
            else
                msg.setText(message);

            Transport.send(msg);
        } catch (MessagingException mex) {
            //TODO save full error message to the log
            throw new Exception("Email sending failed due: " + mex.getMessage());
        }
    }

    private Authenticator getAuthenticator() {
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Defines.SMTP_USER, Defines.SMTP_PASSWORD);
            }
        };
    }
}
