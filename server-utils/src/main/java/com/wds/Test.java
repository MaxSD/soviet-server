package com.wds;

import com.wds.utils.ApplicationUtility;
import com.wds.utils.SenderEmail;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Test {

    private Logger logger = LoggerFactory.getLogger(Test.class);

    public static void main(String[] args) {
        try {
            System.out.println(ApplicationUtility.getNextDate(1451253600));
            System.out.println(ApplicationUtility.getCurrentTimeStamp());
            System.out.println(ApplicationUtility.millisecondsToFormattedString(
                    1451203200000l,
                    ApplicationUtility.GMT_0,
                    "dd.MM.yyyy HH:mm"));

            System.out.println(ApplicationUtility.formattedStringToDate("27.11.1981", ApplicationUtility.GMT_0, "dd.MM.yyyy").getTime() / 1000);


            TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
            Calendar calendar = Calendar.getInstance(timeZone);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);

            System.out.println(
                    ApplicationUtility.formattedStringToDate(
                            ApplicationUtility.millisecondsToFormattedString(new Date().getTime(), "MM.yyyy"),
                            ApplicationUtility.GMT_0,
                            "MM.yyyy").getTime()
                    );

            //SenderEmail senderEmail = new SenderEmail();
            //senderEmail.sendEmail("maxim.zemskov@gmail.com", "Subject", "test", false);

            System.out.println(StringUtils.substringAfterLast("http://5.45.121.172:8181/soviet-server/rest/v1/images/1469455415RnGRte.jpg", "/"));
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
