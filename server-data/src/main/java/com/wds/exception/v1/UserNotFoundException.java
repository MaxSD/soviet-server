package com.wds.exception.v1;

public class UserNotFoundException extends BaseException {

    public UserNotFoundException() {
        super();
        this.code = 404;
        this.localCode = 1;
        this.message = "Failed. User not found!";
    }
}