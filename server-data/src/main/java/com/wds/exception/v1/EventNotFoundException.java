package com.wds.exception.v1;

public class EventNotFoundException extends BaseException {

    public EventNotFoundException() {
        super();
        this.code = 404;
        this.localCode = 2;
        this.message = "Failed. Event not found!";
    }
}