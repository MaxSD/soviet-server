package com.wds.exception.v1;

public class UserWithThisEmailAlreadyExistsException extends BaseException {

    public UserWithThisEmailAlreadyExistsException() {
        super();
        this.code = 400;
        this.localCode = 100;
        this.message = "Failed. User with this Email already exists!";
    }
}