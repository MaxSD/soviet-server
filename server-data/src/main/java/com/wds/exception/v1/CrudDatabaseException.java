package com.wds.exception.v1;

public class CrudDatabaseException extends BaseException {

    public CrudDatabaseException() {
        super();
        this.code = 500;
        this.localCode = 1;
        this.message = "Failed. CRUD operation with database!";
    }
}
