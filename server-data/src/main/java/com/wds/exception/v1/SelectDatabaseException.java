package com.wds.exception.v1;

public class SelectDatabaseException extends BaseException {

    public SelectDatabaseException() {
        super();
        this.code = 500;
        this.localCode = 2;
        this.message = "Failed. Select operation with database!";
    }
}