package com.wds.dao;

import com.wds.dataObjects.UsersOnEventFormData;
import com.wds.database.HibernateUtil;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.SelectDatabaseException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtUsersDao")
public class SvtUsersDao {

    private Logger logger = LoggerFactory.getLogger(SvtUsersDao.class);

    public SvtUser findByCardNumber(String cardNumber) throws SelectDatabaseException {
        SvtUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtUser.class).
                    add(Restrictions.eq("cardNumber", cardNumber));
            user = (SvtUser) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return user;
    }

    public SvtUser findByFirstNameAndSurname(String firstName, String surname) throws SelectDatabaseException {
        SvtUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtUser.class).
                    add(Restrictions.and(
                            Restrictions.eq("firstName", firstName),
                            Restrictions.eq("surname", surname)
                    ));
            user = (SvtUser) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return user;
    }

    public SvtUser findByEmail(String email) throws SelectDatabaseException {
        SvtUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtUser.class).
                    add(Restrictions.eq("email", email));
            user = (SvtUser) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return user;
    }

    public SvtUser findAdminByEmail(String email) throws SelectDatabaseException {
        SvtUser user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtUser.class).
                    add(Restrictions.eq("email", email));
            user = (SvtUser) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public List<SvtUser> getUsersByTagIds(List<Long> tags) throws SelectDatabaseException {
        List<SvtUser> result;
        Transaction transaction = null;
        try {
            String sreSql = "SELECT DISTINCT u.* " +
                    "FROM svt_users AS u, svt_user_interests AS ui " +
                    "WHERE u.id = ui.user_id AND ui.tag_id IN (" + StringUtils.join(tags, ",") + ")";

            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().createSQLQuery(sreSql).list();
            result = (List<SvtUser>) HibernateUtil.getSession().createSQLQuery(sreSql).addEntity(SvtUser.class).list();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<SvtUser> getUsersListByRoleId(long roleId, int offset, int limit) throws SelectDatabaseException {
        List<SvtUser> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            list = (List<SvtUser>) session.createCriteria(SvtUser.class).
                    add(Restrictions.eq("role.id", roleId)).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
                    setFirstResult(offset).
                    setMaxResults(limit).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<UsersOnEventFormData> getUsersListWithEventsByRoleId(long roleId, long eventId, int start, int count) throws SelectDatabaseException {
        List<UsersOnEventFormData> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String strSQL = "SELECT " +
                    "  u.id AS \"userId\", " +
                    "  u.photo, " +
                    "  u.first_name || ' ' || u.surname AS \"userName\", " +
                    "  u.email, " +
                    "  u.phone, " +
                    "  u.card_number AS \"cardNumber\", " +
                    "  ue.id AS \"userEventId\" " +
                    "FROM svt_users AS u " +
                    "  LEFT JOIN svt_user_events AS ue ON (ue.user_id = u.id AND ue.event_id = " + eventId + ") " +
                    "WHERE u.id_role = " + roleId;

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            session.beginTransaction();
            list = session.createSQLQuery(strSQL).
                    addScalar("userId", new LongType()).
                    addScalar("photo").
                    addScalar("userName").
                    addScalar("email").
                    addScalar("phone").
                    addScalar("cardNumber").
                    addScalar("userEventId", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(UsersOnEventFormData.class)).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public Number getCount(long roleId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUser.class).
                    add(Restrictions.eq("role.id", roleId)).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public Number getAcceptedUsersCount() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUser.class).
                    add(Restrictions.ne("cardNumber", "")).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public Number getActiveUsersCount() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUser.class).
                    add(Restrictions.and(
                            Restrictions.gt("eventsScore", 0),
                            Restrictions.gt("ordersScore", 0),
                            Restrictions.gt("partnersScore", 0)
                    )).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }
}
