package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.SvtOrder;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtOrdersDao")
public class SvtOrdersDao {

    private Logger logger = LoggerFactory.getLogger(SvtOrdersDao.class);

    @SuppressWarnings("unchecked")
    public List<SvtOrder> getOrdersListByUserId(long userId, boolean isPaid,
                                                int start, int count) throws SelectDatabaseException {
        List<SvtOrder> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String strSQL = "SELECT " +
                    "  o.* " +
                    "FROM svt_orders AS o " +
                    "WHERE o.uid = " + userId + " AND o.is_paid = " + isPaid;

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            session.beginTransaction();
            list = session.createSQLQuery(strSQL).addEntity(SvtOrder.class).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public Number getCount(long uid) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtOrder.class).
                    add(Restrictions.eq("uid", uid)).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public Double getPaidOrdersTotalAmount() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Double result;
        try {
            String strSql = "SELECT COALESCE(SUM(o.price), 0) " +
                    "FROM svt_orders AS o WHERE o.is_paid";

            session.beginTransaction();
            result = (Double) session.createSQLQuery(strSql).uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return result;
    }
}
