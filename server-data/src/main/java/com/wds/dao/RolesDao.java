package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.Role;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("RolesDao")
public class RolesDao {

    private Logger logger = LoggerFactory.getLogger(RolesDao.class);

    public Role findById(long id) throws SelectDatabaseException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Role role;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Role.class).add(Restrictions.eq("id", id));
            role = (Role) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return role;
    }

    public List<Role> findAll() throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();



            Criteria criteria = session.createCriteria(Role.class).
                    addOrder(Order.asc("id")).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            @SuppressWarnings("unchecked")
            List<Role> rolesList = criteria.list();
            transaction.commit();

            return rolesList;
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
    }

    public List<Map<String, String>> getFullDataList(String locale) throws CrudDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        List<Map<String, String>> rolesList = new ArrayList<>();
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = session.createCriteria(Role.class).
                    addOrder(Order.asc("id")).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            @SuppressWarnings("unchecked")
            List<Role> instance = criteria.list();
            Map<String, String> roleData;
            for (Role role : instance) {
                roleData = new HashMap<>();
                roleData.put("id", role.getId().toString());
                roleData.put("text", role.getText().get(locale));
                roleData.put("description", role.getDescription().get(locale));
                rolesList.add(roleData);
            }
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
        return rolesList;
    }

    public Map<Long, String> getAll(String locale) throws CrudDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        Map<Long, String> roleList = new HashMap<>();
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = session.createCriteria(Role.class).
                    addOrder(Order.asc("id")).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            @SuppressWarnings("unchecked")
            List<Role> instance = criteria.list();
            for (Role role : instance) {
                roleList.put(role.getId(), role.getText().get(locale));
            }
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
        return roleList;
    }

    public void create(Role record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }

    public void update(Role record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }

    public void delete(Role record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }
}
