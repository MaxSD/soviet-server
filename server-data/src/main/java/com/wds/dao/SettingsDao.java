package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.Settings;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service("SettingsDao")
public class SettingsDao {

    private Logger logger = LoggerFactory.getLogger(SettingsDao.class);

    public Settings findByParamName(String paramName) throws SelectDatabaseException {
        Transaction transaction = null;
        try {
            Settings appSetting;
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(Settings.class).
                    add(Restrictions.eq("paramName", paramName));
            appSetting = (Settings) criteria.uniqueResult();
            transaction.commit();
            return appSetting;
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
    }

    public void update(Settings record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }
}
