package com.wds.dao;

import com.wds.dataObjects.EventsForChartData;
import com.wds.database.HibernateUtil;
import com.wds.database.define.Defines;
import com.wds.entity.*;
import com.wds.exception.v1.SelectDatabaseException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtEventsDao")
public class SvtEventsDao {

    private Logger logger = LoggerFactory.getLogger(SvtEventsDao.class);

    public void create(SvtEvent event, List<Long> tags) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.persist(event);

            // Save new event's tags
            if (tags != null) {
                for (Long tagId : tags) {
                    SvtEventTags eventTags = new SvtEventTags();
                    eventTags.setEventId(event.getId());
                    eventTags.setTagId(tagId);
                    session.persist(eventTags);
                }
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
    }

    public void update(SvtEvent event, List<Long> tags) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(event);

            // Save event's tags
            if (tags != null) {

                // Delete old event's tags
                SvtEventTagsDao eventTagsDao = new SvtEventTagsDao();
                eventTagsDao.deleteEventTags(event.getId());

                for (Long tagId : tags) {
                    SvtEventTags eventTags = new SvtEventTags();
                    eventTags.setEventId(event.getId());
                    eventTags.setTagId(tagId);
                    session.persist(eventTags);
                }
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<SvtEvent> findByCityId(long cityId, int start, int count) throws SelectDatabaseException {
        List<SvtEvent> result;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtEvent.class).
                    add(Restrictions.and(
                                    Restrictions.eq("cityId", cityId),
                                    Restrictions.eq("status", Defines.EVENT_STATUS_ACCEPTED))
                    );

            if (start > 0)
                criteria.setFirstResult(start);

            if (count > 0)
                criteria.setMaxResults(count);

            result = criteria.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Number getParticipantsCount(long eventId) throws SelectDatabaseException {
        Number result;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtUserEvents.class).
                    add(Restrictions.eq("eventId", eventId)).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            result = (Number) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Number getCommentsCount(long eventId) throws SelectDatabaseException {
        Number result;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtEventComment.class).
                    add(Restrictions.eq("eventId", eventId)).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            result = (Number) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getCommentsList(long eventId, int start, int count, Class entityObject) throws SelectDatabaseException {
        List<Object> result;
        Transaction transaction = null;
        try {
            String strSQL = "SELECT " +
                    "  ec.id                            AS id, " +
                    "  u.id                             AS uid, " +
                    "  u.photo                          AS \"userPhoto\", " +
                    "  u.first_name || ' ' || u.surname AS \"userName\", " +
                    "  ec.message, " +
                    "  ec.date " +
                    "FROM svt_event_comments AS ec " +
                    "  INNER JOIN svt_users AS u ON (u.id = ec.user_id) " +
                    "WHERE event_id = " + eventId;

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            transaction = HibernateUtil.beginTransaction();
            result = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("id", new LongType()).
                    addScalar("uid", new LongType()).
                    addScalar("userPhoto").
                    addScalar("userName").
                    addScalar("message").
                    addScalar("date", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject)).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(re.getMessage());
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<EventsForChartData> getEventsForChart() throws SelectDatabaseException {
        List<EventsForChartData> result;
        Transaction transaction = null;
        try {
            String strSQL = "SELECT e.curr_month_ms, COUNT(*) " +
                    "FROM svt_events AS e " +
                    "GROUP BY e.curr_month_ms " +
                    "ORDER BY e.curr_month_ms ASC ";

            transaction = HibernateUtil.beginTransaction();
            result = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("curr_month_ms", new LongType()).
                    addScalar("count", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(EventsForChartData.class)).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(re.getMessage());
            throw new SelectDatabaseException();
        }
        return result;
    }
}
