package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.Permission;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("PermissionsDao")
public class PermissionsDao {

    private Logger logger = LoggerFactory.getLogger(PermissionsDao.class);

    public Permission findById(long id) throws SelectDatabaseException {
        Session session = HibernateUtil.getSession();
        Permission permission;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = session.createCriteria(Permission.class).add(Restrictions.eq("id", id));
            permission = (Permission) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return permission;
    }

    public List<Map<String, String>> getFullDataList(String locale) throws CrudDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        List<Map<String, String>> permissionsList = new ArrayList<>();
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = session.createCriteria(Permission.class);
            @SuppressWarnings("unchecked")
            List<Permission> instance = criteria.list();
            Map<String, String> permissionData;
            for (Permission permission : instance) {
                permissionData = new HashMap<>();
                permissionData.put("id", permission.getId().toString());
                permissionData.put("name", permission.getName());
                permissionData.put("text", permission.getText().get(locale));
                permissionData.put("description", permission.getDescription().get(locale));
                permissionsList.add(permissionData);
            }
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
        return permissionsList;
    }

    public Map<Long, String> getAll(String locale) throws CrudDatabaseException {
        Session session = HibernateUtil.getSession();
        Transaction transaction = null;
        Map<Long, String> permissionsList = new HashMap<>();
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = session.createCriteria(Permission.class);
            @SuppressWarnings("unchecked")
            List<Permission> instance = criteria.list();
            for (Permission permission : instance) {
                permissionsList.put(permission.getId(), permission.getText().get(locale));
            }
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
        return permissionsList;
    }
}
