package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.User;
import com.wds.exception.v1.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("UsersDao")
public class UsersDao {

    private Logger logger = LoggerFactory.getLogger(UsersDao.class);

    public User findById(long id) throws SelectDatabaseException {
        User user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    add(Restrictions.eq("id", id));
            user = (User) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return user;
    }

    public User findByLogin(String login) throws SelectDatabaseException {
        User user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    add(Restrictions.eq("login", login));
            user = (User) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return user;
    }

    public User findByEmail(String email) throws SelectDatabaseException {
        User user;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    add(Restrictions.eq("email", email));
            user = (User) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return user;
    }

    public List<User> findByRoleId(long roleId) throws SelectDatabaseException {
        List<User> userList;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    add(Restrictions.eq("role.id", roleId)).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            userList = criteria.list();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return userList;
    }

    @SuppressWarnings("unchecked")
    public List<User> getAll() throws SelectDatabaseException {
        Transaction transaction = null;
        List<User> userList;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(User.class).
                    addOrder(Order.asc("id")).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            userList = criteria.list();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return userList;
    }

    public void create(User record) throws BaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (ConstraintViolationException ce) {
            if (StringUtils.equals(ce.getConstraintName(), "srv_admins_email_key"))
                throw new UserWithThisEmailAlreadyExistsException();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }

    public void update(User record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(record);
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }

    public void delete(User record) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().delete(record);
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }
}
