package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.SvtUserToken;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtUserTokenDao")
public class SvtUserTokenDao {

    private Logger logger = LoggerFactory.getLogger(SvtUserTokenDao.class);

    public SvtUserToken findById(long id) throws SelectDatabaseException {
        SvtUserToken userToken;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserToken.class).
                    add(Restrictions.eq("id", id));
            userToken = (SvtUserToken) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return userToken;
    }

    @SuppressWarnings("unchecked")
    public SvtUserToken findByUserId(long userId) throws SelectDatabaseException {
        SvtUserToken userToken;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserToken.class).
                    add(Restrictions.eq("userId", userId));
            userToken = (SvtUserToken) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return userToken;
    }

    @SuppressWarnings("unchecked")
    public List<SvtUserToken> findByUsersIds(List<Long> usersIds) throws SelectDatabaseException {
        List<SvtUserToken> userTokenList;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserToken.class).
                    add(Restrictions.eq("userId", usersIds));
            userTokenList = (List<SvtUserToken>) criteria.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return userTokenList;
    }

    @SuppressWarnings("unchecked")
    public SvtUserToken findByToken(String token) throws SelectDatabaseException {
        SvtUserToken userToken;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserToken.class).
                    add(Restrictions.eq("token", token));
            userToken = (SvtUserToken) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return userToken;
    }

    @SuppressWarnings("unchecked")
    public List<SvtUserToken> getAllForApple() throws SelectDatabaseException {
        List<SvtUserToken> userTokenList;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserToken.class).
                    add(Restrictions.eq("isIsAndroid", false));
            userTokenList = (List<SvtUserToken>) criteria.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return userTokenList;
    }

    @SuppressWarnings("unchecked")
    public List<String> getTokensListForApple() throws SelectDatabaseException {
        List<String> tokenList;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserToken.class).
                    add(Restrictions.eq("isIsAndroid", false)).setProjection(Projections.property("token"));
            tokenList = (List<String>) criteria.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return tokenList;
    }
}
