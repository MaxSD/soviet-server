package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.PasswordRecovery;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service("PasswordRecoveryDao")
public class PasswordRecoveryDao {

    private Logger logger = LoggerFactory.getLogger(PasswordRecoveryDao.class);

    public PasswordRecovery findBySessionId(String sessionId) throws SelectDatabaseException {
        PasswordRecovery passwordRecovery;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(PasswordRecovery.class).
                    add(Restrictions.eq("sessionId", sessionId));
            passwordRecovery = (PasswordRecovery) criteria.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return passwordRecovery;
    }

    public void create(PasswordRecovery passwordRecovery) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            HibernateUtil.getSession().saveOrUpdate(passwordRecovery);
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }

    public void delete(String sessionId) throws CrudDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            String hql = "delete from PasswordRecovery where sessionId = :sessionId";
            HibernateUtil.getSession().createQuery(hql).setString("sessionId", sessionId).executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        }
    }
}
