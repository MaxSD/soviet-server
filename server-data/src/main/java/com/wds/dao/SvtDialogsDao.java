package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.SvtDialog;
import com.wds.entity.SvtUser;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtDialogsDao")
public class SvtDialogsDao {

    private Logger logger = LoggerFactory.getLogger(SvtDialogsDao.class);

    @SuppressWarnings("unchecked")
    public List<Object> getList(long uid, int start, int count, Class entityObject) throws SelectDatabaseException {
        List<Object> result;
        Transaction transaction = null;
        try {
            String strSQL = "SELECT " +
                    "  d.id                             AS \"dialogId\", " +
                    "  d.title                          AS \"title\", " +
                    "  u.photo                          AS \"userPhoto\", " +
                    "  u.first_name || ' ' || u.surname AS \"userName\", " +
                    "  (SELECT m1.message FROM svt_messages AS m1 WHERE m1.dialog_id = d.id ORDER BY date DESC LIMIT 1) AS \"lastMessage\", " +
                    "  (SELECT m2.type FROM svt_messages AS m2 WHERE m2.dialog_id = d.id ORDER BY date DESC LIMIT 1) AS \"messageType\", " +
                    "  (SELECT m3.date FROM svt_messages AS m3 WHERE m3.dialog_id = d.id ORDER BY date DESC LIMIT 1) AS \"date\", " +
                    "  (SELECT COUNT(*) FROM svt_messages AS msg WHERE msg.dialog_id = d.id AND msg.is_read = FALSE AND msg.is_from_user = FALSE) AS \"unreadCount\", " +
                    "  d.is_closed AS \"dialogIsClosed\" " +
                    "FROM svt_dialogs AS d " +
                    "  INNER JOIN svt_users AS u ON (u.id = d.user_id) " +
                    "WHERE is_closed = FALSE AND user_id = " + uid;

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            transaction = HibernateUtil.beginTransaction();
            result = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("dialogId", new LongType()).
                    addScalar("title").
                    addScalar("userPhoto").
                    addScalar("userName").
                    addScalar("lastMessage").
                    addScalar("messageType", new IntegerType()).
                    addScalar("date", new LongType()).
                    addScalar("unreadCount", new IntegerType()).
                    addScalar("dialogIsClosed", new BooleanType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject)).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(re.getMessage());
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Object> getList(int start, int count, Boolean isClosed, Class entityObject) throws SelectDatabaseException {
        List<Object> result;
        Transaction transaction = null;
        try {
            String strSQL = "SELECT * FROM (" +
                    " SELECT " +
                    "  d.id                             AS \"dialogId\", " +
                    "  d.title                          AS \"title\", " +
                    "  u.photo                          AS \"userPhoto\", " +
                    "  u.first_name || ' ' || u.surname AS \"userName\", " +
                    "  (SELECT m1.message FROM svt_messages AS m1 WHERE m1.dialog_id = d.id ORDER BY date DESC LIMIT 1) AS \"lastMessage\", " +
                    "  (SELECT m2.type FROM svt_messages AS m2 WHERE m2.dialog_id = d.id ORDER BY date DESC LIMIT 1) AS \"messageType\", " +
                    "  (SELECT m3.date FROM svt_messages AS m3 WHERE m3.dialog_id = d.id ORDER BY date DESC LIMIT 1) AS \"date\", " +
                    "  (SELECT COUNT(*) FROM svt_messages AS msg WHERE msg.dialog_id = d.id AND msg.is_read = FALSE AND msg.is_from_user = TRUE) AS \"unreadCount\", " +
                    "  d.is_closed AS \"dialogIsClosed\" " +
                    "FROM svt_dialogs AS d " +
                    "  INNER JOIN svt_users AS u ON (u.id = d.user_id) " +
                    ") AS dm " +
                    "WHERE \"lastMessage\" IS NOT NULL AND \"lastMessage\" <> '' ";

            if (isClosed != null)
                strSQL += " AND \"dialogIsClosed\" = " + isClosed;

            strSQL += "ORDER BY \"date\" DESC ";

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            transaction = HibernateUtil.beginTransaction();
            result = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("dialogId", new LongType()).
                    addScalar("title").
                    addScalar("userPhoto").
                    addScalar("userName").
                    addScalar("lastMessage").
                    addScalar("messageType", new IntegerType()).
                    addScalar("date", new LongType()).
                    addScalar("unreadCount", new IntegerType()).
                    addScalar("dialogIsClosed", new BooleanType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject)).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(re.getMessage());
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public Number getActiveDialogsCount() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            String strSql = "SELECT COUNT(dm) " +
                    "FROM ( " +
                    "       SELECT DISTINCT d.id " +
                    "       FROM svt_messages AS m " +
                    "         INNER JOIN svt_dialogs AS d ON (d.id = m.dialog_id AND d.is_closed = FALSE) " +
                    "       WHERE m.is_from_user = FALSE AND m.is_read = TRUE " +
                    "     ) AS dm";

            session.beginTransaction();
            count = (Number) session.createSQLQuery(strSql).uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public Number getClosedDialogsCount() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtDialog.class).
                    add(Restrictions.ne("isIsClosed", true)).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }
}
