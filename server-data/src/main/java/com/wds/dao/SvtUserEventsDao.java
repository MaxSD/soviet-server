package com.wds.dao;

import com.wds.dataObjects.UsersOnEventFormData;
import com.wds.dataObjects.UsersOnPartnerFormData;
import com.wds.database.HibernateUtil;
import com.wds.entity.SvtEvent;
import com.wds.entity.SvtUserEvents;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtUserEventsDao")
public class SvtUserEventsDao {

    private Logger logger = LoggerFactory.getLogger(SvtUserEventsDao.class);

    public SvtUserEvents findByUserIdAndEventId(long userId, long eventId) throws SelectDatabaseException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserEvents.class).
                    add(Restrictions.and(
                            Restrictions.eq("userId", userId),
                            Restrictions.eq("eventId", eventId)));
            SvtUserEvents result = (SvtUserEvents) criteria.uniqueResult();
            session.getTransaction().commit();

            return result;
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<UsersOnEventFormData> getUsersListByEventId(long eventId,
                                                            int start, int count) throws SelectDatabaseException {
        List<UsersOnEventFormData> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String strSQL = "SELECT " +
                    "  u.id AS \"userId\", " +
                    "  u.photo, " +
                    "  u.first_name || ' ' || u.surname AS \"userName\", " +
                    "  u.email, " +
                    "  u.phone, " +
                    "  u.card_number AS \"cardNumber\", " +
                    "  ue.id AS \"userEventId\" " +
                    "FROM svt_user_events AS ue " +
                    "  LEFT JOIN svt_users AS u ON (u.id = ue.user_id) " +
                    "WHERE ue.event_id = " + eventId;

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            session.beginTransaction();
            list = session.createSQLQuery(strSQL).
                    addScalar("userId", new LongType()).
                    addScalar("photo").
                    addScalar("userName").
                    addScalar("email").
                    addScalar("phone").
                    addScalar("cardNumber").
                    addScalar("userEventId", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(UsersOnEventFormData.class)).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<SvtEvent> getEventsListByUserId(long userId, boolean isVisited,
                                                            int start, int count) throws SelectDatabaseException {
        List<SvtEvent> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String strSQL = "SELECT " +
                    "  e.* " +
                    "FROM svt_user_events AS ue " +
                    "  LEFT JOIN svt_events AS e ON (e.id = ue.event_id) " +
                    "WHERE ue.user_id = " + userId + " AND ue.is_visited = " + isVisited;

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            session.beginTransaction();
            list = session.createSQLQuery(strSQL).addEntity(SvtEvent.class).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public Number getCount(long eventId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserEvents.class).
                    add(Restrictions.eq("eventId", eventId)).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public Number getCountByUserId(long userId, boolean isVisited) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserEvents.class).
                    add(Restrictions.and(
                            Restrictions.eq("userId", userId),
                            Restrictions.eq("isIsVisited", isVisited)
                    )).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }
}
