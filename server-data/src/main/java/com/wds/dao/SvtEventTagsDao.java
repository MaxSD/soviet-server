package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.SvtEventTags;
import com.wds.entity.SvtUserInterests;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtEventTagsDao")
public class SvtEventTagsDao {

    private Logger logger = LoggerFactory.getLogger(SvtEventTagsDao.class);

    @SuppressWarnings("unchecked")
    public void deleteEventTags(long eventId) throws SelectDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            String strSql = "DELETE FROM svt_event_tags WHERE event_id = " + eventId;
            HibernateUtil.getSession().createSQLQuery(strSql).executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Long> getTagIdsByEventId(long eventId) throws SelectDatabaseException {
        List<Long> result;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtEventTags.class).
                    add(Restrictions.eq("eventId", eventId)).
                    setProjection(Projections.property("tagId"));
            result = criteria.list();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return result;
    }
}
