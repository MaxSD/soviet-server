package com.wds.dao;

import com.wds.dataObjects.UsersOnPartnerFormData;
import com.wds.database.HibernateUtil;
import com.wds.entity.SvtUserPartners;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtUserPartnersDao")
public class SvtUserPartnersDao {

    private Logger logger = LoggerFactory.getLogger(SvtUserPartnersDao.class);

    @SuppressWarnings("unchecked")
    public List<UsersOnPartnerFormData> getUsersListByPartnerId(long partnerId,
                                                            int start, int count) throws SelectDatabaseException {
        List<UsersOnPartnerFormData> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String strSQL = "SELECT " +
                    "  u.id AS \"userId\", " +
                    "  u.photo, " +
                    "  u.first_name || ' ' || u.surname AS \"userName\", " +
                    "  u.email, " +
                    "  u.phone, " +
                    "  u.card_number AS \"cardNumber\", " +
                    "  up.id AS \"userPartnerId\" " +
                    "FROM svt_user_partners AS up " +
                    "  LEFT JOIN svt_users AS u ON (u.id = up.user_id) " +
                    "WHERE up.partner_id = " + partnerId  + " " +
                    "ORDER BY up.id DESC ";

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            session.beginTransaction();
            list = session.createSQLQuery(strSQL).
                    addScalar("userId", new LongType()).
                    addScalar("photo").
                    addScalar("userName").
                    addScalar("email").
                    addScalar("phone").
                    addScalar("cardNumber").
                    addScalar("userPartnerId", new LongType()).
                    setResultTransformer(Transformers.aliasToBean(UsersOnPartnerFormData.class)).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public Number getCount(long partnerId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(SvtUserPartners.class).
                    add(Restrictions.eq("partnerId", partnerId)).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }
}
