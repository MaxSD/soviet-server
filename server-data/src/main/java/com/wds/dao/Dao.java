package com.wds.dao;

import com.wds.database.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("Dao")
public class Dao {

    private Logger logger = LoggerFactory.getLogger(Dao.class);

    public <T> void create(final T o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(o);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T save(final T o) {
        T t;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(o);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return o;
    }

    public void delete(final Object object) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.delete(object);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T find(final Class<T> type, final Long id) {
        T t;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            t = (T) session.get(type, id);
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return t;
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> get(final Class<T> type) {
        List<T> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            list = session.createCriteria(type).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> get(final Class<T> type, int offset, int limit) {
        List<T> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            list = session.createCriteria(type).
                    addOrder(Order.asc("id")).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
                    setFirstResult(offset).
                    setMaxResults(limit).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public <T> Number getCount(final Class<T> type) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Number count;
        try {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(type).
                    setProjection(Projections.rowCount()).
                    setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

            count = (Number) criteria.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
        return count;
    }

}
