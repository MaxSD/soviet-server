package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.database.define.Defines;
import com.wds.entity.SvtMessageUnread;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service("SvtMessagesUnreadDao")
public class SvtMessagesUnreadDao {

    private Logger logger = LoggerFactory.getLogger(SvtMessagesUnreadDao.class);

    public SvtMessageUnread findById(long id) throws SelectDatabaseException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtMessageUnread.class).
                    add(Restrictions.eq("id", id));
            SvtMessageUnread messageUnread = (SvtMessageUnread) criteria.uniqueResult();
            session.getTransaction().commit();

            return messageUnread;
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
    }

    public long getCountByReceiverId(long receiverId) throws SelectDatabaseException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtMessageUnread.class).
                    add(Restrictions.and(
                            Restrictions.eq("receiverId", receiverId),
                            Restrictions.ne("status", Defines.UNREAD_MESSAGES_STATUS_LAST_VIEWED_MESSAGE)
                    )).
                    setProjection(Projections.rowCount());
            Long messageUnreadCount = (Long) criteria.uniqueResult();
            session.getTransaction().commit();

            return messageUnreadCount;
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        } finally {
            session.close();
        }
    }

    public void setStatus(Long messageId, int status) throws CrudDatabaseException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            StringBuilder sbSql = new StringBuilder("UPDATE svt_messages_unread ");
            sbSql.append("SET status = ").append(status);
            sbSql.append(" WHERE ");
            sbSql.append("message_id = ").append(messageId);

            session.beginTransaction();
            HibernateUtil.getSession().createSQLQuery(sbSql.toString()).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        } finally {
            session.close();
        }
    }

    public void deleteAllByMessagesIds(Long[] messageIds) throws CrudDatabaseException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            StringBuilder sbSql = new StringBuilder("DELETE FROM sf_messages_unread WHERE ");
            for (int i = 0; i < messageIds.length; i++) {
                if (i > 0)
                    sbSql.append(" OR ");
                sbSql.append(" message_id = ").append(messageIds[i]);
            }

            session.beginTransaction();
            HibernateUtil.getSession().createSQLQuery(sbSql.toString()).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new CrudDatabaseException();
        } finally {
            session.close();
        }
    }
}
