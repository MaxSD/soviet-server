package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.entity.SvtUserInterests;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtInterestsDao")
public class SvtInterestsDao {

    private Logger logger = LoggerFactory.getLogger(SvtInterestsDao.class);

    @SuppressWarnings("unchecked")
    public List<String> getAllInterests() throws SelectDatabaseException {
        List<String> result;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();

            String strSQL = "SELECT id || '|' || name AS tagIdName FROM svt_tags";

            result = HibernateUtil.getSession().createSQLQuery(strSQL).list();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUserInterests(long uid) throws SelectDatabaseException {
        List<Long> result;
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            Criteria criteria = HibernateUtil.getSession().createCriteria(SvtUserInterests.class).
                    add(Restrictions.eq("userId", uid)).
                    setProjection(Projections.property("tagId"));
            result = criteria.list();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public void deleteUserInterests(long uid) throws SelectDatabaseException {
        Transaction transaction = null;
        try {
            transaction = HibernateUtil.beginTransaction();
            String strSql = "DELETE FROM svt_user_interests WHERE user_id = " + uid;
            HibernateUtil.getSession().createSQLQuery(strSql).executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(Arrays.toString(e.getStackTrace()));
            throw new SelectDatabaseException();
        }
    }
}
