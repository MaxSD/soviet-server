package com.wds.dao;

import com.wds.database.HibernateUtil;
import com.wds.exception.v1.CrudDatabaseException;
import com.wds.exception.v1.SelectDatabaseException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service("SvtMessagesDao")
public class SvtMessagesDao {

    private Logger logger = LoggerFactory.getLogger(SvtMessagesDao.class);

    @SuppressWarnings("unchecked")
    public List<Object> getList(long dialogId, int start, int count, Class entityObject) throws SelectDatabaseException {
        List<Object> result;
        Transaction transaction = null;
        try {
            String strSQL = "SELECT " +
                    "  (SELECT d.title FROM svt_dialogs d WHERE d.id = "+dialogId+") AS \"dialogName\", " +
                    "  m.id                             AS \"messageId\", " +
                    "  m.sender_id                      AS \"senderId\", " +
                    "  u.photo                          AS \"senderPhoto\", " +
                    "  u.first_name || ' ' || u.surname AS \"senderName\", " +
                    "  m.message, " +
                    "  m.service_message AS \"serviceMessage\", " +
                    "  m.type AS \"messageType\", " +
                    "  m.date," +
                    "  m.is_from_user AS \"isIsFromUser\" " +
                    "FROM svt_messages AS m " +
                    "  INNER JOIN svt_users AS u ON (u.id = m.sender_id) " +
                    "WHERE dialog_id = " + dialogId +
                    " ORDER BY m.id ASC ";

            if (start > 0)
                strSQL += " OFFSET " + start;

            if (count > 0)
                strSQL += " LIMIT " + count;

            transaction = HibernateUtil.beginTransaction();
            result = HibernateUtil.getSession().createSQLQuery(strSQL).
                    addScalar("dialogName").
                    addScalar("messageId", new LongType()).
                    addScalar("senderId", new LongType()).
                    addScalar("senderPhoto").
                    addScalar("senderName").
                    addScalar("message").
                    addScalar("serviceMessage").
                    addScalar("messageType", new IntegerType()).
                    addScalar("date", new LongType()).
                    addScalar("isIsFromUser", new BooleanType()).
                    setResultTransformer(Transformers.aliasToBean(entityObject)).list();
            transaction.commit();
        } catch (RuntimeException re) {
            HibernateUtil.rollbackTransaction(transaction);
            logger.error(re.getMessage());
            throw new SelectDatabaseException();
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public void selectMessagesAsReaded(long dialogId, boolean isFromUser) throws CrudDatabaseException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {

            String strSql = "UPDATE svt_messages SET is_read = TRUE WHERE dialog_id = " + dialogId + " AND is_from_user = " + isFromUser;

            session.beginTransaction();
            session.createSQLQuery(strSql).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            logger.error(Arrays.toString(e.getStackTrace()));
            throw e;
        } finally {
            session.close();
        }
    }
}
