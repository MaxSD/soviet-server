package com.wds.dataObjects;

public class EventsForChartData {
    private long curr_month_ms;
    private long count;

    public long getCurr_month_ms() {
        return curr_month_ms;
    }

    public void setCurr_month_ms(long curr_month_ms) {
        this.curr_month_ms = curr_month_ms;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
