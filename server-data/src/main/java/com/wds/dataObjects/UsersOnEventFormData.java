package com.wds.dataObjects;

public class UsersOnEventFormData {
    private Long userId;
    private String photo;
    private String userName;
    private String email;
    private String phone;
    private String cardNumber;
    private Long userEventId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Long getUserEventId() {
        return userEventId;
    }

    public void setUserEventId(Long userEventId) {
        this.userEventId = userEventId;
    }
}
