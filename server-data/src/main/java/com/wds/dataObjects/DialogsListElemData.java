package com.wds.dataObjects;

public class DialogsListElemData {
    private Long dialogId;
    private String title;
    private String userPhoto;
    private String userName;
    private String lastMessage;
    private Integer messageType;
    private Long date;
    private Integer unreadCount;
    private Boolean dialogIsClosed;

    public Long getDialogId() {
        return dialogId;
    }

    public void setDialogId(Long dialogId) {
        this.dialogId = dialogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(Integer unreadCount) {
        this.unreadCount = unreadCount;
    }

    public Boolean getDialogIsClosed() {
        return dialogIsClosed;
    }

    public void setDialogIsClosed(Boolean dialogIsClosed) {
        this.dialogIsClosed = dialogIsClosed;
    }
}
