package com.wds.database.define;

public class Defines {
    // EVENTS
    public final static int EVENT_STATUS_NEW = 0;
    public final static int EVENT_STATUS_ACCEPTED = 1;
    public final static int EVENT_STATUS_PAID = 2;

    // Unread messages status
    public final static int UNREAD_MESSAGES_STATUS_LAST_VIEWED_MESSAGE = 1;

    // Message types
    public final static int MESSAGE_TYPE_TEXT = 1;
    public final static int MESSAGE_TYPE_LINK = 2;
    public final static int MESSAGE_TYPE_BUTTON = 3;

    // User roles
    public final static long USER_TYPE_ADMIN = 1;
    public final static long USER_TYPE_MODERATOR = 2;
    public final static long USER_TYPE_USER = 3;
}
