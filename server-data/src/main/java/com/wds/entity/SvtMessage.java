package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "svt_messages")
public class SvtMessage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "dialog_id")
    private Long dialogId;

    @Column(name = "sender_id")
    private Long senderId;

    @Column(name = "message")
    private String message;

    @Column(name = "service_message")
    private String serviceMessage;

    @Column(name = "type")
    private int type;

    @Column(name = "date")
    private long date;

    @Column(name = "is_from_user")
    private boolean isIsFromUser;

    @Column(name = "is_read")
    private boolean isIsRead;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDialogId() {
        return dialogId;
    }

    public void setDialogId(Long dialogId) {
        this.dialogId = dialogId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public String getServiceMessage() {
        return serviceMessage;
    }

    public void setServiceMessage(String serviceMessage) {
        this.serviceMessage = serviceMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isIsFromUser() {
        return isIsFromUser;
    }

    public void setIsIsFromUser(boolean isIsFromUser) {
        this.isIsFromUser = isIsFromUser;
    }

    public boolean isIsRead() {
        return isIsRead;
    }

    public void setIsIsRead(boolean isIsRead) {
        this.isIsRead = isIsRead;
    }
}
