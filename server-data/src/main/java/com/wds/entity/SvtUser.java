package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "svt_users")
public class SvtUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_registration")
    private long dateRegistration;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "surname")
    private String surname;

    @Column(name = "gender")
    private int gender;

    @Column(name = "birth_date")
    private long birthDate;

    @Column(name = "phone")
    private String phone;

    @Column(name = "profession")
    private String profession;

    @Column(name = "about")
    private String about;

    @Column(name = "email")
    private String email;

    @Column(name = "photo")
    private String photo;

    @Column(name = "password")
    private String password;

    @Column(name = "is_push_up", nullable = false, columnDefinition = "true")
    private boolean isIsPushUp;

    @Column(name = "newsletters_period")
    private int newslettersPeriod;

    @Column(name = "is_geolocation_on", nullable = false, columnDefinition = "true")
    private boolean isIsGeolocationOn;

    @Column(name = "events_score")
    private int eventsScore;

    @Column(name = "orders_score")
    private int ordersScore;

    @Column(name = "partners_score")
    private int partnersScore;

    @Column(name = "token")
    private String token;

    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;

    @Column(name = "friend")
    private String friend;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(long dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIsPushUp() {
        return isIsPushUp;
    }

    public void setIsIsPushUp(boolean isIsPushUp) {
        this.isIsPushUp = isIsPushUp;
    }

    public int getNewslettersPeriod() {
        return newslettersPeriod;
    }

    public void setNewslettersPeriod(int newslettersPeriod) {
        this.newslettersPeriod = newslettersPeriod;
    }

    public boolean isIsGeolocationOn() {
        return isIsGeolocationOn;
    }

    public void setIsIsGeolocationOn(boolean isIsGeolocationOn) {
        this.isIsGeolocationOn = isIsGeolocationOn;
    }

    public int getEventsScore() {
        return eventsScore;
    }

    public void setEventsScore(int eventsScore) {
        this.eventsScore = eventsScore;
    }

    public int getOrdersScore() {
        return ordersScore;
    }

    public void setOrdersScore(int ordersScore) {
        this.ordersScore = ordersScore;
    }

    public int getPartnersScore() {
        return partnersScore;
    }

    public void setPartnersScore(int partnersScore) {
        this.partnersScore = partnersScore;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFriend() {
        return friend;
    }

    public void setFriend(String friend) {
        this.friend = friend;
    }
}
