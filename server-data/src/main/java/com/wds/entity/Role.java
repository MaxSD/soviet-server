package com.wds.entity;

import com.wds.database.HstoreUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@SuppressWarnings("UnusedDeclaration")
@Entity
@TypeDef(name = "hstore", typeClass = HstoreUserType.class)
@Table(name = "srv_roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> text;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> description;

    @OneToMany(mappedBy = "role")
    private List<SvtUser> users;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE},
            fetch = FetchType.EAGER)
    @JoinTable(
            name = "srv_roles_permissions",
            joinColumns = {@JoinColumn(name = "id_role", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "id_permission", referencedColumnName = "id")}
    )
    private List<Permission> permissionsList;

    public String[] getPermissionsNames() {
        if(this.permissionsList !=null) {
            String[] result = new String[this.permissionsList.size()];
            int i = 0;
            for(Permission permission : this.permissionsList) {
                result[i] = permission.getName();
                i++;
            }
            return result;
        }
        return new String[0];
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Map<String, String> getText() { return text; }
    public void setText(Map<String, String> text) {
        this.text = text;
    }

    public Map<String, String> getDescription() {
        return description;
    }
    public void setDescription(Map<String, String> description) {
        this.description = description;
    }

    public List<SvtUser> getUsers() { return users; }
    public void setUsers(List<SvtUser> users) { this.users = users; }

    public List<Permission> getPermissionsList() { return permissionsList; }
    public void setPermissionsList(List<Permission> permissionsList) { this.permissionsList = permissionsList; }
}
