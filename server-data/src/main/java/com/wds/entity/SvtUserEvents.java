package com.wds.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("UnusedDeclaration")
@Entity
@Table(name = "svt_user_events")
public class SvtUserEvents implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "event_id")
    private Long eventId;

    @Column(name = "is_paid")
    private boolean isIsPaid;

    @Column(name = "is_visited")
    private boolean isIsVisited;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public boolean isIsPaid() {
        return isIsPaid;
    }

    public void setIsIsPaid(boolean isIsPaid) {
        this.isIsPaid = isIsPaid;
    }

    public boolean isIsVisited() {
        return isIsVisited;
    }

    public void setIsIsVisited(boolean isIsVisited) {
        this.isIsVisited = isIsVisited;
    }
}
