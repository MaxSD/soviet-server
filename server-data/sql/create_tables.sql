-- ***********************************************************************************
-- Table: svt_users
DROP TABLE IF EXISTS svt_users CASCADE;
DROP SEQUENCE IF EXISTS svt_users_id_seq;

-- Sequence: svt_users_id_seq
CREATE SEQUENCE svt_users_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_users_id_seq
OWNER TO soviet;

CREATE TABLE svt_users
(
  id                 BIGINT  NOT NULL           DEFAULT nextval('svt_users_id_seq' :: REGCLASS),
  date_registration  BIGINT  NOT NULL           DEFAULT 0,
  card_number        TEXT                       DEFAULT '' :: TEXT,
  first_name         TEXT    NOT NULL,
  surname            TEXT    NOT NULL,
  gender             INTEGER NOT NULL,
  birth_date         BIGINT  NOT NULL           DEFAULT 0,
  phone              TEXT    NOT NULL,
  profession         TEXT    NOT NULL,
  about              TEXT,
  email              TEXT    NOT NULL,
  photo              TEXT                       DEFAULT '' :: TEXT,
  password           TEXT    NOT NULL,
  is_push_up         BOOLEAN NOT NULL           DEFAULT TRUE,
  newsletters_period INTEGER                    DEFAULT 1,
  is_geolocation_on  BOOLEAN NOT NULL           DEFAULT FALSE,
  events_score       INTEGER                    DEFAULT 0,
  orders_score       INTEGER                    DEFAULT 0,
  partners_score     INTEGER                    DEFAULT 0,
  token              TEXT                       DEFAULT '' :: TEXT,
  id_role            BIGINT                     DEFAULT 3,
  friend             TEXT                       DEFAULT '' :: TEXT,
  CONSTRAINT svt_users_pkey PRIMARY KEY (id),
  CONSTRAINT "User_Role" FOREIGN KEY (id_role)
  REFERENCES srv_roles (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT svt_users_email_key UNIQUE (email)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_users
OWNER TO soviet;

DROP INDEX IF EXISTS "Email";
CREATE INDEX "Email"
ON svt_users
USING BTREE
(email);

-- Default: users
INSERT INTO svt_users (card_number, first_name, surname, gender, birth_date, phone, profession, about,
                       email, photo, password, is_push_up, newsletters_period, is_geolocation_on, events_score,
                       orders_score, partners_score, token, id_role)
VALUES
  ('', '', '', 1, 0, '', '', '',
   'admin', '', '21232f297a57a5a743894a0e4a801fc3', FALSE, 0, FALSE, 0,
   0, 0, '', 1);

-- ***********************************************************************************
-- Table: svt_user_token

DROP TABLE IF EXISTS svt_user_token CASCADE;
DROP SEQUENCE IF EXISTS svt_user_token_id_seq;

-- Sequence: svt_user_token_id_seq
CREATE SEQUENCE svt_user_token_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_user_token_id_seq
OWNER TO safer;

CREATE TABLE svt_user_token
(
  id         BIGINT  NOT NULL DEFAULT nextval('svt_user_token_id_seq' :: REGCLASS),
  user_id    BIGINT  NOT NULL,
  is_android BOOLEAN NOT NULL DEFAULT FALSE,
  token      TEXT,
  CONSTRAINT svt_user_token_pkey PRIMARY KEY (id),
  CONSTRAINT "User" FOREIGN KEY (user_id)
  REFERENCES svt_users (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT "UserTokenExists" UNIQUE (user_id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_user_token
OWNER TO soviet;

-- Index: "fki_User"

DROP INDEX IF EXISTS "fki_User";
CREATE INDEX "fki_User"
ON svt_user_token
USING BTREE
(user_id);

-- ***********************************************************************************
-- Table: svt_industry
DROP TABLE IF EXISTS svt_industry CASCADE;
DROP SEQUENCE IF EXISTS svt_industry_id_seq;

-- Sequence: svt_industry_id_seq
CREATE SEQUENCE svt_industry_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_industry_id_seq
OWNER TO soviet;

CREATE TABLE svt_industry
(
  id   BIGINT NOT NULL  DEFAULT nextval('svt_industry_id_seq' :: REGCLASS),
  name TEXT   NOT NULL,
  CONSTRAINT svt_industry_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_industry
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_events
DROP TABLE IF EXISTS svt_events CASCADE;
DROP SEQUENCE IF EXISTS svt_events_id_seq;

-- Sequence: svt_events_id_seq
CREATE SEQUENCE svt_events_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_events_id_seq
OWNER TO soviet;

CREATE TABLE svt_events
(
  id               BIGINT  NOT NULL  DEFAULT nextval('svt_events_id_seq' :: REGCLASS),
  city_id          BIGINT  NOT NULL,
  title            TEXT    NOT NULL  DEFAULT '',
  description      TEXT    NOT NULL  DEFAULT '',
  full_description TEXT    NOT NULL  DEFAULT '',
  type             TEXT    NOT NULL  DEFAULT '',
  address          TEXT    NOT NULL  DEFAULT '',
  image1           TEXT    NOT NULL  DEFAULT '',
  image2           TEXT    NOT NULL  DEFAULT '',
  image3           TEXT    NOT NULL  DEFAULT '',
  date             BIGINT  NOT NULL,
  curr_month_ms    BIGINT  NOT NULL,
  price            DOUBLE PRECISION  DEFAULT 0,
  score            INTEGER NOT NULL  DEFAULT 0,
  status           INTEGER NOT NULL  DEFAULT 0,
  CONSTRAINT svt_events_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_events
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_event_comments
DROP TABLE IF EXISTS svt_event_comments CASCADE;
DROP SEQUENCE IF EXISTS svt_event_comments_id_seq;

-- Sequence: svt_event_comments_id_seq
CREATE SEQUENCE svt_event_comments_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_event_comments_id_seq
OWNER TO soviet;

CREATE TABLE svt_event_comments
(
  id       BIGINT NOT NULL  DEFAULT nextval('svt_event_comments_id_seq' :: REGCLASS),
  event_id BIGINT NOT NULL,
  user_id  BIGINT NOT NULL,
  message  TEXT   NOT NULL  DEFAULT '',
  date     BIGINT NOT NULL,
  CONSTRAINT svt_event_comments_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_event_comments
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_event_tags
DROP TABLE IF EXISTS svt_event_tags CASCADE;
DROP SEQUENCE IF EXISTS svt_event_tags_id_seq;

-- Sequence: svt_event_tags_id_seq
CREATE SEQUENCE svt_event_tags_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_event_tags_id_seq
OWNER TO soviet;

CREATE TABLE svt_event_tags
(
  id       BIGINT NOT NULL  DEFAULT nextval('svt_event_tags_id_seq' :: REGCLASS),
  event_id BIGINT NOT NULL,
  tag_id   BIGINT NOT NULL,
  CONSTRAINT svt_event_tags_pkey PRIMARY KEY (id),
  CONSTRAINT "Event" FOREIGN KEY (event_id)
  REFERENCES svt_events (id) MATCH SIMPLE
  ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_event_tags
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_user_events
DROP TABLE IF EXISTS svt_user_events CASCADE;
DROP SEQUENCE IF EXISTS svt_user_events_id_seq;

-- Sequence: svt_user_events_id_seq
CREATE SEQUENCE svt_user_events_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_user_events_id_seq
OWNER TO soviet;

CREATE TABLE svt_user_events
(
  id         BIGINT  NOT NULL  DEFAULT nextval('svt_user_events_id_seq' :: REGCLASS),
  user_id    BIGINT  NOT NULL,
  event_id   BIGINT  NOT NULL,
  is_paid    BOOLEAN NOT NULL  DEFAULT FALSE,
  is_visited BOOLEAN NOT NULL  DEFAULT FALSE,
  CONSTRAINT svt_user_events_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_user_events
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_user_interests
DROP TABLE IF EXISTS svt_user_interests CASCADE;
DROP SEQUENCE IF EXISTS svt_user_interests_id_seq;

-- Sequence: svt_user_interests_id_seq
CREATE SEQUENCE svt_user_interests_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_user_interests_id_seq
OWNER TO soviet;

CREATE TABLE svt_user_interests
(
  id      BIGINT NOT NULL  DEFAULT nextval('svt_user_interests_id_seq' :: REGCLASS),
  user_id BIGINT NOT NULL,
  tag_id  BIGINT NOT NULL,
  CONSTRAINT svt_user_interests_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_user_interests
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_dialogs
DROP TABLE IF EXISTS svt_dialogs CASCADE;
DROP SEQUENCE IF EXISTS svt_dialogs_id_seq;

-- Sequence: svt_dialogs_id_seq
CREATE SEQUENCE svt_dialogs_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_dialogs_id_seq
OWNER TO soviet;

CREATE TABLE svt_dialogs
(
  id        BIGINT  NOT NULL  DEFAULT nextval('svt_dialogs_id_seq' :: REGCLASS),
  user_id   BIGINT  NOT NULL,
  title     TEXT    NOT NULL  DEFAULT '',
  date      BIGINT  NOT NULL,
  is_closed BOOLEAN NOT NULL  DEFAULT FALSE,
  CONSTRAINT svt_dialogs_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_dialogs
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_messages
DROP TABLE IF EXISTS svt_messages CASCADE;
DROP SEQUENCE IF EXISTS svt_messages_id_seq;

-- Sequence: svt_messages_id_seq
CREATE SEQUENCE svt_messages_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_messages_id_seq
OWNER TO soviet;

CREATE TABLE svt_messages
(
  id              BIGINT  NOT NULL  DEFAULT nextval('svt_messages_id_seq' :: REGCLASS),
  dialog_id       BIGINT  NOT NULL,
  sender_id       BIGINT  NOT NULL,
  message         TEXT    NOT NULL  DEFAULT '',
  service_message TEXT              DEFAULT '',
  type            INTEGER NOT NULL  DEFAULT 1,
  date            BIGINT  NOT NULL,
  is_from_user    BOOLEAN NOT NULL  DEFAULT FALSE,
  is_read         BOOLEAN NOT NULL  DEFAULT FALSE,
  CONSTRAINT svt_messages_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_messages
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_tags
DROP TABLE IF EXISTS svt_tags CASCADE;
DROP SEQUENCE IF EXISTS svt_tags_id_seq;

-- Sequence: svt_tags_id_seq
CREATE SEQUENCE svt_tags_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_tags_id_seq
OWNER TO safer;

CREATE TABLE svt_tags
(
  id   BIGINT NOT NULL DEFAULT nextval('svt_tags_id_seq' :: REGCLASS),
  name TEXT,
  CONSTRAINT svt_tags_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_tags
OWNER TO soviet;

-- Default: srv_tags
INSERT INTO svt_tags (name) VALUES ('Art');
INSERT INTO svt_tags (name) VALUES ('Sport');
INSERT INTO svt_tags (name) VALUES ('Food');
INSERT INTO svt_tags (name) VALUES ('Restaurants');
INSERT INTO svt_tags (name) VALUES ('Networking');
INSERT INTO svt_tags (name) VALUES ('Business');
INSERT INTO svt_tags (name) VALUES ('Bars');
INSERT INTO svt_tags (name) VALUES ('Music');
INSERT INTO svt_tags (name) VALUES ('Concerts');
INSERT INTO svt_tags (name) VALUES ('London');
INSERT INTO svt_tags (name) VALUES ('Berlin');
INSERT INTO svt_tags (name) VALUES ('Moscow');
INSERT INTO svt_tags (name) VALUES ('Prague');
INSERT INTO svt_tags (name) VALUES ('Dubai');
INSERT INTO svt_tags (name) VALUES ('Miami');
INSERT INTO svt_tags (name) VALUES ('Geneva');
INSERT INTO svt_tags (name) VALUES ('Paris');
INSERT INTO svt_tags (name) VALUES ('Baku');
INSERT INTO svt_tags (name) VALUES ('Erevan');
INSERT INTO svt_tags (name) VALUES ('Almaty');
INSERT INTO svt_tags (name) VALUES ('St.Petersburg');
INSERT INTO svt_tags (name) VALUES ('Vienna');
INSERT INTO svt_tags (name) VALUES ('Milano');
INSERT INTO svt_tags (name) VALUES ('Zurich');
INSERT INTO svt_tags (name) VALUES ('Barcelona');
INSERT INTO svt_tags (name) VALUES ('Amsterdam');
INSERT INTO svt_tags (name) VALUES ('Marbella');
INSERT INTO svt_tags (name) VALUES ('St.Tropez');
INSERT INTO svt_tags (name) VALUES ('Cannes');
INSERT INTO svt_tags (name) VALUES ('Monaco');
INSERT INTO svt_tags (name) VALUES ('Lectures');
INSERT INTO svt_tags (name) VALUES ('Exhibitions');
INSERT INTO svt_tags (name) VALUES ('Theatres');
INSERT INTO svt_tags (name) VALUES ('Cinema');
INSERT INTO svt_tags (name) VALUES ('Movie');
INSERT INTO svt_tags (name) VALUES ('Politics');
INSERT INTO svt_tags (name) VALUES ('Economics');
INSERT INTO svt_tags (name) VALUES ('Finance');
INSERT INTO svt_tags (name) VALUES ('Traveling');
INSERT INTO svt_tags (name) VALUES ('Books');
INSERT INTO svt_tags (name) VALUES ('Pets');
INSERT INTO svt_tags (name) VALUES ('Happiness');
INSERT INTO svt_tags (name) VALUES ('Health');
INSERT INTO svt_tags (name) VALUES ('Cooking');
INSERT INTO svt_tags (name) VALUES ('Design');
INSERT INTO svt_tags (name) VALUES ('Technology');
INSERT INTO svt_tags (name) VALUES ('Ecology');
INSERT INTO svt_tags (name) VALUES ('Extreme sports');
INSERT INTO svt_tags (name) VALUES ('Motivation');
INSERT INTO svt_tags (name) VALUES ('Humour');
INSERT INTO svt_tags (name) VALUES ('Self development');

-- ***********************************************************************************
-- Table: svt_orders
DROP TABLE IF EXISTS svt_orders CASCADE;
DROP SEQUENCE IF EXISTS svt_orders_id_seq;

-- Sequence: svt_orders_id_seq
CREATE SEQUENCE svt_orders_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_orders_id_seq
OWNER TO soviet;

CREATE TABLE svt_orders
(
  id      BIGINT  NOT NULL  DEFAULT nextval('svt_orders_id_seq' :: REGCLASS),
  uid     BIGINT  NOT NULL,
  date    BIGINT  NOT NULL,
  title   TEXT    NOT NULL  DEFAULT '',
  price   DOUBLE PRECISION  DEFAULT 0,
  is_paid BOOLEAN NOT NULL  DEFAULT FALSE,
  CONSTRAINT svt_orders_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_orders
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_partners
DROP TABLE IF EXISTS svt_partners CASCADE;
DROP SEQUENCE IF EXISTS svt_partners_id_seq;

-- Sequence: svt_partners_id_seq
CREATE SEQUENCE svt_partners_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_partners_id_seq
OWNER TO soviet;

CREATE TABLE svt_partners
(
  id               BIGINT NOT NULL  DEFAULT nextval('svt_partners_id_seq' :: REGCLASS),
  name             TEXT   NOT NULL  DEFAULT '',
  description      TEXT   NOT NULL  DEFAULT '',
  full_description TEXT   NOT NULL  DEFAULT '',
  contacts         TEXT   NOT NULL  DEFAULT '',
  image1           TEXT   NOT NULL  DEFAULT '',
  image2           TEXT   NOT NULL  DEFAULT '',
  image3           TEXT   NOT NULL  DEFAULT '',
  comments         TEXT   NOT NULL  DEFAULT '',
  discount         DOUBLE PRECISION DEFAULT 0,
  CONSTRAINT svt_partners_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_partners
OWNER TO soviet;

-- ***********************************************************************************
-- Table: svt_user_partners
DROP TABLE IF EXISTS svt_user_partners CASCADE;
DROP SEQUENCE IF EXISTS svt_user_partners_id_seq;

-- Sequence: svt_user_partners_id_seq
CREATE SEQUENCE svt_user_partners_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE svt_user_partners_id_seq
OWNER TO soviet;

CREATE TABLE svt_user_partners
(
  id         BIGINT NOT NULL  DEFAULT nextval('svt_user_partners_id_seq' :: REGCLASS),
  user_id    BIGINT NOT NULL,
  partner_id BIGINT NOT NULL,
  date       BIGINT NOT NULL,
  CONSTRAINT svt_user_partners_pkey PRIMARY KEY (id)
)
WITH (
OIDS =FALSE
);
ALTER TABLE svt_user_partners
OWNER TO soviet;